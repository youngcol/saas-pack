<?php

return [

    'pages' => [
        'home' => [
            'title'         => 'Saas pack home | SEO title',
            'description'   => 'Public index home page description'
        ],

    ],


    'main' => [
        'og:title'      => '',
        'description'   => '',
        'keywords'      => ''
    ]

];
