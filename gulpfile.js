'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
sass.compiler = require('node-sass');
var exec = require('child_process').exec;

var paths = {

};

var adminSass = function()
{
    return gulp.src('./htdocs/admin/assets/scss/**/*.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest('./htdocs/admin/assets/css'));
};


var compileLangs = function (cb) {
    return exec('php partisan compile:languages', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
};


// Watch files
function watchFiles() {
    gulp.watch("./htdocs/admin/assets/scss/**/*.scss", adminSass);
    gulp.watch("./lang/**/*.php", compileLangs);
}


gulp.task('admin:sass', function () {

});


gulp.task('default', function () {
    gulp.watch('./htdocs/admin/assets/scss/**/*.scss', adminSass);
});


const watch = gulp.parallel(watchFiles);


exports.watch = watch;
exports.admin_sass = adminSass;
