#!/usr/bin/env bash


ssh vlad@52.51.255.999 << EOF

    cd /shared/httpd/releases
    git clone https://gitlab.com/youngcol/saas-pack.git `date +%s`


    sudo cp /home/some/.env .env


    sudo composer install
    ./scripts/install.sh
    npm i
    npm run production
    php partisan prepare:production:build
EOF

#"scripts": {
#		"post-create-project-cmd": "bash -c 'chmod 777 ./log && chmod 777 ./public/uploads && mv ./.env.dist ./.env'",
#		"post-update-cmd": "bash -c 'chmod 0755 ./version.sh; ./version.sh ${JOB_NAME} ${BUILD_NUMBER} ${GIT_BRANCH} > ./version.json'",
#		"post-install-cmd": "bash -c 'chmod 0755 ./version.sh; ./version.sh ${JOB_NAME} ${BUILD_NUMBER} ${GIT_BRANCH} > ./version.json'"
#	}
