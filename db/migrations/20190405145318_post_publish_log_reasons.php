<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20190405145318_post_publish_log_reasons.php
 */
class PostPublishLogReasons
{
    /**
     * Do the migration
     */
    public function up()
    {

        Capsule::schema()->table('post_publish_logs', function($table) {

            $table->string('publich_reason_class', 255);
            $table->unsignedInteger('publich_reason_id');
        });

        Capsule::schema()->create('queue_time_published_log', function($table) {
            $table->timestamps();

            $table->unsignedInteger('queue_time_id');
            $table->unsignedInteger('post_queued_place_id');

            $table->foreign('queue_time_id')->references('id')->on('queue_times')->onDelete('cascade');
            $table->foreign('post_queued_place_id')->references('id')->on('post_queued_places')->onDelete('cascade');
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {

        Capsule::schema()->table('post_publish_logs', function($table) {
            $table->dropColumn('publich_reason_class');
            $table->dropColumn('publich_reason_id');
        });

    }
}
