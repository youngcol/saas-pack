<?php

use Illuminate\Database\Capsule\Manager as Capsule;

    /**
     * https://laravel.com/docs/5.8/migrations#columns
     *
     * 20190401194223_contacts_list.php
     */
class SocialEngineInit
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->create('posts', function ($table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('title');
            $table->string('text');
            $table->integer('user_id')->unsigned();
            $table->boolean('is_draft');
        });

        Capsule::schema()->create('media_files', function ($table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('type');
            $table->string('url');
        });

        Capsule::schema()->create('post_media_files', function ($table) {
            $table->increments('id');

            $table->integer('post_id')->unsigned();
            $table->integer('media_file_id')->unsigned();

            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
            $table->foreign('media_file_id')->references('id')->on('media_files')->onDelete('cascade');

        });

    }
}
