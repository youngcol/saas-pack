<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20200302213614_contacts.php
 */
class Contacts
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('contacts', function($table) {

        });

        Capsule::schema()->create('contact_categories', function($table) {
            $table->increments('id');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            $table->string('name', 255);
        });


        Capsule::schema()->create('contact_locations', function($table) {
            $table->increments('id');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            $table->string('name', 255);
        });


        Capsule::schema()->create('contacts', function($table) {
            $table->increments('id');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            $table->string('name', 255);
            $table->string('email', 255);
            $table->string('site_url', 255);
            $table->string('phone', 255);
            $table->string('address', 255);

            $table->string('facebook_link', 255);
            $table->string('youtube_link', 255);
            $table->string('linkedin_link', 255);

            $table->string('yelp_url', 255);
            $table->integer('yelp_reviews_count');
            $table->integer('yelp_reviews');

            $table->integer('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('contact_categories')->onDelete('cascade');

            $table->integer('location_id')->unsigned()->nullable();;
            $table->foreign('location_id')->references('id')->on('contact_locations')->onDelete('cascade');

        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('contacts');

        Capsule::schema()->table('contacts', function($table) {
            // $table->dropColumn('url');
        });

    }
}
