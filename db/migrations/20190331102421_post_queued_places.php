<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20190331102421_post_queued_places.php
 */
class PostQueuedPlaces
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->create('post_publish_params', function($table) {
            $table->increments('id');
            $table->timestamps();

            $table->text('social_accounts');
            $table->text('custom_params');

            $table->integer('post_id')->unsigned();
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
        });

        Capsule::schema()->create('post_queued_places', function($table) {
            $table->increments('id');
            $table->timestamps();

            $table->dateTime('process_started_time');
            $table->integer('is_processed');

            $table->integer('publish_params_id')->unsigned();
            $table->foreign('publish_params_id')->references('id')->on('post_publish_params')->onDelete('cascade');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('post_id')->unsigned();
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');

            $table->integer('order');
        });

        Capsule::schema()->table('post_sheduled_times', function($table) {
            $table->integer('publish_params_id')->unsigned();
            $table->foreign('publish_params_id')->references('id')->on('post_publish_params')->onDelete('cascade');
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('post_queued_places');
        Capsule::schema()->drop('post_publish_params');
    }
}
