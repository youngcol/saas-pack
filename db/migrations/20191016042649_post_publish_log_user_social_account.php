<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20191016042649_post_publish_log_user_social_account.php
 */
class PostPublishLogUserSocialAccount
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('post_publish_logs', function($table) {

//            $table->dropColumn('social_network_id');

            $table->integer('user_social_account_id')->unsigned();
            $table->foreign('user_social_account_id')->references('id')->on('user_social_accounts')->onDelete('cascade');

        });

    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }
}
