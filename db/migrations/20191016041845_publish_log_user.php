<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20191016041845_publish_log_user.php
 */
class PublishLogUser
{
    /**
     * Do the migration
     */
    public function up()
    {

        Capsule::schema()->table('post_publish_logs', function($table) {
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });


    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }
}
