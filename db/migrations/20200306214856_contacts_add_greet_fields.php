<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20200306214856_contacts_add_greet_fields.php
 */
class ContactsAddGreetFields
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('contacts', function($table) {
            $table->string('success_item', 512);
            $table->string('greet_item', 512);
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }
}
