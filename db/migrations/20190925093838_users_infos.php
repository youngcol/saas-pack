<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20190925093838_users_infos.php
 */
class UsersInfos
{
    /**
     * Do the migration
     */
    public function up()
    {

        Capsule::schema()->create('user_infos', function($table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('deleted_at')->nullable();
            $table->string('field');
            $table->longText('data');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('users_infos');

    }
}
