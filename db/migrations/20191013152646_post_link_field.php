<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20191013152646_post_link_field.php
 */
class PostLinkField
{
    /**
     * Do the migration
     */
    public function up()
    {
        Capsule::schema()->table('posts', function($table) {

            $table->string('link', 255);

        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }
}
