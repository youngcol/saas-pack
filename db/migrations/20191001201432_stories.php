<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20191001201432_stories.php
 */
class Stories
{
    /**
     * Do the migration
     */
    public function up()
    {
//        Capsule::schema()->table('stories', function($table) {
//
//        });

        Capsule::schema()->create('stories', function($table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('deleted_at')->nullable();

            $table->longText('args');
            $table->string('class_name');
            $table->string('status');
            $table->string('version');
            $table->string('finished_step')->nullable();
            $table->longText('next_step_params');
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Capsule::schema()->drop('stories');

//        Capsule::schema()->table('stories', function($table) {
//        });
    }
}
