<?php

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * https://laravel.com/docs/5.8/migrations#columns
 *
 * 20200313142317_contact_lists.php
 */
class ContactLists
{
    /**
     * Do the migration
     */
    public function up()
    {

        Capsule::schema()->create('contact_lists', function($table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('deleted_at')->nullable();

            $table->string('name', 255)->nullable();
//
//            $table->unsignedInteger('votes');
//            $table->integer('integer')->nullable();
//            $table->float('amount', 8, 2);
//            $table->tinyInteger('votes');
//
//            $table->dateTime('col');
//            $table->date('col');
//            $table->time('sunrise');
//
//            $table->longText('longText');
//            $table->text('description');
//            $table->enum('level', ['easy', 'hard'])->default('hard');

            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });

        Capsule::schema()->create('contact_list__have__contact', function($table) {
            $table->increments('id');

            $table->integer('contact_id')->unsigned()->nullable();
            $table->foreign('contact_id')->references('id')->on('contacts')->onDelete('cascade');

            $table->integer('contact_list_id')->unsigned()->nullable();
            $table->foreign('contact_list_id')->references('id')->on('contact_lists')->onDelete('cascade');

        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
//        Capsule::schema()->drop('contact_lists');
//
//        Capsule::schema()->table('contact_lists', function($table) {
//            // $table->dropColumn('url');
//        });

    }
}
