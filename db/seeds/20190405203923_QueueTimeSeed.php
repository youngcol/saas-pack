<?php

use App\Models\SocialEngine\QueueTime;

class QueueTimeSeed
{
    public function run()
    {
        $user_id = 1;

        $data = [

            ['week_day' => 'monday', 'intraday_time' => vo('10:14', 'time')],
            ['week_day' => 'monday', 'intraday_time' => vo('12:34', 'time')],
            ['week_day' => 'monday', 'intraday_time' => vo('15:05', 'time')],
            ['week_day' => 'monday', 'intraday_time' => vo('18:54', 'time')],
            ['week_day' => 'monday', 'intraday_time' => vo('22:44', 'time')],


            ['week_day' => 'tuesday', 'intraday_time' => vo('10:14', 'time')],
            ['week_day' => 'tuesday', 'intraday_time' => vo('12:34', 'time')],
            ['week_day' => 'tuesday', 'intraday_time' => vo('15:05', 'time')],
            ['week_day' => 'tuesday', 'intraday_time' => vo('18:54', 'time')],
            ['week_day' => 'tuesday', 'intraday_time' => vo('22:44', 'time')],


            ['week_day' => 'wednesday', 'intraday_time' => vo('10:14', 'time')],
            ['week_day' => 'wednesday', 'intraday_time' => vo('12:34', 'time')],
            ['week_day' => 'wednesday', 'intraday_time' => vo('15:05', 'time')],
            ['week_day' => 'wednesday', 'intraday_time' => vo('18:54', 'time')],
            ['week_day' => 'wednesday', 'intraday_time' => vo('22:44', 'time')],

            ['week_day' => 'thursday', 'intraday_time' => vo('10:14', 'time')],
            ['week_day' => 'thursday', 'intraday_time' => vo('12:34', 'time')],
            ['week_day' => 'thursday', 'intraday_time' => vo('15:05', 'time')],
            ['week_day' => 'thursday', 'intraday_time' => vo('18:54', 'time')],
            ['week_day' => 'thursday', 'intraday_time' => vo('22:44', 'time')],

            ['week_day' => 'friday', 'intraday_time' => vo('10:14', 'time')],
            ['week_day' => 'friday', 'intraday_time' => vo('12:34', 'time')],
            ['week_day' => 'friday', 'intraday_time' => vo('15:05', 'time')],
            ['week_day' => 'friday', 'intraday_time' => vo('18:54', 'time')],
            ['week_day' => 'friday', 'intraday_time' => vo('22:44', 'time')],

            ['week_day' => 'saturday', 'intraday_time' => vo('10:14', 'time')],
            ['week_day' => 'saturday', 'intraday_time' => vo('12:34', 'time')],
            ['week_day' => 'saturday', 'intraday_time' => vo('15:05', 'time')],
            ['week_day' => 'saturday', 'intraday_time' => vo('18:54', 'time')],
            ['week_day' => 'saturday', 'intraday_time' => vo('22:44', 'time')],


            ['week_day' => 'sunday', 'intraday_time' => vo('10:14', 'time')],
            ['week_day' => 'sunday', 'intraday_time' => vo('12:34', 'time')],
            ['week_day' => 'sunday', 'intraday_time' => vo('15:05', 'time')],
            ['week_day' => 'sunday', 'intraday_time' => vo('18:54', 'time')],
            ['week_day' => 'sunday', 'intraday_time' => vo('22:44', 'time')],

        ];

        foreach ($data as $datum)
        {
            $datum['user_id'] = $user_id;
            QueueTime::create($datum);
        }

    }
}
