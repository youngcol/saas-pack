<?php

global $container;

use App\Models\SocialEngine\Post;
use App\Tasks\SocialEngine\Publish\PublishToSocialAccountsTask;
use App\VO\Post\PostInstantlyPublish;


$post = Post::find(11);


$res = task(new PublishToSocialAccountsTask,
    [
        new PostInstantlyPublish(
            $post,
            $post->scheduledTime->publishParams
        )
    ]
);

return [$res];
