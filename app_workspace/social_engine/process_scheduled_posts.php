<?php

global $container;

use App\Facades\CU;
use App\Models\SocialEngine\Post\PostScheduledTime;
use App\Tasks\SocialEngine\Publish\PublishToSocialAccountsTask;


$nowDate = now_date();

$postScheduledTimes = PostScheduledTime::query()
    ->where('scheduled_time', '<=', date_mysql_format($nowDate))
    ->where('process_status', 'scheduled')
    ->orderBy('scheduled_time')
    ->get();

//dump($postScheduledTimes);echo 'process_scheduled_posts.php:17'; exit;
foreach ($postScheduledTimes as $postScheduledTime)
{
    $now = now_date();
    $postScheduledTime->process_started_time = date_mysql_format($now);
    $postScheduledTime->process_status = 'processing';
    $postScheduledTime->save();


    $res = task(new PublishToSocialAccountsTask,
        [
            $postScheduledTime
        ]
    );

    $postScheduledTime->process_status = 'done';
    $postScheduledTime->save();
}
