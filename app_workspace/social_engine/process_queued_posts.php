<?php

global $container;

use App\Console\Commands\App\ProcessPostsPublishingQueueCommand;
use App\Facades\CU;
use App\Facades\db;
use App\Models\SocialEngine\Post\PostQueuedPlace;
use App\Models\SocialEngine\QueueTime;
use App\Tasks\SocialEngine\Publish\PublishToSocialAccountsTask;
use Carbon\Carbon;


$nowGmt = now_date();
// get all users that have placed posts queued places

//$l = l('process_queued_posts');

$processUserIds = DB::table('post_queued_places')
    ->select('user_id')
    ->distinct()
    ->where('is_processed', 0)
    ->get()
    ->pluck('user_id')
    ->toArray();


if (count($processUserIds) == 0)
{
    return;
}

$processUserId = $processUserIds[0];

// TODO we can get this ids for all users
//$queueTimePublishedTodayIds = db::table('queue_time_published_log as ql')
//    ->leftJoin('queue_times as qt', 'qt.id', '=', 'ql.queue_time_id')
//    ->where('ql.created_at', '>=', $nowGmt->format('Y-m-d 00:00:00'))
//    ->where('qt.user_id', $processUserId)
//    ->pluck('queue_time_id')
//    ->toArray();


//$postQueuedPlaces = PostQueuedPlace::query()
//    ->where('is_processed', 0)
//    ->with('user')
//    ->get();
//
//dump($postQueuedPlaces);echo 'process_queued_posts.php:21'; exit;
//
//
//$rows = db::table('queue_times as qt')
//        ->leftJoin('queue_time_published_log as qtpl', 'qt.id','=','qtpl.queue_time_id')
//        ->whereNull('qtpl.post_queued_place_id')
//        ->get();
//
//dump($rows);echo 'process_queued_posts.php:21'; exit;


$queueTime = QueueTime::where('week_day', day_of_week($nowGmt))
    ->where('user_id', $processUserId)
    ->where('intraday_time', '<=', $nowGmt->format('H:i'))
    ->orderBy('intraday_time', 'DESC')
    ->with('publishedLog')
    ->first();


$publishedLog = $queueTime->publishedLog
    ->where('created_at', '>=', $nowGmt->format('Y-m-d 00:00:00'))
    ->first()
;

if ($publishedLog)
{
    return;
}

$postQueuedPlace =
    PostQueuedPlace::query()
        ->where('is_processed', 0)
        ->orderBy('order')
        ->first();

if (eN($postQueuedPlace))
{
    log_warn('Cant find PostQueuedPlace for user '. $processUserId);
}

$now = now_date();
$postQueuedPlace->process_started_time = date_mysql_format($now);
$postQueuedPlace->save();


$res = task(new PublishToSocialAccountsTask,
    [
        $postQueuedPlace
    ]
);

$postQueuedPlace->is_processed = 1;
$postQueuedPlace->save();

$queueTime->publishedLog()->attach
(
    $postQueuedPlace->id, [
        'created_at' => date_mysql_format(now_date())
    ]
);

dump($res);echo 'process_queued_posts.php:110'; exit;
