<?php

global $container;


use App\Facades\CU;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;


$res = task(new GetPinterestAccountsBoardsTask,
    [
        CU::user(), [12,13]
    ]
);

return [$res];
