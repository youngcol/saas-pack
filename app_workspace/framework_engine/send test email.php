<?php

global $container;


use App\Facades\CU;
use App\VO\EmailAddress;


$m = new \App\Emails\TestEmail(CU::user(), 'param1');
$ret = $m
    ->from('admin@convelly.com', 'Vlad Silchenko')
    ->to('webdevsol6@gmail.com')
    ->send();

return [$ret];
