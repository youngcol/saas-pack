<?php

global $container;


use App\Facades\CU;
use App\Tasks\Network\SendEmailTask;
use App\VO\EmailAddress;
use PHPMailer\PHPMailer\PHPMailer;

$toEmails = [];
$toEmails[] = new EmailAddress('webdevsol6@gmail.com');

$res = task(new SendEmailTask,
    [
        $toEmails,
        vo('custom'),
        vo('subject'),
        vo('mail message'),
    ]
);


