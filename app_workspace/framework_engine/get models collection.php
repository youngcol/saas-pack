<?php

global $container;

use App\Facades\CU;
use App\Models\SocialEngine\SocialNetwork;
use App\Tasks\GetModelsCollectionTask;

$res = task(new GetModelsCollectionTask(['with_total' => 1]),
    [
        CU::user(),
        vo(SocialNetwork::class),
        null,
        null,
        vo(1, 'numeric'),
        vo(2, 'numeric'),
    ]
);

dump($res);echo 'get models collection.php:19'; exit;
