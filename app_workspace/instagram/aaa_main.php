<?php

global $container;

$ig = new \InstagramAPI\Instagram();

try {
    $ig->login($username, $password);
} catch (\Exception $e) {
    echo 'Something went wrong: '.$e->getMessage()."\n";
    exit(0);
}
