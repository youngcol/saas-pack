<?php
declare(strict_types=1);
namespace App\Models;


use Illuminate\Database\Eloquent\Relations\HasMany;

trait UserTrait
{
    /**
     * @param string $password
     * @return string
     */
    public function encryptPassword(string $password): string
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * @param string $password
     * @return bool
     */
    public function checkPassword(string $password): bool
    {
        return password_verify($password, $this->password);
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword(string $password): Model
    {
        $this->password = $this->encryptPassword($password);
        return $this;
    }

    /**
     * Check user have permission
     * @param string $permission
     * @return bool
     */
    public function can(string $permission): bool
    {
        return strpos($this->permissions, $permission) !== false;
    }

    /**
     * Add permission to user
     * @param string $name
     */
    public function addPermission(string $name)
    {
        $permissions = explode(' ', $this->permissions);
        $permissions[] = strtolower($name);
        $this->permissions = implode(' ', array_unique($permissions));

        $this->save();
    }

    /**
     * @param string $name
     * @return bool
     */
    public function removePermission(string $name): bool
    {
        $permissions = array_filter(explode(' ', $this->permissions), function ($item) use ($name) {
            return $item !== $name;
        });

        $this->permissions = implode(' ', array_unique($permissions));
        $save = $this->save();

        return $save;
    }

    /**
     * Get user infos
     * @return HasMany
     */
    public function infos(): HasMany
    {
        return $this->hasMany(UserInfo::class);
    }

    /**
     * Get related info to user
     *
     * @param string $field
     * @return UserInfo|mixed
     */
    public function getInfo(string $field): UserInfo
    {
        $r = repo(UserInfo::class);
        $infoModel = $r->where(['field' => $field, 'user_id' => $this->id])->first();

        if (eN($infoModel))
        {
            $infoModel = new UserInfo([
                'field' => $field,
                'user_id' => $this->id
            ]);

            $infoModel->set([]);
            $infoModel->save();
        }

        return $infoModel;
    }
}
