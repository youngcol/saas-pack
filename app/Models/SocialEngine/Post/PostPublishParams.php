<?php

declare(strict_types=1);
namespace App\Models\SocialEngine\Post;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PostPublishParam
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property text $social_accounts
 * @property text $custom_params
 *
 * @package App\Model
 */
final class PostPublishParams extends Model
{
//    use SoftDeletes;

    protected $table = 'post_publish_params';

    protected $fillable = [
        'social_accounts',
        'custom_params',
        'post_id'
    ];

    protected $casts = [
        'social_accounts' => 'array',
        'custom_params' => 'array',
        'post_id' => 'integer'
    ];
}
