<?php
/**
 * Media file attached to post
 */

namespace App\Models\SocialEngine;

use App\Common\ValidationException;
use App\Models\Model;

class MediaFile extends Model
{
    protected $table = 'media_files';

    protected $fillable = [
        'type',
        'url',
        's3_url',
        'filename',
        'user_id'
    ];

    public function val()
    {
        if (empty($this->filename) && empty($this->s3_url))
        {
            throw new ValidationException('This model has corrupted data: '. json_decode($this, 1));
        }
    }

    public function isUploadedToS3()
    {
        return !empty($this->s3_url);
    }

    public function isUploadedLocally()
    {
        return !empty($this->filename);
    }
}
