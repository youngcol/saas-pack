<?php

declare(strict_types=1);
namespace App\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TaskLog
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property date $deleted_at
 * @property string $class_name
 * @property text $backtrace
 * @property text $args
 * @property string $duration
 * @property string $status
 *
 * @package App\Model
 */
final class TaskLog extends Model
{
    use SoftDeletes;

    protected $table = 'task_logs';

    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
        'class_name',
        'backtrace',
        'args',
        'duration',
        'status',
    ];

    /* These fields can be updated by UpdateModelsCollectionTask */
    protected $api_fillable = [

    ];

    // integer, real, float, double, decimal:<digits>, string, boolean,  object, array, collection, date, datetime, timestamp
    protected $casts = [
        'social_network_id' => 'integer'
    ];

}
