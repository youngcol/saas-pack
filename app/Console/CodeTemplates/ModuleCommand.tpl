<?php

namespace Module\<module>\Console\Commands;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class <command>Command extends Command
{
    /**
     * Configuration of command
     */
    protected function configure()
    {
        $this
            ->setName('<module_lowercase>:<command>')
            ->addArgument('arg1', InputArgument::REQUIRED, 'Arg help')
            ->setDescription('Module command: <command>')
        ;
    }

    /**
    * Execute method of command
    *
    * @param InputInterface $input
    * @param OutputInterface $output
    *
    * @return void
    */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['<comment>Welcome to the <command> coommand</comment>']);

        return;
    }
}
