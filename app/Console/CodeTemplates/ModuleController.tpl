<?php

namespace Module\<module>\Controller;

use App\Controller\BaseController;
use Slim\Http\Request;
use Slim\Http\Response;

class MainController extends BaseController
{
    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     */
    public function actionIndex(Request $request, Response $response, $args)
    {

        var_dump('MainController@actionIndex');
    }
}
