<?php

namespace Module\<module>\Providers;

use App\Providers\BaseServiceProvider;
use Pimple\Container;

final class MainServiceProvider extends BaseServiceProvider
{
    /**
     * Register database service provider.
     *
     * @param Container $container
     */
    public function register(Container $container)
    {
        $container['<module>_admin_sidebar'] = [
            'main_item' => '<module>',
            'sub_items' => [
                ['title' => 'Item1', 'url' => '/item1'],
                ['title' => 'Item2', 'url' => '/item2'],
            ]
        ];
    }
}