<?php
declare(strict_types=1);
namespace App\Console\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Console\Traits\CodeGenerate;

/**
 * <class>
 */
class <class> extends BaseCommand
{
    use CodeGenerate;

    /**
     * Configuration of command
     */
    protected function configure()
    {
        $this
            ->setName('<name>')
            ->setDescription('Command <name>')
            //->addOption(
            //    'iterations',
            //    null,
            //    InputOption::VALUE_REQUIRED,
            //    'How many times should the message be printed?',
            //    1
            //);
            //->addArgument('command_class', InputArgument::REQUIRED, 'What command class name?')
        ;
    }

    /**
    * @param InputInterface $input
    * @param OutputInterface $output
    * @return int|void|null
    * @throws Exception
    */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->executeMain($input, $output);
    }

    /**
    * @param InputInterface $input
    * @param OutputInterface $output
    * @throws Exception
    */
    protected function main(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['<info>Running command...</info>']);
        //$commandClass = $input->getArgument('command_class');
        //$commandClass = $input->getOption('option1');

    }
}
