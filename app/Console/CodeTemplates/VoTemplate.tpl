<?php

declare(strict_types=1);
namespace <namespace>;

use App\Common\VoException;
use App\VO\Vo;

class <class> extends Vo
{
    protected function val($val)
    {
        val($val, 'pinterest.oauth.scopes');

        //foreach ($scopes as $scope) {
        //    val($scope, 'pinterest.oauth.scopes');
        //}
    }
}
