<?php
declare(strict_types=1);
namespace <namespace>;

/**
 *
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\VoVal;

class <class> extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (

    )
    {
        list(
            $resGates
        ) = $this->gates
        (

        );
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=



        return new Res([
            'res' => ''
        ]);
    }

    /**
    *
    * @throws ValidationException
    */
    protected function gates
    (

    )
    {

        test_gate(true, '<class>');

        return [resOk()] ;
    }
}
