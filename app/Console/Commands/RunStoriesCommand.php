<?php
declare(strict_types=1);
namespace App\Console\Commands;

use App\Facades\db;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Console\Traits\CodeGenerate;

/**
 * RunStoriesCommand
 */
class RunStoriesCommand extends BaseCommand
{
    use CodeGenerate;

    /**
     * Configuration of command
     */
    protected function configure()
    {
        $this
            ->setName('run:stories')
            ->setDescription('Command run:stories')

            //->addOption(
            //    'iterations',
            //    null,
            //    InputOption::VALUE_REQUIRED,
            //    'How many times should the message be printed?',
            //    1
            //);
            //->addArgument('command_class', InputArgument::REQUIRED, 'What command class name?')
        ;
    }

    /**
    * @param InputInterface $input
    * @param OutputInterface $output
    * @return int|void|null
    * @throws Exception
    */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->executeMain($input, $output);
    }

    /**
    * @param InputInterface $input
    * @param OutputInterface $output
    * @throws Exception
    */
    protected function main(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['<info>Running command...</info>']);

        //$commandClass = $input->getArgument('command_class');
        //$commandClass = $input->getOption('option1');

        do {

            $stories = DB::table('stories')
                ->where('status', '<>', 'finished')
                ->get();

            $ids = implode(',', map($stories, function ($item) {
                return $item->id;
            }));

            $this->info($output, "Found stories: $ids");
            log_tag('run_php_stories', 'Found stories', $stories);


            foreach ($stories as $story)
            {
                $story = collect($story)->toArray();
                $storyObj = new $story['class_name'];

                log_tag(
                    'run_php_stories',
                    "Run story {$story['id']}, prev finished step {$story['finished_step']}"
                );

                $stepResult = null;
                $isError = false;
                try
                {
                    $stepResult = $storyObj->runStep($story);
                }
                catch(Exception $e)
                {
                    log_exception($e);
                    $isError = true;
                }

                log_tag(
                    'run_php_stories',
                    $isError
                        ? "Run story {$story['id']} next step FAILED"
                        : "Run story {$story['id']} next step success",
                    $stepResult
                );

            }

        }
        while(count($stories) > 0);

    }
}
