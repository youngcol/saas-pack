<?php

namespace App\Console\Commands\App;

use App\Console\Commands\BaseCommand;
use App\Models\Pet;
use Exception;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * RunTestCommand
 */
class RunTestCommand extends BaseCommand
{
    /**
     * Configuration of command
     */
    protected function configure()
    {
        $this
            ->setName('run:test')
            ->setDescription('Command for run tests')
            ->addOption(
                'iterations',
                null,
                InputOption::VALUE_REQUIRED,
                'How many times should the message be printed?',
                1
            );
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->executeMain($input, $output);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws Exception
     */
    protected function main(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['<info>Running command...</info>']);
        $iterations = $input->getOption('iterations');
        $this->log($output, 'goto main', [
            $iterations
        ]);

//        sleep(10);

//        throw new Exception('Custom exception');
    }
}
