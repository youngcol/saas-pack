<?php

namespace App\Console\Commands\App;

use App\Models\Biz;
use App\Models\ScrapeRequest;
use DOMDocument;
use DOMXPath;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * RunTestCommand
 */

class RunYelpScrapeRequestCommand extends Command
{
    /**
     * Configuration of command
     */
    protected function configure()
    {
        global $container;
        $this->container = $container;

        $this
            ->setName('yelp:scrape:run')
            ->setDescription('Run scrape request on yelp')
            ->addArgument('id', InputArgument::REQUIRED, 'Scrape request id?')
            ->addArgument('pagesCount', InputArgument::REQUIRED, 'How much list pages to scrape?')
        ;
    }

    /**
     * Execute method of command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $requestId = $input->getArgument('id');
        $pagesCount = $input->getArgument('pagesCount');
        //-=-=-=-=-=-=-=-=-=-=-=-=

        try
        {

            $this->run__($output, $requestId, $pagesCount);

        }
        catch (\Exception $e)
        {
            log_exception($e);

            $scrapeRequest = ScrapeRequest::find($requestId);
            $scrapeRequest->process_status = 'stopped';
            $scrapeRequest->save();
        }

    }

    protected function run__($output, $requestId, $pagesCount)
    {
        $scrapeRequest = ScrapeRequest::find($requestId);

        if (eN($scrapeRequest))
        {
            $output->writeln("Cant find scrape request with this id $requestId");
            $output->writeln("Exit 0");
            return 0;
        }

        $scrapeRequest->process_status = 'running';
        $scrapeRequest->save();

        $offset = $scrapeRequest->last_processed_list_index * $scrapeRequest->results_per_page;


        while($offset < $scrapeRequest->max_offset)
        {
            $output->writeln("<info>Processing request $requestId with page $scrapeRequest->last_processed_list_index</info>");

            $url = "https://www.yelp.com/search?cflt={$scrapeRequest->category->name}&find_loc=".urlencode($scrapeRequest->location->name)."&start=$offset";
            $listPage = $scrapeRequest->lists_pathname . "/list_$offset.html";
            $output->writeln("Get page url: <info>$url</info>");


            $this->container->downloader->getUrl($url, $listPage);
            $yelpLinks = $this->parseBusinessList($listPage);


            foreach ($yelpLinks as $index => $yelpLink)
            {
                $destFilename = $scrapeRequest->bizpages_pathname . "/biz_".basename($listPage, '.html')."_$index.html";
                $output->writeln("Get biz url: <info>$yelpLink</info>");
                $this->container->downloader->getUrl($yelpLink, $destFilename);
                $bizInfo = $this->scrapeBusinessInfo(file_get_contents($destFilename));

                $output->writeln("Storing business...");
                if (!eN($bizInfo['name']))
                {
                    Biz::create([
                        'name' => $bizInfo['name'],
                        'yelp_url' => $yelpLink,

                        'site_url' => $bizInfo['siteUrl'],
                        'phone' => $bizInfo['phone'],
                        'address' => $bizInfo['address'],
                        'yelp_reviews_count' => $bizInfo['reviews_count'],
                        'yelp_reviews' => $bizInfo['reviews_rating'],

                        'yelp_category_id' => $scrapeRequest->category->id,
                        'yelp_location_id' => $scrapeRequest->location->id,
                    ]);
                }

                usleep(500);
            }

            $scrapeRequest = ScrapeRequest::find($requestId);
            $scrapeRequest->last_processed_list_index++;
            $scrapeRequest->save();

            if ($scrapeRequest->process_status === 'stopping')
            {
                break;
            }

            if (--$pagesCount <= 0)
            {
                break;
            }

            $offset = $scrapeRequest->last_processed_list_index * $scrapeRequest->results_per_page;
        }


        $scrapeRequest->process_status = 'stopped';
        $scrapeRequest->save();

        $output->writeln("<info>Command finished</info>");
    }

    protected function parseBusinessList($fileName)
    {

        $matches = [];
        preg_match_all("|href=\"\/biz/(.*)\" target=\"\" name=\"|U", file_get_contents($fileName), $matches);

        $biz = [];
        foreach ($matches[1] as $item)
        {
            if (strstr($item, 'hrid') === false)
            {
                $biz['/biz/' . $item] = 1;
            }
        }

        return map(array_keys($biz), function ($item) {
            return 'https://www.yelp.com'. $item;
        });
    }

    protected function scrapeBusinessInfo($pageHtml)
    {
        $dom = new DOMDocument();
        @$dom->loadHTML($pageHtml);
        $xpath = new DOMXPath($dom);

        $name = $xpath->query('//*[@id="wrap"]/div[2]/div/div[1]/div/div[3]/div[1]/div[1]/h1')->item(0);
        $name2 = $xpath->query('//*[@id="wrap"]/div[2]/div/div[1]/div/div[3]/div[1]/div[1]/div/h1')->item(0);
        $siteUrl = $xpath->query('//*[@id="wrap"]/div[2]/div/div[1]/div/div[4]/div[1]/div/div[2]/ul/li[4]/span[2]/a')->item(0);
        $address = $xpath->query('//*[@id="wrap"]/div[2]/div/div[1]/div/div[4]/div[1]/div/div[2]/ul/li[1]/div/strong/address')->item(0);

        if (!$address)
        {
            $address = $xpath->query('//*[@id="wrap"]/div[2]/div/div[1]/div/div[4]/div[1]/div/div[2]/ul/li[1]/div/address')->item(0);
        }

        $phone = $xpath->query('//*[@id="wrap"]/div[2]/div/div[1]/div/div[4]/div[1]/div/div[2]/ul/li[3]/span[3]')->item(0);
        if (!$phone)
        {
            $phone = $xpath->query('//*[@id="wrap"]/div[2]/div/div[1]/div/div[4]/div[1]/div/div[2]/ul/li[2]/span[3]')->item(0);
        }

        if (!$siteUrl)
        {
            $siteUrl = $xpath->query('//*[@id="wrap"]/div[2]/div/div[1]/div/div[4]/div[1]/div/div[2]/ul/li[3]/span[2]/a')->item(0);
        }

        $reviewsCount = $xpath->query('//*[@id="wrap"]/div[2]/div/div[1]/div/div[3]/div[1]/div[2]/div[1]/div[1]/span')->item(0);
        $reviewsRating = $xpath->query('//*[@id="wrap"]/div[2]/div/div[1]/div/div[3]/div[1]/div[2]/div[1]/div[1]/div/@title')->item(0);


        return [
            'name' =>  $name ? $name->textContent . ($name2 ? $name2->textContent : '') : null,
            'siteUrl' =>  $siteUrl ? 'http://'. $siteUrl->textContent : '',
            'phone' =>  $phone ? trim($phone->textContent) : '',
            'address' =>  $address ? trim($address->textContent) : '',
            'reviews_count' =>  $reviewsCount ? (int)$reviewsCount->textContent : '',
            'reviews_rating' =>  $reviewsRating ? (float)$reviewsRating->textContent : ''
        ];
    }

}
