<?php
declare(strict_types=1);
namespace App\Console\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Console\Traits\CodeGenerate;

/**
 * PrepareProductionBuildCommand
 */
class PrepareProductionBuildCommand extends BaseCommand
{
    use CodeGenerate;

    /**
     * Configuration of command
     */
    protected function configure()
    {
        $this
            ->setName('prepare:production:build')
            ->setDescription('Command prepare:production:build')
            //->addOption(
            //    'iterations',
            //    null,
            //    InputOption::VALUE_REQUIRED,
            //    'How many times should the message be printed?',
            //    1
            //);
            //->addArgument('command_class', InputArgument::REQUIRED, 'What command class name?')
        ;

        $this->removeFiles = [
            'phpdox.phar',
            'phpdox.xml',
            'README.md',
            'README_FRAMEWORK_ARCHITECTURE.md',
            'gulpfile.js',
            'webpack.mix.js',
            '.env.example'
        ];

        $this->removeFolders = [
            'app_workspace',
            'app_vue',
            'development',
            'docs',
            'lang',
            'temp',
            'scripts'
        ];

        $this->removeCommands = [
            'Admin/CompileLanguagesCommand',
            'Admin/DatabaseDropCommand',
            'Admin/DatabaseTruncateCommand',

            'GenerateCommandCommand',
            'GenerateDocsCommand',
            'GenerateMigrationCommand',
            'GenerateModelCommand',
            'GenerateSchemaCommand',
            'GenerateSeedCommand',
            'GenerateTaskCommand',
            'GenerateVOCommand',
        ];

    }

    /**
    * @param InputInterface $input
    * @param OutputInterface $output
    * @return int|void|null
    * @throws Exception
    */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->executeMain($input, $output);
    }

    /**
    * @param InputInterface $input
    * @param OutputInterface $output
    * @throws Exception
    */
    protected function main(InputInterface $input, OutputInterface $output)
    {

//        // remove files
//        map($this->removeFiles, function ($file) {
//            unlink(PATH_ROOT . $file);
//        });
//
//        // remove folders
//        map($this->removeFolders, function ($folder) {
//            exec('rm -rf '. PATH_ROOT . $folder);
//        });

        // remove commands
        map($this->removeCommands, function ($command) {
            exec('unlink '. COMMANDS_PATH . '/' . $command . '.php');
        });

//        $output->writeln(['<info>Running command...</info>']);
        //$commandClass = $input->getArgument('command_class');
        //$commandClass = $input->getOption('option1');

    }
}
