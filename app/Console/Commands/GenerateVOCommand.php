<?php

namespace App\Console\Commands;

use App\Common\Helper;
use Symfony\Component\Console\Command\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Console\Traits\CodeGenerate;
/**
 * GenerateVOCommand
 */
class GenerateVOCommand extends Command
{
    use CodeGenerate;

    /**
     * Configuration of command
     */
    protected function configure()
    {
        $this
            ->setName('generate:vo')
            ->setDescription('Command generate:vo')
            ->addArgument('vo_class_name', InputArgument::REQUIRED, 'What vo class folder path?')
            ->setHelp('php partisan generate:vo vo_class_name</info>')
        ;
    }

    /**
     * Execute method of command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['<comment>Welcome to the seed generator</comment>']);
        $taskClass = $input->getArgument('vo_class_name');

        $pathnameInfo = Helper::classPathname($taskClass);

        $path = $this->getPath($pathnameInfo['basename'], VO_PATH);
        create_folder_tree_by_pathname(dirname($path));

        $namespaceReplacements = 'App\VO';
        if (count($pathnameInfo['namespace'])) {
            $namespaceReplacements .= '\\' . implode('\\', $pathnameInfo['namespace']);
        }

        $placeHolders = [
            '<class>',
            '<namespace>'
        ];
        $replacements = [
            $pathnameInfo['className'],
            $namespaceReplacements
        ];

        $this->generateCode($placeHolders, $replacements, 'VoTemplate.tpl', $path);
        $output->writeln(sprintf('Generated new VO class to "<info>%s</info>"', realpath($path)));

        return;
    }

    /**
     * @param string $baseName
     * @param string $dir
     * @return string
     * @throws \Exception
     */
    private function getPath($baseName, $dir)
    {
        $dir = rtrim($dir, '/');
        if (!file_exists($dir)) {
            throw new \Exception(sprintf('Commands directory "%s" does not exist.', $dir));
        }

        return $dir.'/'.$baseName;
    }
}
