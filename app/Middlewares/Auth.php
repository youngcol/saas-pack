<?php

namespace App\Middlewares;

use App\Facades\CU;
use App\Facades\session as sess;
use App\Models\Session;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

use App\Models\User;

class Auth extends Middleware
{
    /**
     * @var \App\VO\VoVal|null
     */
    protected $context = null;

    /**
     * @var \App\VO\VoVal|null
     */
    protected $mode = null;

    public function __construct($container, $mode='web', $context='app')
    {
        $this->context = vo($context, 'middleware.context');
        $this->mode = vo($mode, 'middleware.mode');

        parent::__construct($container);
    }

	public function __invoke(Request $request, Response $response, callable $next)
	{
	    $kitId = sp($_COOKIE, '_kitid');

	    if (eN($kitId))
	    {
            $this->container->flash->addMessage('warning', 'Not logged in.');
            sess::set('is_authorized', false);

            if ($this->mode == 'web')
            {
                return redirect('signin');
            }
            else
            {
                return json_401('Unauthorized. Please use authorisation form.');
            }
	    }

		// search the current session uniqid in the session table
		$session = Session::where('uniqid', '=', $kitId)->first();

		if(eN($session))
		{
            $this->container->flash->addMessage('warning', 'Session not found.');
            sess::set('is_authorized', false);

            if ($this->mode == 'web')
            {
                return redirect('signin');
            }
            else
            {
                return json_401('Unauthorized. Session not found.');
            }
		}

        sess::set('is_authorized', true);

        $currentUser = User::findOrFail($session->user_id);

		// when php session expired and custom auth session is still working
		if (sess::get('uniqid') !== $kitId)
		{
            sess::fill([
                'uniqid' => $kitId
            ]);
		}

        if ($this->context === 'admin')
        {
            if (!$currentUser->is_admin)
            {
                return redirect( 'signin');
            }
        }

        /** current user facade */
        CU::setup($currentUser);

		return $next($request, $response);
	}
}
