<?php


namespace App\Stories;

use App\Facades\db;
use function _\findIndex;


abstract class Story
{
    protected $steps = [];
    protected $file = null;
    protected $class = null;

    public function __construct($steps, $file, $class)
    {
        $this->steps = $steps;
        $this->file = $file;
        $this->class = $class;
    }

    public function create(array $input)
    {

        $inputSerialized = serialize($input);

        $ins = DB::table('stories')
            ->insert([
                'class_name' => $this->class,
                'version' => filemtime($this->file),

                'args' => $inputSerialized,
                'status' => 'new',
                'next_step_params' => $inputSerialized
            ]);

        return $ins;
    }

    public function runStep($storyState)
    {
        $storyState['next_step_params'] = unserialize($storyState['next_step_params']);

        if (eN($storyState['finished_step']))
        {
            $nextIndex = 0;
        }
        else
        {
            $nextIndex = findIndex($this->steps, function($o) use ($storyState) {return $o == $storyState['finished_step'];});
            $nextIndex += 1;
        }


        $stepFunction = sp($this->steps, $nextIndex);

        log_tag(
            'story_' . $storyState['class_name'],
            "ID:{$storyState['id']} --> Run step: `$stepFunction`",
            $storyState['next_step_params']
        );

        $stepReturn = $this->$stepFunction($storyState['next_step_params']);

        $update = [
            'next_step_params' => serialize($stepReturn),
            'finished_step' => $stepFunction,
            'status' => eN(sp($this->steps, $nextIndex + 1)) ? 'finished' : 'running'
        ];

        $upd = DB::table('stories')
            ->where('id', '=', $storyState['id'])
            ->update($update);

        return $update;
    }
}
