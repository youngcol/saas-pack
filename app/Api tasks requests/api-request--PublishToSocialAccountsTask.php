<?php

use App\Facades\CU;
use App\Models\SocialEngine\Post;use App\Models\SocialEngine\SocialNetwork;
use App\Tasks\SocialEngine\AddSocialNetworkAccountTask;
use App\Tasks\SocialEngine\CreatePostTask;
use App\Tasks\SocialEngine\Publish\PublishToSocialAccountsTask;use App\VO\Post\PostInstantlyPublish;use App\VO\Post\PublishParams;
use App\VO\User\MediaFiles;
use App\VO\User\SocialAccounts;

$params = [
    'post_id' => 'required|numeric',
];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params) {


        $r = repo(Post::class);
        $post = $r->where(['user_id' => CU::user()->id, 'id' => $params['post_id']])->first();

        if (eN($post))
        {
            return bad_request_error(['Current user doesn\'t have post with such id']);
        }

        $res = task(new PublishToSocialAccountsTask,
            [
                new PostInstantlyPublish(
                    $post,
                    $post->scheduledTime->publishParams
                )
            ]
        );

        return $res;
    }
];
