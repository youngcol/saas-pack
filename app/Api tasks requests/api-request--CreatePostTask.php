<?php

use App\Facades\CU;
use App\Models\SocialEngine\SocialNetwork;
use App\Tasks\SocialEngine\AddSocialNetworkAccountTask;
use App\Tasks\SocialEngine\CreatePostTask;
use App\VO\Post\PublishParams;
use App\VO\User\MediaFiles;
use App\VO\User\SocialAccounts;

$params = [
    'text' => 'filled.string',
    'link' => 'filled.string',
    'media_files' => 'ids.array',
    'creation_type' => 'required|post.publish_params.publish_type',
    'social_accounts' => 'required|ids.array',
    'scheduled_time' => 'datetime',
    'publish_params' => 'required',
];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params) {

        $publishParams = json_decode($params['publish_params'], true);

        if ($params['scheduled_time']->isFilled())
        {
            $publishParams['scheduled_time'] = $params['scheduled_time']->_();
        }

        $publishParams = new PublishParams($params['creation_type'], $publishParams);
        $socialAccounts = new SocialAccounts(CU::user(), $params['social_accounts']->toArray());


        $res = task(new CreatePostTask,
            [
                CU::user(),
                $params['text'],
                $params['link'],
                new MediaFiles(CU::user(), $params['media_files']->_()),
                $socialAccounts,
                $publishParams
            ]
        );

        return $res;
    }
];
