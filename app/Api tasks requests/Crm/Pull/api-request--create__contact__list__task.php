<?php


use App\Facades\CU;
use App\Tasks\Crm\create__contact__list__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;

$params = [
    'accounts' => 'required|ids.array',
];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params)
    {

        $res = task(new create__contact__list__task,
            [
                CU::user(),
                vo('name')
            ]
        );

        return $res;
    }
];
