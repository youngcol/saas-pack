<?php

use App\Facades\CU;
use App\Models\SocialEngine\SocialNetwork;
use App\Tasks\SocialEngine\AddSocialNetworkAccountTask;

$params = [
    'username' => 'required|string',
    'password' => 'required|string',
    'social_network' => 'social.networks',
];

return [
// only users with this permissions allowed
'allowed_permissions' => [],
'params' => $params,
'runner' => function (array $params) {


    $res = task(new AddSocialNetworkAccountTask,
        [
            CU::user(),
            SocialNetwork::find(1),
            $params['username'],
            $params['password'],
        ]
    );

    return $res;
}
];
