<?php


use App\Facades\CU;
use App\Tasks\Crm\create__contact__task;

$params = [
    'name' => 'required|filled.string',
    'email' => 'email',
    'phone' => 'string',
    'address' => 'string',
    'site_url' => 'string',
    'facebook_link' => 'string',
    'linkedin_link' => 'string',
    'success_item' => 'string',
    'greet_item' => 'string',
];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params) {

        $res = task(new create__contact__task,
            [
                $params['name'],
                $params['email'],
                $params['phone'],
                $params['site_url'],
                $params['address'],
                $params['facebook_link'],
                $params['linkedin_link'],
                $params['success_item'],
                $params['greet_item'],
            ]
        );

        return $res;
    }
];
