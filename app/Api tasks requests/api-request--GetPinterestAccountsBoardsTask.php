<?php


use App\Facades\CU;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;

$params = [
    'accounts' => 'required|ids.array',
];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params) {

        $res = task(new GetPinterestAccountsBoardsTask,
            [
                CU::user(),
                $params['accounts']->toArray()
            ]
        );

        return $res;
    }
];
