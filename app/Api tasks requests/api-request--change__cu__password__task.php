<?php


use App\Facades\CU;
use App\Tasks\change__cu__password__task;
use App\Tasks\SocialEngine\GetPinterestAccountsBoardsTask;

$params = [

    'oldPass' => 'required|user.password',
    'newPass' => 'required|user.password',
    'newPass2' => 'required|user.password|same:newPass',
];

return [
// only users with this permissions allowed
    'allowed_permissions' => [],
    'params' => $params,
    'runner' => function (array $params) {

        $res = task(new change__cu__password__task,
            [
                CU::user(),
                $params['oldPass'],
                $params['newPass'],
                $params['newPass2']
            ]
        );

        return $res;
    }
];
