<?php

declare(strict_types=1);
namespace App\Facades;

use Illuminate\Database\Capsule\Manager as Capsule;

class db
{
    public static function table($table)
    {
        return Capsule::table($table);
    }

    public static function select($sql, $params=[])
    {
        return Capsule::connection()->select($sql, $params);
    }

    public static function raw($sql)
    {
        return Capsule::connection()->raw($sql);
    }

    public static function statement($sql)
    {
        return Capsule::connection()->statement($sql);
    }
}
