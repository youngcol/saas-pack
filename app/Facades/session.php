<?php

declare(strict_types=1);
namespace App\Facades;

class session
{
    public static function get($key, $def=null)
    {
        return sp($_SESSION, $key, $def);
    }

    public static function set($key, $val)
    {
        $_SESSION[$key] = $val;
    }

    public static function fill($data)
    {
        $_SESSION = array_merge($_SESSION, $data);
    }

}
