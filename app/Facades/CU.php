<?php
/**
 * Current user authorized with session
 */
declare(strict_types=1);
namespace App\Facades;

use App\Models\User;

class CU
{
    /** App\Models\User $user */
    protected static $user = null;

    /**
     * @return bool
     */
    public static function setup(User $currentUser = null)
    {
        self::$user = $currentUser;

        return eN(self::$user);
    }

    /**
     * @return mixed
     */
    public static function id()
    {
        return optional(self::user())->id;
    }

    /**
     * @return User|null
     */
    public static function user()
    {
        return self::$user;
    }

    /**
     * @return bool|null
     */
    public static function isAuthorized()
    {
        return session::get('is_authorized', false);
    }

    /**
     * @example can('run watch go view')
     * @example can('watch')
     *
     * @param string $permission
     * @return bool
     */
    public static function can(string $permission): bool
    {
        if (eN(self::$user))
        {
            return false;
        }
        //-=-=-=-=-=-=-=-=-=-=-=-=

        foreach (explode(' ', $permission) as $item)
        {
            if (!self::$user->can($item))
            {
                return false;
            }
        }

        return true;
    }
}
