<?php

namespace App\Common;


class GateException extends \Exception
{
    protected $data = [];

    public function __construct($message, $data = [])
    {
        parent::__construct($message);

        $this->data = $data;
    }
}

