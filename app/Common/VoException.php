<?php
namespace App\Common;

use \Neomerx\JsonApi\Encoder\Encoder;
use \Neomerx\JsonApi\Document\Error;

class VoException extends \Exception
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $detail;

    /**
     * JsonException constructor.
     *
     * @param string $type
     * @param int    $statusCode
     * @param string $title
     * @param string $detail
     */
    public function __construct($title, $detail='')
    {
        $this->title      = $title;
        $this->detail     = $detail;

        $this->message = $title . "  --|--  " . $detail;
    }

    /**
     * JsonApi encode error
     *
     * @return string
     */
    public function encodeError()
    {
        $error = new Error(
            null,
            null,
            null,
            null,
            $this->title,
            $this->detail
        );

        return Encoder::instance()->encodeError($error);
    }
}
