<?php


namespace App\Emails;

use App\Tasks\Network\SendEmailTask;
use App\VO\EmailAddress;

abstract class Email
{
    protected $c;

    protected $subject=null;
    protected $message=null;

    protected $from=null;
    protected $toAddresses=[];


    protected $fromEmail=null;
    protected $fromName=null;

    public function __construct()
    {
        global $container;
        $this->c = $container;

    }

    abstract protected function getSubject();
    abstract protected function getMessageBody();

    public function to($email, $name=null)
    {
        $this->toAddresses[] = new EmailAddress($email, $name);

        return $this;
    }

    public function from(string $email, string $name=null)
    {
        $this->fromEmail = $email;
        $this->fromName = $name;

        return $this;
    }

    public function send()
    {
        $this->validateEmail();

        $messageBody = $this->getMessageBody();
        $subject = $this->getSubject();

        $renderParams = [
            'app_name' => settings('name'),
            'subject' => $subject,
            'messageBody' => $messageBody
        ];

        $emailHtml = render('emails/layout', $renderParams);


        $res = task(new SendEmailTask,
            [
                $this->toAddresses,
                new EmailAddress($this->fromEmail, $this->fromName),
                vo($subject, 'required|filled.string'),
                vo($emailHtml, 'required|filled.string')
            ]
        );

        return [
            'resSendEmailTask' => $res
        ];
    }

    protected function validateEmail()
    {
        if (eN($this->fromEmail))
        {

        }
    }

}
