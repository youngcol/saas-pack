<?php


namespace App\Emails;


use App\Models\User;

class TestEmail extends Email
{
    protected $param;

    public function __construct(User $user, $param)
    {
        parent::__construct();

        $this->user = $user;
        $this->param = $param;

    }

    protected function getSubject()
    {
        return sprintf2(
            'Test subject [user] [date]',
            [
                'user' => $this->user->name,
                'date' => date('r')
            ]
        );
    }


    protected function getMessageBody()
    {
        $tmpl = 'emails/test_email';

        $emailHtml = render($tmpl, [
            'param' => $this->param
        ]);

        return $emailHtml;
    }
}
