<?php
declare(strict_types=1);
namespace App\Tasks\MediaFile;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\SocialEngine\MediaFile;
use App\Tasks\Task;

class UploadMediaToS3Task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run(MediaFile $mediaFile)
    {

        $res = $this->c->file_uploader->uploadMediaToS3(
            $mediaFile->filename,
            $mediaFile->filename,
            settings('amazon.s3.media_bucket')
        );

        $mediaFile->s3_url = $res->aws->get('ObjectURL');
        $mediaFile->save();

        return new Res([
            'mediaFile' => $mediaFile
        ]);

    }
}
