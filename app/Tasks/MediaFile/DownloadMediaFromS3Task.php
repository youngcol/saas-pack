<?php
declare(strict_types=1);
namespace App\Tasks\MediaFile;

use App\Common\Res;
use App\Models\SocialEngine\MediaFile;
use App\Tasks\Task;

class DownloadMediaFromS3Task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run(MediaFile $mediaFile)
    {

        $basename = basename($mediaFile->s3_url);

        $destFileName = implode(
                '/',
                [
                    settings('upload_media_temp_folder'),
                    $basename
                ]
            );

        if (!file_exists($destFileName))
        {
            $this->c->file_uploader->downloadFileFromUrl(
                $mediaFile->s3_url,
                $destFileName
            );
        }

        return new Res([
            'mediaFile' => $mediaFile,
            'destFileName' => $destFileName
        ]);

    }
}
