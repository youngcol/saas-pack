<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 * Get models collection task (acts as repository)
 */

use App\Common\Res;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Capsule\Manager as Capsule;

class GetModelsCollectionTask extends Task
{
    public function __construct($context=[])
    {
        parent::__construct();

        $this->context = $context;

        vo(sp($this->context, 'with_is_next'), 'numeric');
        vo(sp($this->context, 'with_total'), 'numeric');
    }

    public function run
    (
        User $cu,
        VoVal $modelName,
        VoVal $relations = null,
        VoVal $where = null,
        VoVal $page = null,
        VoVal $perPage = null,
        VoVal $sortBy = null,
        VoVal $order = null
    )
    {
        ifNullDef($where, vo([]));
        ifNullDef($relations, vo([]));
        ifNullDef($page, vo(0));
        ifNullDef($perPage, vo(100));
        //-=-=-=-=-=-=-=-=-=-=-=-=


        $name = $modelName->_();
        if (!class_exists($name))
        {
            return resFail('',"Model doesn't exists", ['model_name' => $name]);
        }
        //-=-=-=-=-=-=-=-=-=-=-=-=
        global $dictionary_models;
//        dump([$dictionary_models, $name]);echo 'GetModelsCollectionTask.php:52'; exit;
        $isDictionaryModel = in_array($name, $dictionary_models);

        $model = new $name;
        $q = $model::query();

        if (!$isDictionaryModel)
        {
            $q->where('user_id', '=', $cu->id);
        }

        foreach ($where->toArray() as $item)
        {
            $q->where($item[0], $item[1], $item[2]);
        }

        $total = null;
        if (sp($this->context, 'with_total'))
        {
            $total = $q->count();
        }
        
        if ($page)
        {
            $limit = $perPage->toInt();

            if (sp($this->context, 'with_is_next'))
            {
                $limit++;
            }

            $q->offset($page->toInt() * $perPage->toInt())
                ->limit($limit);
        }
        

        if ($relations)
        {
            $withModels = $relations->toArray();

            if ($withModels and count($withModels))
            {
                $q->with($withModels);
            }
        }

        if ($sortBy && $sortBy->isFilled())
        {
            $q->orderBy($sortBy->_(), $order->_());
        }
        
//        else
//        {
//            $q->orderBy('id', $order->_());
//        }

        $isNextExists = false;

        Capsule::enableQueryLog();
        $list = $q->get();
        $query = Capsule::getQueryLog();

        if (sp($this->context, 'with_is_next'))
        {
            if ($list->count() == $limit)
            {
                $isNextExists = true;
                $list->pop();
            }
        }

        return new Res([
            'total' => $total,
            'list' => $list,
            'is_next_exists' => $isNextExists,
            'perPage' => $perPage->_(),
            'page' => $page->_(),
            'relations' => $relations->_(),
            'sort_by' => $sortBy ? $sortBy->_() : '',
            'order' => $order ? $order->_() : '',
            'model_name' => $modelName->_(),
            'query' => $query
        ]);
    }
}
