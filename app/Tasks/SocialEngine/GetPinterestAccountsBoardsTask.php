<?php
declare(strict_types=1);
namespace App\Tasks\SocialEngine;

/**
 *
 *
 */

use App\Common\Res;
use App\Facades\CU;
use App\Models\SocialEngine\UserSocialAccount;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;

class GetPinterestAccountsBoardsTask extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        User $user,
        array $accountIds
    )
    {
        $boards = [];
        $socialAccounts = $user->socialAccount()->whereIn('id', $accountIds)->get();
        $boardsInfo = $user->getInfo('pinterest.accounts.boards');

        foreach ($socialAccounts as $socialAccount)
        {
            /** @var UserSocialAccount $socialAccount */
            $list = $this->c->pinner_b->boards->forUser($socialAccount->username);

            $list = map($list, function ($item) {
                return [
                    'id' => $item['id'],
                    'name' => $item['name'],
                ];
            });

            $boards[] = [
                'account_id' => $socialAccount->id,
                'username' => $socialAccount->username,
                'list' => $list
            ];

            $boardsInfo->setField($socialAccount->id, $list);
        }

        // update user info
        $boardsInfo->save();

        return new Res([
            'boards' => $boards
        ]);
    }
}
