<?php
declare(strict_types=1);
namespace App\Tasks\SocialEngine;

/**
 * Get last posts list
 *
 */
use App\Common\Res;
use App\Models\SocialEngine\Post;
use App\Models\SocialEngine\Post\PostPublishLog;
use App\Models\SocialEngine\QueueTime;
use App\Tasks\Task;
use App\VO\VoVal;
use Carbon\Carbon;
use Illuminate\Support\Collection;

trait SocialEngineTrait {


    function get_next_queue_time(Carbon $now): Carbon
    {
        $ret = $now->copy();

        $nextTime = QueueTime::where('week_day', settings("week_days.$now->dayOfWeekIso"))
            ->where('intraday_time', '>', $now->format('H:i:00'))
            ->first();

        if ($nextTime)
        {

        }
        else
        {
            $nextDay = sp(settings("week_days"), $now->dayOfWeekIso+1, settings("week_days.1"));
            $nextTime = QueueTime::where('week_day', $nextDay)->first();

            $ret->add(1, 'day');
        }

        $ret->hour = $nextTime->time_parts[0];
        $ret->minute = $nextTime->time_parts[1];

        return $ret;
    }

}
