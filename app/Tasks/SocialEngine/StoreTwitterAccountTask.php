<?php
declare(strict_types=1);
namespace App\Tasks\SocialEngine;

use App\Common\Res;
use App\Common\ResFail;
use App\Facades\CU;
use App\Models\SocialEngine\SocialNetwork;
use App\Models\SocialEngine\UserSocialAccount;
use App\Tasks\Task;
use App\VO\VoVal;

class StoreTwitterAccountTask extends Task
{
    public const step1 = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function run(VoVal $oauthVerifier)
    {
        $resRequest =  $this->c->twitter->getUserRequestTokenInfo();
        $oauth_token = vo($resRequest->oauth_token, 'filled.string');
        $oauth_token_secret = vo($resRequest->oauth_token_secret, 'filled.string');

        $this->c->twitter->makeConnection($oauth_token, $oauth_token_secret);
        $resToken = $this->c->twitter->getUserAccessToken($oauthVerifier);

        UserSocialAccount::where([
            'social_network_id' => SocialNetwork::TWITTER,
            'user_id' => CU::user()->id,
            'social_account_id' => $resToken->token['user_id']
        ])->delete();

        $userSocialAccount = UserSocialAccount::create([
            'social_network_id' => SocialNetwork::TWITTER,
            'user_id' => CU::user()->id,
            'access_token' => $resToken->token['oauth_token'],
            'access_token_secret' => $resToken->token['oauth_token_secret'],
            'username' => $resToken->token['screen_name'],
            'social_account_id' => $resToken->token['user_id'],
            'profile_image' => '',
        ]);

        $this->c->twitter->makeConnection(
            vo($resToken->token['oauth_token'], 'filled.string'),
            vo($resToken->token['oauth_token_secret'], 'filled.string')
        );

        $resMe = $this->c->twitter->getMeUser();
        $userSocialAccount->profile_image = $resMe->user->profile_image_url_https;
        $userSocialAccount->save();

        return new Res([
            'userSocialAccount' => $userSocialAccount
        ]);

    }
}
