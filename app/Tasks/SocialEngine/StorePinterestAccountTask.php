<?php
declare(strict_types=1);
namespace App\Tasks\SocialEngine;

use App\Common\Res;
use App\Common\ResFail;
use App\Facades\CU;
use App\Models\SocialEngine\SocialNetwork;
use App\Models\SocialEngine\UserSocialAccount;
use App\Tasks\Task;
use App\VO\VoVal;

class StorePinterestAccountTask extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run(VoVal $code)
    {

        $token = $this->c->pinner->auth->getOAuthToken((string)$code);
        $this->c->pinner->auth->setOAuthToken($token->access_token);
        $resUser = $this->c->pinner->getMeUser();

        UserSocialAccount::where([
            'social_network_id' => SocialNetwork::PINTEREST,
            'user_id' => CU::user()->id,
            'social_account_id' => $resUser->me->id
        ])->delete();

        $userSocialAccount = UserSocialAccount::create([
            'social_network_id' => SocialNetwork::PINTEREST,
            'user_id' => CU::user()->id,
            'access_token' => $token->access_token,
            'username' => $resUser->me->username,
            'social_account_id' => $resUser->me->id,
            'profile_image' => $resUser->me->image['large']['url'],
        ]);

        $ret = new Res([
            'userSocialAccount' => $userSocialAccount
        ]);

        return $ret;
    }
}
