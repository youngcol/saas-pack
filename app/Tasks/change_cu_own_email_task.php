<?php
declare(strict_types=1);
namespace App\Tasks;

/**
 * Current user updates own email
 *
 */

use App\Common\Res;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;


class change_cu_own_email_task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        User $user,
        VoVal $email
    )
    {

        $upd = repo(User::class)
                    ->where('id', $user->id)
                    ->update([
                       'is_email_verified'  => 0,
                       'email'              => $email->_()
                    ]);

        // send email


        return new Res([
            'upd' => $upd,
        ]);
    }
}
