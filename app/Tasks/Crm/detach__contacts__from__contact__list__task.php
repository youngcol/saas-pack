<?php
declare(strict_types=1);
namespace App\Tasks\Crm;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Crm\ContactList;
use App\Tasks\Task;
use App\VO\VoVal;

class detach__contacts__from__contact__list__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        ContactList $contactList,
        VoVal $contactIds
    )
    {

        $contactList->contacts()->detach($contactIds->_());
        $contactList->save();


        return new Res([
            'res' => $contactList
        ]);
    }
}
