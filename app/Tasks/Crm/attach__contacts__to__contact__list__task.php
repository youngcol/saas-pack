<?php
declare(strict_types=1);
namespace App\Tasks\Crm;

/**
 *
 *
 */

use App\Common\Res;
use App\Models\Crm\ContactList;
use App\Tasks\Task;
use App\VO\VoVal;

class attach__contacts__to__contact__list__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        ContactList $contactList,
        VoVal $contactIds
    )
    {

        $res = task(new detach__contacts__from__contact__list__task,
            [
                $contactList,
                $contactIds
            ]
        );


        foreach ($contactIds->_() as $contactId)
        {
            $contactList->contacts()->attach($contactId);
        }

        $contactList->save();

        return new Res([
            'res' => $contactList
        ]);
    }
}
