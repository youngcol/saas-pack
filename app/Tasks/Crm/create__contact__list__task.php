<?php
declare(strict_types=1);
namespace App\Tasks\Crm;

use App\Common\Res;
use App\Models\Crm\ContactList;
use App\Models\User;
use App\Tasks\Task;
use App\VO\VoVal;

/**
 * Class create__contact__list__task
 * @package App\Tasks\Crm
 */
class create__contact__list__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param User $user
     * @param VoVal $name
     * @return Res
     */
    public function run
    (
        User $user,
        VoVal $name
    )
    {

        $model = new ContactList();
        $model->name = $name->_();
        $model->user()->associate($user);
        $model->save();


        return new Res([
            'contactListModel' => $model
        ]);
    }
}
