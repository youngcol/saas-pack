<?php
declare(strict_types=1);
namespace App\Tasks\Crm;

/**
 *
 *
 */

use App\Common\Res;
use App\Common\ValidationException;
use App\Models\Crm\Contact;
use App\Tasks\Task;
use App\VO\VoVal;
use Rakit\Validation\Rule;

class create__contact__task extends Task
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run
    (
        VoVal $name,
        VoVal $email,
        VoVal $phone,
        VoVal $siteUrl,
        VoVal $address,
        VoVal $facebookLink,
        VoVal $linkedinLink,
        VoVal $successItem,
        VoVal $greetItem
    )
    {
        list(
            $resGates
            ) = $this->gates
            (
                $address,
                $address

            );
        //-=-=-=-=-=- END GATES =-=-=-=-=-=-=



        // code here

        $contact = new Contact();
        $contact->name = $name->_();
        $contact->email = $email->_();
        $contact->site_url = $siteUrl->_();
        $contact->phone = $phone->_();
        $contact->address = $address->_();
        $contact->facebook_link = $facebookLink->_();
        $contact->linkedin_link = $linkedinLink->_();
        $contact->success_item = $successItem->_();
        $contact->greet_item = $greetItem->_();

        $contact->save();


        return new Res([
            'contact' => $contact
        ]);
    }

    /**
     *
     * @throws ValidationException
     */
    protected function gates
    (
        VoVal $address,
        VoVal $address2
    )
    {

        throw new ValidationException('ValidationException');

        return resOk([

        ]) ;
    }
}
