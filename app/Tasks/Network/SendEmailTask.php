<?php
declare(strict_types=1);
namespace App\Tasks\Network;

/**
 * Send email task
 *
 */

use App\Common\Res;
use App\Tasks\Task;
use App\VO\EmailAddress;
use App\VO\VoVal;

use PHPMailer\PHPMailer\PHPMailer;

/**
 * Class SendEmailTask
 * @package App\Tasks\Network
 */
class SendEmailTask extends Task
{
    protected $mailer;

    public function __construct()
    {
        parent::__construct();

    }

    public function run
    (
        array $toAddresses,
        VoVal $from,
        VoVal $subject,
        VoVal $message
    )
    {

        $res = $this->phpMailer($toAddresses, $from, $subject, $message);

        return $res;

    }

    protected function phpMailer(
        array $toAddresses,
        VoVal $from,
        VoVal $subject,
        VoVal $message
    )
    {

        $mail = new PHPMailer(true);
        //$mail->SMTPDebug = 2;                                       // Enable verbose debug output
        $mail->isSMTP();
        $mail->Host = settings('mails.custom.host');// Set mailer to use SMTP
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = settings('mails.custom.username'); // SMTP username
        $mail->Password   = settings('mails.custom.password'); // SMTP password
        $mail->SMTPSecure = settings('mails.custom.encryption'); // Enable TLS encryption, `ssl` also accepted
        $mail->Port       = settings('mails.custom.port');  // TCP port to connect to

        foreach ($toAddresses as $toAddress)
        {
            /** @var EmailAddress $toAddress */
            $mail->addAddress($toAddress->getEmail());
        }

        $mail->setFrom(
            settings('mails.custom.username'),
            settings('mails.custom.name')
        );

        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject  = $subject->_();
        $mail->Body     = $message->_();
        $send           = $mail->send();

        return new Res([
            'send' => $send
        ]);
    }

    /**
     * @param array $toAddresses
     * @param EmailAddress $from
     * @param VoVal $subject
     * @param VoVal $message
     * @return Res
     * @throws \SendGrid\Mail\TypeException
     */
    protected function sendGrid(
        array $toAddresses,
        EmailAddress $from,
        VoVal $subject,
        VoVal $message
    )
    {
        $email = new \SendGrid\Mail\Mail();
        $email->setSubject($subject->_());
        $email->setFrom($from->getEmail(), $from->getName());

        foreach ($toAddresses as $toAddress)
        {
            /** @var EmailAddress $toAddress */
            $email->addTo($toAddress->getEmail(), $toAddress->getName());
        }
        $email->addContent("text/html", $message->_());

        $sendgrid = new \SendGrid(settings('mails.sendgrid.api_key'));
        $response = $sendgrid->send($email);

        return new Res([
            'send' => $response
        ]);
    }
}
