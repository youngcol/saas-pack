<?php
declare(strict_types=1);
namespace App\Repositories;

use App\Models\Model;
use Illuminate\Database\Query\Builder;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class UserRepository extends Repository
{
    public function getAllByDefault()
    {
        $q = $this->getCurrentQuery();
//        $q->where('email', 'asdasd');

        return $q->get();
    }
}
