<?php



namespace App\Controllers\App;

use App\Controllers\Controller;
use App\Facades\CU;
use App\Models\SocialEngine\Post;
use App\Models\SocialEngine\UserSocialAccount;
use App\Models\User;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rakit\Validation\Rules\Uppercase;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Finder\Finder;

class AccountSettingsController extends Controller
{

    public function indexAction(Request $request, Response $response)
    {
        $params = [
            'title' => 'Settings',
            'page' => 'account-settings',
            'skip_menu' => 1
        ];

        return $this->appRender('app/account-settings/index', $params, $request, $response);
    }

    public function changePasswordAction(Request $request, Response $response)
    {
        $params = [
            'title' => 'Settings - Change password',
            'page' => 'change-password',
            'skip_menu' => 1
        ];

        return $this->appRender('app/account-settings/index', $params, $request, $response);
    }

}
