<?php

namespace App\Controllers\App;

use App\Controllers\Controller;
use App\Facades\CU;
use App\Models\SocialEngine\UserSocialAccount;
use App\Tasks\SocialEngine\GetLastPostsTask;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class PublishController extends Controller
{
    public function queueAction(Request $request, Response $response, $args)
    {
        $count = 20;
        $res = task(new GetLastPostsTask,
            [
                vo('queued', 'post.publish_params.publish_type'),
                0,
                $count
            ]
        );

        $params = [
            'title' => 'Publish',
            'page' => 'publish',
            'submenu' => 'queue',
            'resGetLastPostsTask' => $res
        ];

        return $this->appRender('app/publish/queue', $params, $request, $response);
    }

    public function sentAction(Request $request, Response $response, $args)
    {
        $count = 20;
        $res = task(new GetLastPostsTask,
            [
                vo('sent', 'post.status'),
                0,
                $count
            ]
        );

        $params = [
            'title' => 'Publish',
            'page' => 'publish',
            'submenu' => 'sent',
            'resGetLastPostsTask' => $res
        ];

        return $this->appRender('app/publish/sent', $params, $request, $response);
    }

    public function failedAction(Request $request, Response $response, $args)
    {
        $count = 20;

        $res = task(new GetLastPostsTask,
            [
                vo('failed', 'post.status'),
                0,
                $count
            ]
        );

        $params = [
            'title' => 'Failed',
            'page' => 'publish',
            'submenu' => 'failed',
            'resGetLastPostsTask' => $res
        ];

        return $this->appRender('app/publish/failed', $params, $request, $response);
    }

    public function draftsAction(Request $request, Response $response, $args)
    {
        $count = 20;
        $res = task(new GetLastPostsTask,
            [
                vo('draft', 'post.status'),
                0,
                $count
            ]
        );

        $params = [
            'title' => 'Publish',
            'page' => 'publish',
            'submenu' => 'drafts',
            'resGetLastPostsTask' => $res
        ];

        return $this->appRender('app/publish/drafts', $params, $request, $response);
    }
}
