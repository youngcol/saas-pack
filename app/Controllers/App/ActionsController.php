<?php

namespace App\Controllers\App;

use App\Controllers\Controller;
use App\Facades\CU;
use App\Models\SocialEngine\UserSocialAccount;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Symfony\Component\Finder\Finder;

class ActionsController extends Controller
{
    public function indexAction(Request $request, Response $response, $args)
    {
        $menu = $this->getMenu();


        $params = [
            'menu' => $menu,

            'title' => 'Actions',
            'page' => 'actions'
        ];


        return $this->appRender('app/actions/index', $params, $request, $response);
    }


    public function runActionAction(Request $request, Response $response, $args)
    {
        list($group, $action) = explode('@', $args['action']);
        $cntFileName = APP_PATH . '/actions/'.$group.'/'.$action.'/controller.php';

        $jsParams = require($cntFileName);
        $menu = $this->getMenu();


        $params = [
            'title' => 'Actions',
            'page' => 'actions',
            'menu' => $menu,
            'currentAction' => $args['action'],
            'jsParams' => $jsParams,
            'vueComponentName' => $group . '__' . str_replace(' ', '_', $action)
        ];

        return $this->appRender('app/actions/index', $params, $request, $response);
    }

    protected function getMenu()
    {
        $finder = new Finder();
        $finder->directories()->in(APP_PATH . '/actions');

        $menu = [];
        foreach ($finder as $file) {
            /* @var \Symfony\Component\Finder\SplFileInfo $file */
            $relativePath = $file->getRelativePath();

            if ($relativePath) {
                array_create_key($menu, $relativePath);
                $menu[$relativePath][] = $file->getBasename();

            }
        }

        foreach ($menu as $folder => $files)
        {
            usort($files, function ($f1, $f2) {
                return $f1 > $f2;
            });

            $menu[$folder] = $files;
        }

        return $menu;
    }
}
