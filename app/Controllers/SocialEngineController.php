<?php

namespace App\Controllers;

use App\Models\SocialEngine\SocialNetwork;
use App\Models\SocialEngine\UserSocialAccount;
use App\Tasks\SocialEngine\StoreFacebookAccountTask;
use App\Tasks\SocialEngine\StorePinterestAccountTask;
use App\Tasks\SocialEngine\StoreTwitterAccountTask;
use App\VO\FilledString;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\VO\Pinterest\OAuthScopes;

class SocialEngineController extends Controller
{
    public function twitterCallback(Request $request, Response $response, $args)
    {
        $oauthToken = $request->getParam('oauth_token');
        $oauthVerifier = $request->getParam('oauth_verifier');

        $oauthVerifier = vo($oauthVerifier, 'filled.string');

        $Res = task(new StoreTwitterAccountTask, [$oauthVerifier]);

        if (!$Res->ok())
        {
            $this->container->flash->addMessage('warning', 'Could not add social account');
        }

        return redirect( 'app.accounts');
    }

    public function pinterestCallback(Request $request, Response $response, $args)
    {
        $code = vo($request->getParam('code'), 'filled.string');
        $Res = task(new StorePinterestAccountTask, [$code]);

        if (!$Res->ok()) {
            $this->container->flash->addMessage('warning', 'Could not add social account');
        }

        return redirect( 'app.accounts');
    }

    public function facebookCallback(Request $request, Response $response, $args)
    {
        $accessToken = $this->container->facebook->getCallbackAccessToken();

        if (eN($accessToken))
        {
            $this->container->flash->addMessage('warning', 'Could not get access token from facebook');
            return redirect( 'app.accounts');
        }

        $res = task(new StoreFacebookAccountTask,
            [
                (string)$accessToken
            ]
        );

        if (!$res->ok()) {
            $this->container->flash->addMessage('warning', 'Could not add social account');
        }

        return redirect( 'app.accounts');
    }

    public function accountTwitter(Request $request, Response $response)
    {
        $res = $this->container->twitter->getLoginLink();

        if (!$res->ok())
        {
            $this->container->flash->addMessage('warning', 'Could not get ');
            return redirect( 'app.accounts');
        }

        return redirect_url($res->link);
    }

    public function accountPinterest(Request $request, Response $response, $args)
    {
        $res = $this->container->pinner->getLoginLink(new OAuthScopes(['read_public', 'write_public']));
        return $response->withRedirect($res->link);
    }

    public function accountFacebook(Request $request, Response $response, $args)
    {
        $res = $this->container->facebook->getLoginLink();
        return $response->withRedirect($res->link);
    }
}
