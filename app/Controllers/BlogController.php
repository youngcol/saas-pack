<?php

namespace App\Controllers;

use App\Tasks\SignUpUserTask;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class BlogController extends Controller
{
	public function index (Request $request, Response $response, $args)
	{
        $params = [

        ];
        return $this->render('public/blog/index', $params, $request, $response);
	}
}
