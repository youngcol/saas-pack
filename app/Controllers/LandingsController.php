<?php

namespace App\Controllers;

use App\Tasks\SignUpUserTask;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class LandingsController extends Controller
{
	public function electricianIndex (Request $request, Response $response, $args)
	{
        $params = [

        ];



        return $this->render('landings/electician/layout', $params, $request, $response);
	}
}
