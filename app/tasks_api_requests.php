<?php

use App\Facades\CU;
use App\Tasks\change_cu_own_email_task;
use App\Tasks\DeleteModelsCollectionTask;
use App\Tasks\GetModelsCollectionTask;
use App\Tasks\UpdateModelsCollectionTask;

$tasks_api_requests = [];


$tasks_api_requests['change_cu_own_email_task'] =
[
    // only users with this permissions allowed
    'allowed_permissions' => [],

    'params' => [
        'email' => 'required|email',
    ],

    'runner' => function (array $params) {


        $res = task(new change_cu_own_email_task,
            [
                CU::user(),
                $params['email']
            ]
        );

        return $res;
    }
];

$tasks_api_requests['GetModelsCollectionTask'] =
[
    // only users with this permissions allowed
    'allowed_permissions' => [],

    'params' => [
        'model_name' => 'required|filled.string',
        'page' => 'required|numeric',
        'per_page' => 'required|numeric',
        'order' => 'required|mysql.order',
        'relations' => 'array',
        'where' => 'array',
        'sort_by' => 'filled.string',
        'with_total' => 'numeric',
        'with_is_next' => 'numeric',
    ],

    'runner' => function (array $params) {


        $res = task(new GetModelsCollectionTask([
            'with_total' => $params['with_total']->getValueOrDef(0),
            'with_is_next' => $params['with_is_next']->getValueOrDef(0),
        ]),
            [
                CU::user(),
                $params['model_name'],
                $params['relations'],
                $params['where'],
                $params['page'],
                $params['per_page'],
                $params['sort_by'],
                $params['order']
            ]
        );

        return $res;
    }
];


$tasks_api_requests['DeleteModelsCollectionTask'] =
[
    // only users with this permissions allowed
    'allowed_permissions' => [],

    'params' => [
        'model_name' => 'required|filled.string',
        'models_ids' => 'array'
    ],

    'runner' => function (array $params) {

        $res = task(new DeleteModelsCollectionTask,
            [
                CU::user(),
                $params['model_name'],
                $params['models_ids'],
            ]
        );
        return $res;
    }
];

$tasks_api_requests['UpdateModelsCollectionTask'] =
[
    // only users with this permissions allowed
    'allowed_permissions' => [],

    'params' => [
        'model_name' => 'required|filled.string',
        'update_data' => 'array'
    ],

    'runner' => function (array $params) {

        $res = task(new UpdateModelsCollectionTask,
            [
                CU::user(),
                $params['model_name'],
                $params['update_data'],
            ]
        );
        return $res;
    }
];

$container['tasks_api_requests'] = $tasks_api_requests;
