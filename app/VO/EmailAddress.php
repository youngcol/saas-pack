<?php

declare(strict_types=1);
namespace App\VO;

use App\Common\VoException;
use App\VO\Vo;

/**
 *
 * Class value object from validation rule
 *
 * @package App\VO
 */
class EmailAddress extends Vo
{
    protected $email;
    protected $name = null;

    public function __construct(string $email, $name=null)
    {
        $this->val($email, $name);
        $this->email = $email;
        $this->name = $name;
    }

    protected function val($email, $name)
    {
        val($email, 'required|email');
        val($name, 'filled.string');
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return null
     */
    public function getName()
    {
        return $this->name;
    }
}
