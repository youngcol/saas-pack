<?php

namespace App\VO;

use App\Common\VoException;

class FilledString extends Vo
{

    protected function val(string $val)
    {
        if (strlen($val) === 0) {
            throw new VoException('Input string should not be empty');
        }
    }

}
