<?php
declare(strict_types=1);

namespace App\Services;


use Symfony\Component\Finder\Finder;

/**
 * Language service
 *
 * Class Language
 * @package App\Services
 */
class Language
{
    protected $lang;
    protected $dictionary = [];

    public function __construct($lang)
    {
        $this->setLang($lang);
    }

    /**
     * @param string $lang
     * @throws \App\Common\FlowException
     */
    public function setLang(string $lang)
    {
        $this->lang = $lang;
        $allLangs = unserialize(file_get_contents(PATH_ROOT . 'i18n.php'));

        test(!isset($allLangs[$lang]), "No such lang `$lang` in lang/i18n.php");

        $this->dictionary = $allLangs[$lang];
    }

    protected function includeFile($file)
    {
        return include $file;
    }

    /**
     * @return array
     */
    public function getDictionary()
    {
        return $this->dictionary;
    }

    /**
     * Get value from dictionary
     *
     * @param $path
     * @return mixed
     * @throws \App\Common\FlowException
     */
    public function o($path)
    {
        $parts = explode('.', $path);
        $route = [];

        $file = array_shift($parts);
        $ret = $this->dictionary[$file];
        $route[] = $file;

        foreach ($parts as $part)
        {
            $route[] = $part;

            test(
                !isset($ret[$part]),
                'No such route in dictionary: `'. implode('.', $route). '`'
            );

            $ret = $ret[$part];
        }

        return $ret;
    }
}
