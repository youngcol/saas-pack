import Vue from "vue";

// SMMA
import smma__create_post from './../app/actions/Smma/create post/smma  create post.vue';
Vue.component('smma__create_post', smma__create_post);

import smma__queue_list from './../app/actions/Smma/queue list/smma  queue list.vue';
Vue.component('smma__queue_list', smma__queue_list);

import smma__sent_list from './../app/actions/Smma/sent list/smma  sent list.vue';
Vue.component('smma__sent_list', smma__sent_list);

import smma__failed_list from './../app/actions/Smma/failed list/smma  failed list.vue';
Vue.component('smma__failed_list', smma__failed_list);


// Marketing
import marketing__send_email from './../app/actions/Marketing/send email/marketing  send email.vue';
Vue.component('marketing__send_email', marketing__send_email);

import marketing__contacts_list from './../app/actions/Marketing/contacts list/marketing  contacts list.vue';
Vue.component('marketing__contacts_list', marketing__contacts_list);


