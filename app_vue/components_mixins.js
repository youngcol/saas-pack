import h from './helpers';

export default {
    methods: {
        /**
         * Return value from locales array
         *
         * @param route
         * @returns {*|string}
         */
        $L: function (route) {
            var parts = route.split('.');

            var ret = this.$root.i18n[parts.shift()];

            parts.map(function (i) {
                ret = ret[i];
            });

            // debugger;
            // return this.$root.i18n[route];

            return ret;
        },

        log(title, data)
        {
            console.log(title, data);
        },

        async runTask(name, params)
        {
            return await h.api.runTask(name, params);
        },

        async apiGetModels()
        {

        },

        successNotify(message='Something happened correctly!')
        {
            this.$buefy.notification.open({
                message: message,
                type: 'is-success'
            })
        },

        errorNotify(message='Error happend.')
        {
            this.$buefy.notification.open({
                message: message,
                type: 'is-danger'
            })
        },

        toastOk(message)
        {
            this.$buefy.toast.open({
                message: message,
                type: 'is-success'
            });
        },

        toastError(message)
        {
            this.$buefy.toast.open({
                message: message,
                type: 'is-danger'
            });
        }

    }
}
