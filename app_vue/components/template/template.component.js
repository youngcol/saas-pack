import h from '../../helpers';

export default {
    name: "Template",
    props: {
        posts: {
            type: Array,
            required: true
        },
        type: {
            type: String,
            required:true
        },
    },
    data() {
        return {
            name: 'Matt Stauffer',
            // title: this.post.title,
        }
    },
    watch: {

    },

    async mounted() {

        this.posts

    },

    methods: {

    },

    created () {

    },

    beforeDestroy() {

    }
}
