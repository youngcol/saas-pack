import {ObjectModel} from "objectmodel"

const M = new ObjectModel({

    filename: String,
    s3_url: [String, undefined],
    type: String,
    id: Number,
    user_id: Number,

}).defaultTo({

    getUploadsUrl() {
        return  '/uploads/' + this.filename;
    },

});

M.prototype.getModelName = function () {
    return 'App\\Models\\SocialEngine\\MediaFile';
};

export default M
