
import {ObjectModel} from "objectmodel"

const M = new ObjectModel({

    id: Number,
    name: String,
    title: String,

}).defaultTo({

    getNameAndEmail()
    {

    }

});

M.prototype.getModelName = function () {
    return 'App\\Models\\SocialEngine\\SocialNetwork';
};

M.prototype.getNetworkName = function (id) {
    if (id === 1) return 'pinterest';
    if (id === 2) return 'twitter';
    if (id === 4) return 'instagram';
};

export default M
