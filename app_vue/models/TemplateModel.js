import {ObjectModel} from "objectmodel"

const M = new ObjectModel({

    active: Number,
    is_admin: Number,
    language: String,
    timezone: String,
    created_at: String,
    permissions: String,
    email: String,
    name: String,
    id: Number,

}).defaultTo({

    some_model_func() {

        return "Dear " + this.name;
    },

});

M.prototype.getModelName = function () {
    return 'App\\Models\\{model_name}';
};

export default M
