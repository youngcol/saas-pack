
@extends("app.layout")

@section('content')

    <div class="container settings">
        <div class="columns">
            <div class="column is-3 ">
                <aside class="menu">

                    @foreach($folders as $item)

                        @php($isCurrentFolder = ($current_folder == $item['folder']))

                        <p class="menu-label">{{$item['folder']}}</p>

                        <ul class="menu-list">

                            @foreach($item['stories'] as $story)
                                @php($isCurrentStory = ($current_story == $story))

                                <li>
                                    <a
                                            href="{{route('app.vue.story', ['story' => $story, 'folder' => $item['folder']])}}"
                                            class="{{ $isCurrentStory ? 'is-active' : '' }}">
                                        {{$story}}
                                    </a>
                                </li>

                            @endforeach
                        </ul>

                    @endforeach
                </aside>
            </div>
            <div class="column is-9">
                @if($storyHtml)
                    {!! $storyHtml !!}
                @endif
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @yield('story_scripts');
@endsection

