import Vue from 'vue';
import _ from 'lodash'
import Buefy from 'buefy'

Vue.use(Buefy)

require('./bootstrap');


// debugger;
// if (process.env.MIX_APP_ENV == 'dev')
// {
//     debugger;
// }

// ----------- COMPONENTS ---------
import DataTable from './components_framework/DataTable.vue';
import DataList from './components_framework/DataList.vue';
import PostsList from './components/posts-list/PostsList.vue';
import DraftsList from './components/drafts-list/DraftsList.vue';
import QueueList from './components/queue-list/QueueList.vue';
import SentList from './components/sent-list/SentList.vue';
import AccountSettingsGeneral from './components/account-settings/AccountSettingsGeneral.vue';
import AccountSettingsPassword from './components/account-settings/AccountSettingsPassword.vue';
import CreatePostWindow from './components/CreatePostWindow.vue';
import SocialAccounts from './components/SocialAccounts.vue';
import CrmContacts from './components/crm/Contacts.vue';


// ----------- STORY COMPONENT ----------------
import Profile from './vue_stories/profile/Profile.vue';

// Framework components
Vue.component('data-table', DataTable);
Vue.component('data-list', DataList);

// App components
Vue.component('profile', Profile);
Vue.component('create-post-window', CreatePostWindow);
Vue.component('social-accounts', SocialAccounts);
Vue.component('crm-contacts', CrmContacts);
// Vue.component('posts-list', PostsList);
Vue.component('drafts-list', DraftsList);
Vue.component('queue-list', QueueList);
Vue.component('sent-list', SentList);
Vue.component('account-settings-general', AccountSettingsGeneral);
Vue.component('account-settings-password', AccountSettingsPassword);
Vue.component('account-settings-password', AccountSettingsPassword);


require('./actions_vue_register');



// ----------- END COMPONENTS ---------


// ----------- MODELS ---------
import h from './helpers';
import UserModel from './models/UserModel'
import TestModel from './models/TestModel'
// ----------- END MODELS ---------


var currentUser = new UserModel(current_user);

let globalVue = new Vue({

    el: '#main-body',

    data: _.extend(globalVueData, {

        currentUser: currentUser,

        // modal window example
        isCreatePostModalActive: false,
        createPostWindowProps: {
            email: 'evan@you.com',
            password: 'testing'
        }

    }),

    mounted()
    {

    },

    methods: {

        onClickCreatePost()
        {
            this.$buefy.modal.open({
                parent: this,
                component: CreatePostWindow,
                // hasModalCard: true,
                customClass: 'custom-class custom-class-2'
            });

        },

        handleScroll: _.debounce(() => {

            log('app::handleScroll');

            const delta = 300;
            var bottomHeight = $(document).height() - (window.scrollY + window.outerHeight);

            if (bottomHeight < delta)
            {
                globalVue.$root.$emit(sp(globalVue.events, 'doc_scrolled_to_bottom'))
            }

        }, 200),

    }

});

