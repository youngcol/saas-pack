

(function(app, soma_data) {

    'use strict';

    app.e = {
        'START':                        'start',
        'DOCUMENT__ON_CHANGE_POSITION': 'DOCUMENT__ON_CHANGE_POSITION',
        'DOCUMENT__ON_ADD_CONTROL':     'DOCUMENT__ON_ADD_CONTROL',
        'CONTROL__ON_ADD_TO_DOCUMENT':  'CONTROL__ON_ADD_TO_DOCUMENT',
        'CONTROL__ON_CHANGE_STYLE_FONT':  'CONTROL__ON_CHANGE_STYLE_FONT',

        'DOCUMENT__CLICKED_ON_CONTROL':  'DOCUMENT__CLICKED_ON_CONTROL',

    };

    var uniqId = 0;
    app.uniqId = function()
    {
        return uniqId++;
    };


    var APP = new soma.Application.extend({

        constructor: function()
        {
            soma.Application.call(this);
        },

        init: function()
        {

            this.injector.mapClass('api', app.services.api, true);

            this.injector.mapValue('bb_gallery',
                this.injector.createInstance(
                    app.services.bb_gallery, {})
            );

//            this.commands.add(app.e.CONTROL__ON_ADD_TO_DOCUMENT,app.commands.control.on_add_to_document);

//            this.commands.add(app.e.CONTROL__ON_CHANGE_CONTENT,app.commands.control.on_change_content);


        },
        start: function()
        {

        }
    });

    var ideObj = new APP(soma_data);

})(window.app = window.app || {}, soma_data);

app.s = {};