

;(function(app, undefined) {

'use strict';

// package
app.services = app.services || {};

app.services.filebrowsing = function(__, config, dispatcher, api)
{
    var SAVING_TIMEOUT = 1000;
    var LINE_HEIGHT = 19;

var pub = {}, f = {}, on={}, ui={}, _i = {}, self = this, $$ = {

    $box: null,

    state: {

    },

    $btn:   null,
    status: null,

    timer:  null,
    editors: {},
    lastCursor: null,

    activeFile: {},
    opened_files: [],

    saving_timeout: null,

    loggin:
    {

    }

};

f.construct = function()
{
    $$ = app__def_comp_options('filebrowsing', $$);
    _.extend($$, __);


    f.__s('big_bang');
};

f.isOpenedFileExists = function(fileName)
{
    var index = _.findIndex($$.opened_files, {filename: fileName});

    return {
        exists: index != -1,
        file:   sp($$.opened_files, index)
    };
};


f.installEditor = function(fileInfo, content, lang)
{

    var editor = CodeMirror.fromTextArea(
        $('#ta_'+ fileInfo.editorId, $$.$box).get(0), {
            scrollbarStyle: "simple",
            lineNumbers:    true,
            matchBrackets:  true,
            mode:           lang,
            indentUnit:     4,
            indentWithTabs: true,
            extraKeys: {
                // make bookmark
                "Alt-X": function(cm){

                    var selection = cm.doc.getSelection();

                    $$.onCreateBookmark(
                        selection, pub.GetCurrentFileData());

                },

                // copy current filepath
                "Alt-C": function(cm){

                    var cursor = cm.getCursor();
                    copyTextToClipboard(fileInfo.filename+':'+cursor.line);
                    infoNotify('Copied: ' + fileInfo.filename);
                    cm.focus();
                },

                },
        });


    editor.on("renderLine", function(cm, line, elem)
    {



    });

    editor.on("change", function(cm, ch)
    {

        var isSendMessage = false;
        var change = {
            line: ch.from.line,
            delta: 0,
        };


        switch (ch.origin)
        {
            case "paste":
            case "+input":
            {
                change.delta = ch.to.line - ch.from.line;
                change.event = ch;
                isSendMessage = true;
                break;
            }

            case "+delete":
            {
                change.delta = ch.from.line - ch.to.line;
                change.event = ch;
                isSendMessage = true;
                break;
            }
        }


        if(isSendMessage)
        {
            dispatcher.dispatch(app.e.FILE_CONTENT__CHANGED, {
                fileName: $$.activeFile.filename,
                change: change
            });
        }



        if($$.isRefreshContent)
        {

            $$.isRefreshContent = false;

        } else {

            f.__s('change_editor_content', {
                cm:         cm,
                fileInfo:   fileInfo
            });
        }
    });

    editor.on('focus', function(cm ,e) {
        $$.onFocus && $$.onFocus();
    });

    editor.on('blur', function(cm ,e) {
        cm.doc.getCursor();
        $$.onFocus && $$.onFocus();
    });

    editor.on('keydown', function(cm ,e) {
        //
        // if(e.ctrlKey) {
        //     if (e.shiftKey) {
        //
        //         e.preventDefault();
        //
        //         switch (e.key) {
        //             case 'F':
        //             case 'f':
        //             {
        //
        //                 debugger;
        //                 break;
        //             }
        //         }
        //     }
        // }


        if(e.ctrlKey && e.key === 'r')
        {
            e.preventDefault();

            f.__s('update_current_editor_content');
        }

    });


    editor.on('keyup', function(cm ,e) {

        // p(e.keyCode == 32);

        if(e.ctrlKey)
        {
            switch (e.keyCode)
            {
                case 32: // space
                {
                    $$.onFocus && $$.onFocus();

                    var sel = cm.doc.getSelection();

                    $$.onGutterClick && $$.onGutterClick(sel, cm.fileName);

                    break;
                }
            }
        }

    });

    editor.on('gutterClick', function(cm, line, gutter, e) {
        if (e.ctrlKey) {

            $$.onFocus && $$.onFocus();

            var sel = cm.doc.getSelection();
            $$.onGutterClick && $$.onGutterClick(sel, cm.fileName);
        }
    });

    fileInfo.editor = editor;

//            $$.editor.on('mousedown', function(cm ,e)
//            {
//                var is_right_btn = e.button == 2;
//
//
//                if (is_right_btn)
//                {
//
//                    var selectionText = $$.editor.doc.getSelection();
//
//                    if (selectionText.length)
//                    {
//                        dispatcher.dispatch(ide.e.CANVAS__CREATE_EDITOR, {
//                            $layout: $$.$layout,
//                            file:       $$.fileInfo.file,
//                            content:    '<?php '+ selectionText
//                        });
//                    }
//
//                }
//            })
};

f.__s = function(state, par)
{
    var ret = null;
    p({state: state, par: par});

    switch (state)
    {
        case 'update_current_editor_content':
        {
            $$.isRefreshContent = true;

            f.__s('update_current_filetab_status', {
                fileInfo: $$.activeFile,
                status: 'editing'
            });

            api.call('GET', '/file/content', {
                editorName:     __.editorName,
                contextId:      __.contextId,
                projectId:      __.projectId,
                basePath:       __.basepath,
                fileName:       $$.activeFile.filename
            }, {
                done: function(resp)
                {
                    var cursor = $$.activeFile.editor.getCursor();
                    var scroll = $$.activeFile.editor.getScrollInfo();


                    $$.activeFile.editor.setValue(resp.content);
                    $$.activeFile.editor.setCursor(cursor.line, cursor.ch);

                    $$.activeFile.editor.scrollTo(scroll.left, scroll.top);

                    f.__s('update_current_filetab_status', {
                        fileInfo: $$.activeFile,
                        status: 'active'
                    });

                    infoNotify('Updated');

                },
                fail: function()
                {
                    fork(function() {
                        f.__s('update_current_filetab_status', {
                            fileInfo: $$.activeFile,
                            status: 'active'
                        });
                    },600);

                    errorNotify('Cant update!');
                }
            });

            break;
        }

        case 'insert_file':
        {
            var opened__ = f.isOpenedFileExists(par.fileName);

            if (opened__.exists)
            {
                f.__s('hide_all_editors');
                f.__s('show_editor', {file: opened__.file});

                break;
            }
            //=-=-=-=-=-=-=

            f.__s('got_file_content', par.content);


            break;
        }
        case 'open_file':
        {
            var opened__ = f.isOpenedFileExists(par.fileName);

            if (opened__.exists)
            {
                f.__s('hide_all_editors');
                f.__s('show_editor', {file: opened__.file});

                if(sp(par.par, 'focus_new_file'))
                {
                    f.focusedTab(opened__.file);
                }

                break;
            }
            //=-=-=-=-=-=-=


            api.call('GET', '/file/content', {
                basePath:       __.basepath,
                fileName:       par.fileName,
                editorName:     __.editorName,
                contextId:      __.contextId,
                projectId:      __.projectId,
            }, {
                done: function(resp)
                {
                    if(!resp) {

                        errorNotify('Cant open: '+ par.fileName);
                        return;
                    }
                    //=-=-=-=-=-=-=


                    f.__s('hide_all_editors');
                    f.__s('got_file_content', resp);

                    if(sp(par.par, 'focus_new_file'))
                    {
                        f.focusedTab($$.activeFile);
                    }

                },
                fail: function()
                {

                }
            });

            break;
        }

        case 'update_current_filetab_status':
        {
            var $btn = $('#btn_'+ par.fileInfo.editorId, $$.$box);

            $btn
                .removeClass('active')
                .removeClass('editing')
                .removeClass('focused')
                .removeClass('error')
                .removeClass('saved')
                .addClass(par.status);

            break;
        }

        case 'change_editor_content':
        {

           f.__s('update_current_filetab_status', {
               fileInfo: par.fileInfo,
               status: 'editing'
           });
//
           clearTimeout($$.saving_timeout);


           $$.saving_timeout = setTimeout(function(){

               api.call('POST', '/file/content', {

                   basePath:   __.basepath,
                   fileName:   par.fileInfo.filename,
                   content:    par.cm.doc.getValue(),

                   editorName:     __.editorName,
                   contextId:      __.contextId,
                   projectId:      __.projectId,
               }, {
                   done: function(prod)
                   {
                       f.__s('update_current_filetab_status', {
                           fileInfo: par.fileInfo,
                           status: 'saved'
                       });

                       fork(function() {
                           f.__s('update_current_filetab_status', {
                               fileInfo: par.fileInfo,
                               status: 'active'
                           });
                       },400);

                   },

                   fail: function()
                   {
                       f.__s('update_current_filetab_status', {
                           fileInfo: par.fileInfo,
                           status: 'error'
                       });

                       fork(function() {
                           f.__s('update_current_filetab_status', {
                               fileInfo: par.fileInfo,
                               status: 'active'
                           });
                       },400);
                   }
               });

           }, SAVING_TIMEOUT);

            break;
        }

        case 'show_editor':
        {
            $('.opened #btn_' +par.file.editorId, $$.$box).addClass('active');
            $('.editors #'+ par.file.editorId, $$.$box).removeClass('hidden');

            break;
        }

        case 'hide_all_editors':
        {
            $('.opened .active', $$.$box).removeClass('active');
            $('.editors .editor', $$.$box).addClass('hidden');

            break;
        }

        case 'close_editor':
        {
            $('#btn_editor_' + par.editorId, $$.$box).remove();

            var closed = _.findWhere($$.opened_files, {id: par.editorId});


            $$.opened_files = _.filter($$.opened_files, function (f) {
               return f.id !== par.editorId
            });


            $('#editor_' + par.editorId, $$.$box).remove();

            var lastFile = _.last($$.opened_files);

            if(lastFile)
            {
                f.__s('open_file', {fileName: lastFile.filename});
            }


            ret = closed;

            break;
        }

        case 'got_file_content':
        {
            var id = uniqId();
            var editorId = 'editor_'+ id;



            $('.opened', $$.$box).append(
                box__tmpl('filebrowsing_opened', {
                    id: id,
                    editorId: editorId,
                    shortFileName: par.shortFileName
                })
            );


            $('.editors', $$.$box).append(
                box__tmpl('filebrowsing_editor', {
                    editorId: editorId,
                    content: par.content
                })
            );


            $$.opened_files.push({
                filename: par.fileName,
                editorId: editorId,
                id: id
            });

            $$.activeFile = _.last($$.opened_files);


            f.installEditor($$.activeFile, par.content, par.fileLanguage);

            $$.activeFile.editor.fileName = par.fileName;


            pub.Resize();
            $$.activeFile.editor.refresh();

//                    f.buildMonacoEditor(editorId, par.content, par.fileLanguage);
//                    par.fileName
            break;
        }

        case 'show':
        {
            $$.$box.removeClass('hidden');

            break;
        }

        case 'bind_events':
        {

            $$.$box.on('click', '.fileName .close', function(e) {
                e.preventDefault();

                var editorId = $(this).parents('.fileName').data('id');

                var closedFile = f.__s('close_editor', {editorId: editorId});

                $$.closeClicked = true;
                _.delay(function () {
                    $$.closeClicked = false;
                }, 300);


                api.call('POST', '/filebrowsing/file/close', {
                    fileName:       closedFile.filename,
                    editorName:     __.editorName,
                    contextId:      __.contextId,
                    projectId:      __.projectId,

                }, {
                    done: function(resp)
                    {

                    },
                    fail: function()
                    {

                    }
                });
            });

            $$.$box.on('click', '.fileName', function() {

                if($$.closeClicked) return;

                f.__s('hide_all_editors');

                var editorId = $(this).data('editor_id');
                $(this).addClass('active');
                $('.editors #'+ editorId, $$.$box).removeClass('hidden');


                var index = _.findIndex($$.opened_files, {editorId: editorId});
                $$.activeFile = $$.opened_files[index];



                api.call('POST', '/filebrowsing/file/select', {
                    fileName:       $$.activeFile.filename,
                    editorName:     __.editorName,
                    contextId:      __.contextId,
                    projectId:      __.projectId,

                }, {
                    done: function(resp)
                    {

                    },
                    fail: function()
                    {

                    }
                });
            });

            break;
        }

        case 'hide':
        {

            $$.$box.addClass('hidden');

            break;
        }

        case 'big_bang':
        {
//                    var boxHtml = box__tmpl('filebrowsing', {
//
//                    });
//
//                    $('body').append(boxHtml);
//                    $$.$box = $('#filebrowsing');
            $$.$box = $(__.box);


            f.__s('bind_events');
//                    $$.$btn     = $('#main-page-btn');
//                    $$.btnCls   = $$.$btn.attr('class');

            break;
        }
    }

    return ret;
};

f.focusedTab = function(fileInfo)
{
    f.__s('update_current_filetab_status', {
        fileInfo: fileInfo,
        status: 'focused'
    });

    fork(function() {
        f.__s('update_current_filetab_status', {
            fileInfo: fileInfo,
            status: 'active'
        });
    },500);
};

f.buildMonacoEditor = function (nodeElement, content, lang)
{


//            require.config({ paths: { 'vs': '/node_modules/monaco-editor/min/vs' }});
//            require(['vs/editor/editor.main'], function() {
//                 var editor = monaco.editor.create(document.getElementById(nodeElement), {
//                    value:      content,
//                    language:   lang
////                    language: 'javascript'
//                });
//
//                var m = editor.getModel();
//
//                var myBinding = editor.addCommand(monaco.KeyCode.F9, function() {
//
//                    var sel = editor.getSelection();
//                    p(sel);
//
//                });
//
//
//                editor.onDidChangeModelContent(function(e) {
//
//
//                });
//
//
//                $$.editors[nodeElement] = editor;
//
//                debugger;
//            });
}


/**
 * Получить выделенную область
 *
 * @return {*}
 * @constructor
 */
pub.GetSelection = function()
{
    var ret= null;

    if ($$.activeFile)
    {
        var sels    = clone($$.activeFile.editor.listSelections());
        var sel     = sels.pop();

        sel.anchor.ch   = 0;
        $$.activeFile.editor.addSelection(sel.anchor, sel.head);

        ret = {
            content:        $$.activeFile.editor.getSelection(),
            filepath:       $$.activeFile.filename,
            sel:            sel
        };
    }

    return ret;
};

pub.show = function() {
    f.__s('show');
};

pub.hide = function() {
    f.__s('hide');
};

pub.openFile = function(fileName, par) {
    f.__s('open_file', {
        fileName: fileName,
        par: par
    });
};


pub.InsertFile = function(fileName, content) {
    f.__s('insert_file', {
        fileName: fileName,
        content: content
    });
};

pub.GetFilesCount = function() {
    return $$.opened_files.length;
};

pub.Resize = function()
{
    var h = $(window).height() - $('#header').height() - 40;
    var ee = $('.editors .editor', $$.$box);


    ee.each(function (i, e) {
        $(e).height(h);
    });

};


pub.GetOpenedFiles = function()
{
    return _.map($$.opened_files, function (f) {
        return f.filename;
    });
};

pub.ScrollToLine = function(line)
{
    return $$.activeFile.editor.scrollTo(0, line * LINE_HEIGHT);
};

pub.GetCurrentFileData = function()
{
    var ret = null;

    if(sp($$.activeFile, 'editor'))
    {

        ret = {
            lineCount: $$.activeFile.editor.lineCount(),
            cursor: $$.activeFile.editor.getCursor(),
            mode: $$.activeFile.editor.getMode().name,
            content: $$.activeFile.editor.getValue(),
            filename: $$.activeFile.filename
        };
    }

    return ret;
};

f.construct();

$$._i = {
    show:       pub.show,
    hide:       pub.hide,

    ScrollToLine: pub.ScrollToLine,
    InsertFile: pub.InsertFile,
    GetFilesCount: pub.GetFilesCount,
    openFile:   pub.openFile,
    GetSelection: pub.GetSelection,
    GetCurrentFileData: pub.GetCurrentFileData,
    Resize: pub.Resize,
    GetOpenedFiles: pub.GetOpenedFiles
};

return $$._i;
};


})(window.app = window.app || {});