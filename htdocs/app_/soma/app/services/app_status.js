


;(function(app, undefined) {

    'use strict';

    // package
    app.services = app.services || {};

    app.services.app_status = function(__, config, dispatcher, api)
    {
        var pub = {}, f = {}, on={}, ui={}, _i = {}, self = this, $$ = {

            $box: null,

            state: {

            },

            $btn:   null,
            status: null,

            uniqId: 0,

            timer:  null,

            loggin:
            {

            }
        };

        f.construct = function()
        {
            $$ = app__def_comp_options('document', $$);
            _.extend($$, __);


            f.__s('big_bang');
        };

        f.__s = function(state, par)
        {

            switch (state)
            {
                case 'big_bang':
                {
                    $$.$btn     = $('#main-page-btn');
                    $$.btnCls   = $$.$btn.attr('class');

                    break;
                }
            }
        };


        f.status__ui_return_do_defaul = function(id)
        {
            _.delay(function()
            {
                if ($$.uniqId != id) return;
                //-=-=-=-=-=-

                pub.default();
            }, 1000);
        };


        f.status__ui_btn_status = function()
        {
            var cls = $$.btnCls;
            $$.uniqId++;

            switch($$.status)
            {
                case "waiting": cls += ' button-waiting'; break;
                case "working": cls += ' button-secondary'; break;
                case "success": cls += ' button-success'; break;
                case "error":   cls += ' button-error'; break;
            }

            $$.$btn.attr('class', cls);

            return $$.uniqId;
        };


        pub.waiting = function()
        {
            $$.status = 'waiting';
            $$.uniqId++;
            f.status__ui_btn_status();
        };


        pub.working = function()
        {
            $$.status = 'working'

            f.status__ui_btn_status();
        };


        pub.success = function()
        {
            $$.status = 'success';

            var uniqId = f.status__ui_btn_status();

            f.status__ui_return_do_defaul(uniqId);
        };

        pub.error = function()
        {
            $$.status = 'error';

            var uniqId = f.status__ui_btn_status();

            f.status__ui_return_do_defaul(uniqId);
        };

        pub.default = function()
        {
            $$.status = 'default';
            f.status__ui_btn_status();
        };


        f.construct();

        $$._i = {
            waiting:    pub.waiting,
            working:    pub.working,

            success:    pub.success,
            error:      pub.error,
            default:    pub.default,

//            info: pub.info,
//            f: f,
//            call:       pub.call,
        };

        return $$._i;
    };


})(window.app = window.app || {});