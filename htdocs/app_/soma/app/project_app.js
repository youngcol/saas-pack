(function(app, soma_data) {

    'use strict';

    app.soma_data = soma_data;

    app.e = {
        'START':                        'start',
        'DOCUMENT__ON_CHANGE_POSITION': 'DOCUMENT__ON_CHANGE_POSITION',
        'DOCUMENT__ON_ADD_CONTROL':     'DOCUMENT__ON_ADD_CONTROL',
        'CONTROL__ON_ADD_TO_DOCUMENT':  'CONTROL__ON_ADD_TO_DOCUMENT',
        'CONTROL__ON_CHANGE_STYLE_FONT':  'CONTROL__ON_CHANGE_STYLE_FONT',
        'DOCUMENT__CLICKED_ON_CONTROL':  'DOCUMENT__CLICKED_ON_CONTROL',
    };

    var uniqId = 0;
    app.uniqId = function()
    {
        return uniqId++;
    };


    var APP = new soma.Application.extend({

        constructor: function()
        {
            soma.Application.call(this);
        },

        init: function()
        {

            var api = this.injector.createInstance(app.services.api, {});
            this.injector.mapValue('api', api);
            window.api = api;

            // this.injector.mapClass('api', app.services.api, true);

            // infoNotify('injector.mapClass(\'controls_builder\'');

            // this.injector.mapClass('controls_builder', app.services.controls_builder, true);
            // this.injector.mapClass('control', app.services.control, true);

//            $$.controlsBuilder =
//                this.injector.createInstance(
//                    app.services.controls_builder, {
//
//                        doc: {
//                            $box:   $$.$box,
//                            id:     $$.in.info.id
//                        }
//                    });

//            this.injector.mapValue('app_status',
//                this.injector.createInstance(
//                    app.services.app_status, sp(config.services, 'app_status', {}))
//            );
////

            window.newPostWindow = this.injector.createInstance(
                app.services.new_post_window, {});


            this.injector.mapValue('new_post_window', window.newPostWindow);

            // this.commands.add(app.e.CONTROL__ON_ADD_TO_DOCUMENT,app.commands.control.on_add_to_document);

//            this.commands.add(app.e.CONTROL__ON_CHANGE_CONTENT,app.commands.control.on_change_content);


        },
        start: function()
        {
//            var dd = this.dispatcher;
//
//            fork(function()
//            {
//                dd.dispatch(app.e.START, {
//
//                });
//            }, 100);
//
//            var services = sp(config, 'services', {});
//
//            if (services.document)
//            {
//                this.injector.createInstance(
//                    app.services.document,
//                    sp(services.document, 'info', {}));
//
//            }

        }
    });


    var ideObj = new APP(soma_data);

})(window.app = window.app || {}, soma_data);

app.s = {};
