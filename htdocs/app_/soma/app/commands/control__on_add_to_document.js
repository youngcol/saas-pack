/**
 * Добавить контрол в документ
 *
 */
;(function(app, undefined) {
    // package
    'use strict';

    // package
    app.commands = app.commands || {};
    app.commands.control = app.commands.control || {};

    app.commands.control.on_add_to_document = function(controls_builder, api, project)
    {
        var self = this, f = {};

        f.__s = function(state, par)
        {
            switch(state)
            {
                case "on_failed_stored_control":
                {

                    break;
                }

                case "on_stored_control":
                {
                    controls_builder.Hide();


                    project.AttachControl2Document(par.docId, par.type, par.data);

                    break;
                }

                default:
                    crit(state);
            }
        };



        this.execute = function(event)
        {
            api.call('POST', 'control', event.params, {
                done: function(resp) {
                    f.__s('on_stored_control', resp)
                },
                fail: function(resp) {

                    f.__s('on_failed_stored_control', resp);

                }
            });

//            {
//                processData:    false,
//                    contentType:    false,
//            }

        };

    };

})(window.app = window.app || {});