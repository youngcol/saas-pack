

(function(app, config) {

    'use strict';

    app.e = {
        'START':                    'start',

           'IDE_OPEN_FILE': 'IDE.OPEN_FILE',
//        'CANVAS__CREATE':           'canvas.create',
//        'CANVAS__CREATE_EDITOR':    'canvas.create_editor',
//        'QUOTES_FEED__ON_GOT_NEW_QUOTES': 'quotes_feed.on_got_new_quotes',
//        'WORKSPACE__ON_SET_CURDOC':       'workspace.on_set_curdoc',
//
//        'DOCUMENT__ON_ADD_CHART':       'document.add_chart',
//        'DOCUMENT__ON_SETUP_WIDJETS':   'document.setup_widjets',
//        'DOCUMENT__ON_WIDJET_CHANGED':  'document.on_widjet_changed',
//        'DOCUMENT__ON_WIDJET_CLOSED':   'document.on_widjet_closed'


//        ui__handle_user_events:     'ui.handle_user_events',
//        chronoroll__open_date:      'chronoroll:open_date',
//        chronoroll__open_neib_pic:  'chronoroll:open_neib_pic',
//
//        timeperiod__ppic_changed:           'timeperiod:ppic_changed',
//        timeperiod__period_comment__edit:   'timeperiod:period_comment:edit',
//        timeperiod__period_rating__edit:    'timeperiod:period_rating:edit',
//        timeperiod__ppic_deleted:           'timeperiod:ppic_deleted',
//
//        messages__topvismess_changed:   'messages:topvismess_changed',
//        messages__clicked_load_future_but: 'messages:clicked_load_future_but',
//
//        messages__clicked_on_menu_but: 'messages:clicked_on_menu_but',
//        messages__message_deleted: 'messages:message_deleted',
//
//        friendship__answered_request: 'friendship:answered_request',
//        newmessage__show:           'newmessage:show',
//        newmessage__printscreen:    'newmessage:printscreen',
//
//        profile__show:              'profile:show',
//        profile__changed_userpic:   'profile:changed_userpic',
//        profile__changed_name:      'profile:changed_name',
//
//        newmessage__created_message:'newmessage:created_message',
////        comments__show:             'newmessage:created_message',
//        comments__new_comment:          'comments:new_comment',
//        comments__del_comment:          'comments:del_comment',
//        comments__clicked_on_menu_but:  'comments:clicked_on_menu_but',
//
//
//        bottomline__clicked_roll_but: 'bottomline:clicked_roll_but',
//
//        models__update:             'models:update',
//        jabber__got_message:        'jabber:got_message',
//
//        window__scroll:             'window:scroll',
//
//        document__end:              'document:end',
//        document__changed:          'document:changed',
//        document__height_changed:   'document:height_changed',
//
//        window__resize:             'window:resize'
    };

    var uniqId = 0;
    app.uniqId = function()
    {
        return uniqId++;
    };


    var APP = new soma.Application.extend({

        constructor: function()
        {
            soma.Application.call(this);
        },

        init: function()
        {

            this.injector.mapClass('api', app.services.api, true);

//            this.injector.mapClass('workspace', app.services.workspace, true);

            this.injector.mapValue('app_status',
                this.injector.createInstance(
                    app.services.app_status, sp(config.services, 'app_status', {}))
            );
//
//            this.injector.mapValue('workspace',
//                this.injector.createInstance(
//                    app.services.workspace, sp(config.services, 'workspace', {}))
//            );


//            this.injector.mapValue('currency_finder',
//                this.injector.createInstance(
//                    app.services.currency_finder,
//                    sp(config.services, 'currency_finder', {})));
//

//            this.commands.add(
//                app.e.IDE_OPEN_FILE,
//                app.commands.document.add_chart);
//
//            this.commands.add(
//                app.e.DOCUMENT__ON_SETUP_WIDJETS,
//                app.commands.document.setup_widjets);
//
//            this.commands.add(
//                app.e.DOCUMENT__ON_WIDJET_CHANGED,
//                app.commands.document.on_widjet_changed);
//
//            this.commands.add(
//                app.e.DOCUMENT__ON_WIDJET_CLOSED,
//                app.commands.document.on_widjet_closed);
//
//            this.commands.add(
//                app.e.QUOTES_FEED__ON_GOT_NEW_QUOTES,
//                app.commands.on_got_new_quotes);


        },
        start: function()
        {
            var dd = this.dispatcher;

            fork(function()
            {
                dd.dispatch(app.e.START, {

                });
            }, 100);


            var services = sp(config, 'services', {});

            if (services.document)
            {

                this.injector.createInstance(
                    app.services.document,
                    sp(services.document, 'info', {}));

            }

        }
    });


    var ideObj = new APP(config);


})(window.app = window.app || {}, config);

app.s = {};