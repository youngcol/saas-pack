;(function(app, undefined) {

'use strict';

// package
app.services = app.services || {};

app.services.new_post_window = function(__, config, dispatcher, api)
{
var pub = {}, f = {}, on={}, ui={}, _i = {}, self = this, $$ = {

    $box: null,

    state: {

    },

    openedPostCallbacks: null,
    openedPostModel: null,
    removeMediaIds: [],

    publish_params: {},
    pinterestBoards: null,

    $btn:   null,
    status: null,

    isFirstTimeOpen: true,
    uniqId: 0,

    timer:  null,

    loggin:
    {
        state: 1
    }
};

f.construct = function()
{
    $$ = app__def_comp_options('new_post_window', $$);
    _.extend($$, __);

    f.__s('big_bang');
    f.__s('bind_events');
    f.__s('drop_pictures_events');

};

f.__s = function(state, par)
{
!par && (par={});
log_tag($$, 'state', state, par);

switch (state) {

    case 'on_close_window':
    {
        if (par.reason == 'close_btn')
        {

        }

        // on close reset ui if edit post
        if ($$.openedPostModel)
        {
            f.__s('clear_window_input_data');
        }

        break;
    }

    case 'bind_events':
    {
        $$.$btn.click(function()
        {
            toggleModal($$.$box, {
                onClose: function () {
                    f.__s('on_close_window', {
                        reason: 'close_btn'
                    });
                }
            });

            if ($$.isFirstTimeOpen)
            {
                f.__s('on_first_time_window_open', {});
                $$.isFirstTimeOpen = false;
            }
        });

        $('.account .picture-holder', $$.$box).click(function ()
        {
            var accountId = $(this).parent().attr('data-id');

            f.__s('click_on_account', {accountId: accountId});
        });

        // send post buttons
        $('.publish-now-btn', $$.$box).click(function () {
            if ($$.openedPostModel)
            {
                f.__s('update_post', {creation_type: 'instantly'})
            }
            else
            {
                f.__s('create_post', {creation_type: 'instantly'})
            }
        });

        $('.schedule-btn', $$.$box).click(function () {
            if ($$.openedPostModel)
            {
                f.__s('update_post', {creation_type: 'scheduled'})
            }
            else
            {
                f.__s('create_post', {creation_type: 'scheduled'})
            }
        });

        $('.draft-btn', $$.$box).click(function () {
            if ($$.openedPostModel)
            {
                f.__s('update_post', {creation_type: 'draft'})
            }
            else
            {
                f.__s('create_post', {creation_type: 'draft'})
            }
        });

        $('.in-queue-btn', $$.$box).click(function () {
            if ($$.openedPostModel)
            {
                f.__s('update_post', {creation_type: 'queued'})
            }
            else
            {
                f.__s('create_post', {creation_type: 'queued'})
            }
        });

        $('.upload-pic-btn', $$.$box).click(function () {
            f.__s('click_upload_pic_btn', {})
        });

        $$.$inputFiles.change(function()
        {
            f.__s('input_file_changed', {file: this});
        });

        $('.uploaded-image', $$.$box).click(function ()
        {
            f.__s('remove_upload_image', {$img: $(this)});
        });

        break;
    }

    case 'on_first_time_window_open':
    {
        // load pinterest boards
        var pinterestAccountsIds =
            _.filter(user_social_accounts, {social_network_id: 1})
                .map(function (item) {
                    return item.id
                });

        api.req("POST", sp(routes, 'GetPinterestAccountsBoardsTask'), {
            accountIds: pinterestAccountsIds.join(',')
        }, {
            done: function (resp)
            {
                $$.pinterestBoards = resp.data.resGetPinterestAccountsBoardsTask.boards;

                $$.pinterestBoards.map(function (item) {

                    var $dropdown = $('.account.id-'+ item.account_id+ ' .dropdown-menu');
                    $dropdown.text('');

                    _.map(item.boards.data, function (board) {

                        $dropdown
                            .append('<a class="dropdown-item board-id-'+board.id+'" data-id="'+board.id+'" href="javascript:void(0)">'+board.name+'</a>');
                    });

                    $('.dropdown-item', $dropdown).click(function(){
                        $(this).toggleClass('active');
                        f.fillFormAccountsFromUi();
                    });

                    // opened post render lists
                    if ($$.openedPostModel)
                    {
                        var customAccounts = sp($$.openedPostModel.scheduled_time.publish_params.custom_params, 'accounts');
                        if (customAccounts)
                        {
                            customAccounts.map(function (item) {
                                var $account = $('.account.id-'+ item.account_id);
                                item.board_ids.map(function (boardId) {
                                    $('.board-id-'+ boardId, $account).addClass('active');
                                });
                            });
                        }
                        f.fillFormAccountsFromUi();
                    }

                });
            },
            fail: function ()
            {

            }
        });

        break;
    }

    case 'remove_upload_image':
    {
        var index = par.$img.attr('data-index');
        var $input = $('.media' + index, $$.$box);
        $input.val('');

        var $image = $('.uploaded-image.image'+index, $$.$box);
        $image.addClass('hidden');

        var mediaId = $image.attr('data-media-id');
        $image.removeAttr('data-media-id');

        if (mediaId)
        {
            $$.removeMediaIds.push(mediaId);
        }

        break;
    }

    case 'click_on_account':
    {
        var $account = $('.account.id-'+ par.accountId, $$.$box);
        $account.toggleClass('active');
        $('.dropdown-link', $account).toggleClass('disabled');

        f.fillFormAccountsFromUi();

        break;
    }

    case 'input_file_changed':
    {
        var $file = $(par.file);

        var
            index = $file.attr('data-index'),
            file,
            reader = new FileReader();

        if( par.file.files.item )
        {
            file = par.file.files.item(0);
        }
        else
        {
            file = par.file.files[0];
        }

        if(file)
        {
            reader.onload = function(e)
            {
                f.showPreviewHolder(index, e.target.result);
            };

            reader.readAsDataURL(file);
        }

        break;
    }

    case 'click_upload_pic_btn':
    {
        var index = f.getImageFreeIndex();

        if (index)
        {
            $('.media' + index, $$.$box).click();
        }

        break;
    }

    case 'drop_pictures_events':
    {
        $$.$box
            .bind( 'dragenter', function(event){
                event.stopPropagation();
                event.preventDefault();
            })
            .bind( 'dragover', function(event){
                event.stopPropagation();
                event.preventDefault();
            })
            .bind('drop', function(dropEvent)
            {
                dropEvent.stopPropagation();
                dropEvent.preventDefault();

                f.__s('dropped_file_to_box', {dropEvent: dropEvent});
            });
        break;
    }

    case 'dropped_file_to_box':
    {
        var files = par.dropEvent.originalEvent.dataTransfer.files;

        for(var i=0;i<files.length; i++)
        {
            var droppedFile = files[i];
            var index = f.getImageFreeIndex();

            if (index)
            {
                var fileReader = new FileReader();
                fileReader.onload = function(onloadEvent)
                {
                    f.showPreviewHolder(index, onloadEvent.target.result);
                };

                fileReader.readAsDataURL(droppedFile);
            }
        }

        break;
    }

    case 'clear_window_input_data':
    {
        $$.$inputFiles.val('');

        $('.account', $$.$box).each(function (i, item) {
            $(item).removeClass('active');
            $('.dropdown-link', $(item)).addClass('disabled');
        });

        $$.$messageText.val('');
        $$.$sheduledTime.text('');

        _.map($('.media', $$.$box), function (image) {
            f.__s('remove_upload_image', {$img: $(image)});
        });

        _.map($('.uploaded-image', $$.$box), function (image) {
            f.__s('remove_upload_image', {$img: $(image)});
        });

        // opened post data
        $$.removeMediaIds = [];
        $$.openedPostModel = null;
        $$.openedPostCallbacks = null;

        break;
    }

    case 'render_opened_post':
    {
        // $$.openedPostModel.sheduledTime

        // accounts
        var accountIds = $$.openedPostModel.scheduled_time.publish_params.social_accounts;

        _.map(accountIds, function (id) {
            f.__s('click_on_account', {accountId: id});
        });

        var customAccounts = sp($$.openedPostModel.scheduled_time.publish_params.custom_params, 'accounts');
        if (customAccounts)
        {
            customAccounts.map(function (item) {
                var $account = $('.account.id-'+ item.account_id);
                item.board_ids.map(function (boardId) {
                    $('.board-id-'+ boardId, $account).addClass('active');
                });
            });
        }

        f.fillFormAccountsFromUi();

        // pictures
        var mediaFiles = $$.openedPostModel.getMediaFiles();
        _.map(mediaFiles, function (mediaFile, index) {
            f.showPreviewHolder(index+1, mediaFile.url, mediaFile.id);
        });

        // text
        $$.$messageText.val($$.openedPostModel.text);

        // scheduled time
        if ($$.openedPostModel.scheduled_time.process_status != 'draft')
        {
            $$.$sheduledTime.text(
                $$.openedPostModel.scheduled_time.scheduled_time
            );
        }

        break;
    }

    case 'update_post':
    {
        var validation = f.validateFormBeforeSending();
        if (!validation) break;
        //-==-=-=-=-=-=

        var fd = f.getUiFormData();
        fd.append('creation_type', par.creation_type);
        fd.append('post_id', $$.openedPostModel.id);
        fd.append('remove_media_ids', $$.removeMediaIds);
        // fd.append('scheduled_time', '0');


        var url = $$.$form.attr('data-update-url');
        api.req("POST", url, fd, {
            done: function (resp)
            {

                if ($$.openedPostCallbacks)
                {
                    $$.openedPostCallbacks.onCloseAfterUserAction({
                        action: par.creation_type,
                        postId: $$.openedPostModel.id
                    });
                }

                f.__s('on_close_window', {
                    reason: 'close_btn'
                });

                closeModal($$.$box);

                f.showPostCreationTypeNotify(par.creation_type);

            },
            fail: function ()
            {

            }
        }, {
            contentType: false,
            processData: false,
        });


        break;
    }

    case 'create_post':
    {
        var validation = f.validateFormBeforeSending();

        if (!validation)
        {
            break;
        }

        var fd = f.getUiFormData();
        fd.append('creation_type', par.creation_type);

        // fd.append('scheduled_time', '0');


        api.req("POST", $$.$form.attr('action'), fd, {
            done: function (resp)
            {
                f.__s('on_close_window', {
                    reason: 'close_btn'
                });

                closeModal($$.$box);

                f.showPostCreationTypeNotify(par.creation_type);

                f.__s('clear_window_input_data');
            },
            fail: function ()
            {

            }
        }, {
            contentType: false,
            processData: false,
        });

        break;
    }

    case 'big_bang':
    {
        $$.$box = $('#new-post-window');
        $$.$btn = $('.new-post-btn');
        $$.$inputFiles = $('.media');
        $$.$form = $('.post-form', $$.$box);
        $$.$socialAccounts = $('.social_accounts', $$.$box);
        $$.$messageText = $('.text', $$.$box);
        $$.$sheduledTime = $('.sheduled-time', $$.$box);

        break;
    }
}
};

f.fillFormAccountsFromUi = function()
{
    var accounts = [];
    var accountIds = _.map($('.account.active', $$.$box), function (account) {
        var $acc = $(account);
        var accountId = $acc.attr('data-id');

        if ($acc.hasClass('pinterest'))
        {
            var boardIds = [];
            $('.dropdown-item.active', $acc).each(function (i, item) {
                boardIds.push($(item).attr('data-id'));
            });

            accounts.push({account_id: accountId, board_ids: boardIds});
        }

        return accountId;
    });

    $$.publish_params.accounts = accounts;
    $$.$socialAccounts.val(accountIds.join(','));
};

f.getUiFormData = function()
{
    var fd = new FormData();

    _.map($$.$form.serializeArray(), function(item){
        fd.append(item.name, item.value);
    });

    _.map($('.media', $$.$box), function (file, i) {
        if (file.files[0])
        {
            fd.append('media' + (i+1), file.files[0]);
        }
    });

    fd.append('publish_params', JSON.stringify($$.publish_params));

    return fd;
};

f.validateFormBeforeSending = function()
{
    var ret = true;

    var socialAccounts = $$.$socialAccounts.val();

    if (socialAccounts.length == 0)
    {
        errorNotify('Select social accounts');
        ret = false;
    }

    return ret;
};

f.getImageFreeIndex = function()
{

    if ($('.hidden.uploaded-image.image1', $$.$box).length === 1)
    {
        return 1;
    }

    if ($('.hidden.uploaded-image.image2', $$.$box).length === 1)
    {
        return 2;
    }

    if ($('.hidden.uploaded-image.image3', $$.$box).length === 1)
    {
        return 3;
    }

    return null;
};


f.showPostCreationTypeNotify = function(creationType)
{
    if (creationType === 'draft')
    {
        successNotify('Post is saved');
    }
    else if (creationType === 'instantly')
    {
        successNotify('Post is published');
    }
    else if(creationType === 'scheduled')
    {
        successNotify('Post is scheduled');
    }
    else if(creationType === 'queued')
    {
        successNotify('Post is scheduled');
    }
};

f.showPreviewHolder = (index, imgSrc, mediaId) =>
{
    var $previewHolder = $('.uploaded-image.image'+ index, $$.$box);
    $('img', $previewHolder).attr('src', imgSrc);
    $previewHolder.removeClass('hidden');

    if (mediaId)
    {
        $previewHolder.attr('data-media-id', mediaId);
    }
};

pub.test = function()
{


};

pub.openPost = function(postModel, callb)
{
    $$.openedPostModel = postModel;
    $$.openedPostCallbacks = callb;
    
    toggleModal($$.$box, {
        onClose: function () {
            f.__s('on_close_window', {
                reason: 'close_btn'
            });
        }
    }, {force_open: 1});

    f.__s('render_opened_post');

    if ($$.isFirstTimeOpen)
    {
        f.__s('on_first_time_window_open', {});
        $$.isFirstTimeOpen = false;
    }
};

f.construct();

$$._i = {
    test:    pub.test,
    openPost: pub.openPost,

};

return $$._i;
};


})(window.app = window.app || {});
