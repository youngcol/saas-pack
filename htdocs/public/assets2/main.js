if (window.matchMedia("(hover: hover)").matches) {
	document.getElementById('page-header').classList.add('hover');
}

// share popup code
window.sharePopup = function(url) {
    let width = 600;
    let height = 400;
    let leftPosition, topPosition;

    leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
    topPosition = (window.screen.height / 2) - ((height / 2) + 50);

    let windowFeatures = "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";

    window.open(url,'Social Share', windowFeatures);
};

// pill section tabs
window.pillSection = function(e) {
    e.stopPropagation();

    let pillBtn = e.target;
    let pillBtns = pillBtn.parentNode.querySelectorAll('.pill-button');
    let paneNum = pillBtn.getAttribute('data-section');
    let pillBox = pillBtn.parentNode.parentNode.parentNode;

    // set all buttons to inactive
    pillBtns.forEach( function(btn) {
        btn.classList.remove('active');
    });
    // set active button
    pillBtn.classList.add('active');

    // hide all panes
    pillBox.querySelectorAll('.tab-section').forEach( function(pane) {
        pane.style.display = 'none';
    });
    // show the active pane
    pillBox.querySelectorAll('.section-' + paneNum)[0].style.display = 'block';

    // set all pills to inactive
    pillBox.querySelectorAll('.tab-section-pills i').forEach( function(pill) {
        pill.classList.remove('active');
    });

    // set ative pill
    pillBox.querySelectorAll('.pill-' + paneNum)[0].classList.add('active');
};

// price plan swap
window.togglePriceRates = function(e) {
    e.stopPropagation();

    let planToggle = e.target;
    let fadedClass = 'faded';
    let offClass = 'toggled-off';

    let toggleContainer = planToggle.parentNode.parentNode.parentNode.parentNode;

    let monthlyViews = toggleContainer.querySelectorAll('.monthly');
    let yearlyViews = toggleContainer.querySelectorAll('.yearly');
    let toggleLabel = toggleContainer.parentNode.querySelectorAll('.bill-label');
    let toggleSwitch = toggleContainer.querySelectorAll('.switch');
    let toggleBox = toggleContainer.parentNode.querySelector('.switch > input');

    // toggle switch if we're in that context
    if ( toggleSwitch.length == 0 ) {
        if ( toggleBox.checked == true ) {
            toggleBox.checked = false;
        } else {
            toggleBox.checked = true;
        }
    }

    // toggle label class
    if ( toggleLabel[0].classList.contains(fadedClass) ) {
        toggleLabel[0].classList.remove(fadedClass);
        toggleLabel[1].classList.add(fadedClass);
    } else {
        toggleLabel[0].classList.add(fadedClass);
        toggleLabel[1].classList.remove(fadedClass);
    }

    // toggle monthly and yearly elements
    if ( monthlyViews[0].classList.contains(offClass) ) {
        monthlyViews.forEach( function(monthly) {
            monthly.classList.remove(offClass);
        });

        yearlyViews.forEach( function(yearly) {
            yearly.classList.add(offClass);
        });
    } else {
        monthlyViews.forEach( function(monthly) {
            monthly.classList.add(offClass);
        });

        yearlyViews.forEach( function(yearly) {
            yearly.classList.remove(offClass);
        });
    }
};

// qna section
window.toggleQandA = function(e) {
    e.preventDefault();
    e.stopPropagation();

    let onClass = 'active';
    let thisQ = e.target;
    let thisBox = thisQ.parentNode.parentNode;
    let thisQnA = thisBox.parentNode.querySelectorAll('.qna');

    if ( thisBox.classList.contains(onClass) ) {
        thisQnA.forEach( function(qna) {
            qna.classList.remove(onClass);
        });

        thisBox.classList.remove(onClass);
    } else {
        thisQnA.forEach( function(qna) {
            qna.classList.remove(onClass);
        });

        thisBox.classList.add(onClass);
    }
};

// change tabs
window.changeTab = function(tabs, num) {
    let tabPanels = tabs.querySelectorAll('.tab-panel');
    let tabItems = tabs.querySelectorAll('.tab');
    let tabLinks = tabs.querySelectorAll('.tab a');

    tabLinks.forEach( function(link, index) {
        link.addEventListener('click', function(e) {
            e.preventDefault();

            let linkTarget = tabs.querySelectorAll('.tab-panel[data-tab="tab-panel-' + (index + 1) + '"]');

            for ( let i = 0; i < tabPanels.length; i++ ) {
                tabPanels.item(i).classList.remove('show');
                tabPanels.item(i).classList.remove('shown');
            }

            for ( let i = 0; i < tabItems.length; i++ ) {
                tabItems.item(i).classList.remove('is-selected');
            }

            link.parentNode.classList.add('is-selected');
            linkTarget[0].classList.add('show');

            setTimeout( function(e) {
                linkTarget[0].classList.add('shown');
            }, 750);
        });
    });
};

// on ready
document.onreadystatechange = function(e) {
     // loading, interactive or complete (https://developer.mozilla.org/en-US/docs/Web/API/Document/readyState)
    if (document.readyState === 'complete') {
        var headerPanel = document.getElementById('page-content').firstChild.innerHTML;
    }
};

// on load
window.onload = function(e) {
    // constants
    const onClass = 'toggled-on';

    // main navigation
    const mainMenu = document.getElementById('site-navigation');
    const loginMenu = document.getElementById('login-navigation');
    const menuToggle = document.getElementsByClassName('menu-toggle')[0];
    const lightsOut = document.getElementById('lightsout');

    menuToggle.addEventListener('click', function(e) {
        if ( mainMenu.classList.contains(onClass) ) {
            lightsOut.classList.remove(onClass);
            mainMenu.classList.remove(onClass);
            loginMenu.classList.remove(onClass);
            menuToggle.classList.remove('is-active');
        } else {
            lightsOut.classList.add(onClass);
            mainMenu.classList.add(onClass);
            loginMenu.classList.add(onClass);
            menuToggle.classList.add('is-active');
            // expand the first sub-menu
            mainMenu.getElementsByClassName('sub-menu-wrap')[0].classList.add(onClass);
        }
    });

    // menu click
    const menuList = document.getElementById('top-menu');
    const menuLinks = menuList.querySelectorAll('#top-menu > li > a');

    menuLinks.forEach( function(menuLink) {
        menuLink.addEventListener('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            let subMenu = menuLink.parentNode.getElementsByClassName('sub-menu-wrap')[0];
            let linkLi = menuLink.parentNode;

            if ( subMenu !== undefined ) {
                if ( subMenu.classList.contains(onClass) ) {
                    lightsOut.classList.remove(onClass);
                    subMenu.classList.remove(onClass);
                    linkLi.classList.remove('active');
                    menuLink.classList.remove('active');
                } else {
                    lightsOut.classList.add(onClass);
                    subMenu.classList.add(onClass);
                    menuLink.classList.add('active');
                    linkLi.classList.add('active');
                }
            } else {
                window.location = menuLink.href;
            }
        });

        let inputs = document.querySelectorAll('.inline_wrap input, .inline_wrap textarea');
        for (let i = 0; i < inputs.length; i++) {
            if(inputs[i].value != ""){
                inputs[i].closest('div').classList.add('active');
            }
        }
    });

    // smooth anchor links
    var links = document.getElementsByTagName("A");
    for(var i=0; i < links.length; i++) {
      if(!links[i].hash) continue;
      if(links[i].origin + links[i].pathname != self.location.href) continue;
      (function(anchorPoint) {
        links[i].addEventListener("click", function(e) {
          let theHash = this.hash;

          anchorPoint.scrollIntoView({behavior: "smooth"}, true);
          e.preventDefault();

          setTimeout(function(evt) {
            self.location.hash = theHash;
          }, 1000);
        }, false);
      })(document.getElementById(links[i].hash.replace(/#/, "")));
    };

    // lights out
    lightsOut.addEventListener('click', function(e) {
        document.querySelectorAll('#top-menu .sub-menu-wrap').forEach( function(subMenus) {
            e.target.classList.remove(onClass);
            subMenus.classList.remove(onClass);
        });
        menuLinks.forEach( function(menuLink) {
            menuLink.classList.remove('active');
        });
    });

    // tabs
    const tabSets = document.querySelectorAll('.vertical_tabs .tab-container');

    tabSets.forEach( function(tabSet, i) {
        window.changeTab(tabSet, i);
    });

    setTimeout(function(e) {
        let hasTabs = document.querySelectorAll('.tab-panel.show');

        if ( hasTabs.length ) {
            hasTabs.forEach( function(hasTab) {
                hasTab.classList.add('shown');
            });
        }
    }, 1000);

    // flickity for tabs
    const flkty = [];
    const tabsElem = document.querySelectorAll('.tab-container .grid.left');

    tabsElem.forEach( function(tabElem, index) {
        flkty[index] = new Flickity( tabElem, {
            watchCSS: true,
            cellAlign: 'left',
            contain: true,
            pageDots: false,
            prevNextButtons: false,
        });

        flkty[index].on('change', function(i) {
            // let tabLinks = tabElem.querySelectorAll('.tab a');
            this.selectCell( i );
           // return tabLinks[i].click();
        });
    });
};

// on scroll
window.onscroll = function() {
    const hideNavClass = 'hidden';
    const showNavClass = 'visible';
    const primaryNav = document.getElementById('nav-container');
    const secondaryNav = document.getElementById('nav-container-secondary');
    const headerPanel = document.getElementById('page-content').firstElementChild;
    let headerHeight = headerPanel.offsetHeight; 

    if ( secondaryNav ) {
        setTimeout( function(e) {
            let scrolledFromTop = window.pageYOffset;

            if ( scrolledFromTop >= headerHeight ) {
                primaryNav.classList.add(hideNavClass);

                setTimeout( function() {
                    secondaryNav.classList.add(showNavClass);
                }, 250);
            } else {
                primaryNav.classList.remove(hideNavClass);

                setTimeout( function() {
                    secondaryNav.classList.remove(showNavClass);
                }, 250);
            }
        }, 300);
    }
};

window.getClosest = function(elem, selector) {
    // Element.matches() polyfill
    if (!Element.prototype.matches) {
        Element.prototype.matches =
            Element.prototype.matchesSelector ||
            Element.prototype.mozMatchesSelector ||
            Element.prototype.msMatchesSelector ||
            Element.prototype.oMatchesSelector ||
            Element.prototype.webkitMatchesSelector ||
            function(s) {
                let matches = (this.document || this.ownerDocument).querySelectorAll(s);
                let i = matches.length;

                while (--i >= 0 && matches.item(i) !== this) {}

                return i > -1;
            };
    }

    // Get the closest matching element
    for ( ; elem && elem !== document; elem = elem.parentNode ) {
        if ( elem.matches( selector ) ) return elem;
    }
    return null;
};

(function() {
    'use strict';

    let inputs = document.querySelectorAll('.inline_wrap input, .inline_wrap textarea, .gfield input, .gfield textarea');

    function onFocus(e) {
        e.target.closest('div').classList.add('active');
        e.target.closest('li').classList.add('active');
    }

    function onBlur(e) {
        if (e.target.value == '') {
            e.target.closest('div').classList.remove('active');
            e.target.closest('li').classList.remove('active');
        }
    }
    for (let i = 0, l = inputs.length; i < l; i++) {
        inputs[i].addEventListener('focus', onFocus);
        inputs[i].addEventListener('blur', onBlur);
    }
    
})();

// Checking if we have a gravity form select and adding a class
function addParentClass(selector, myClass) {

    // get all elements that match our selector
    elements = document.querySelectorAll(selector);
  
    // add class to all chosen elements
    for (var i=0; i<elements.length; i++) {
      elements[i].parentNode.classList.add(myClass);
    }
  }
  
  // lets add a class:
  addParentClass('.ginput_container_select', 'tisaselect');

  


// Homepage Animation

const tools = document.getElementById('tools');
const cb = document.getElementById('cb');
const rep = document.getElementById('rep');
const lead = document.getElementById('lead');
const toolsnav = document.getElementById('toolsnav');
const toolclick = document.getElementById('toolclick');
const cbnav = document.getElementById('cbnav');
const cbclick = document.getElementById('cbclick');
const repnav = document.getElementById('repnav');
const repclick = document.getElementById('repclick');
const leadnav = document.getElementById('leadnav');
const leadclick = document.getElementById('leadclick');
const slides = document.querySelectorAll('.carousel-main .cell');
const navLinks = document.querySelectorAll('.slide-nav .carousel-cell');

if(document.getElementById('heroAnime')) {

   //  document.onreadystatechange = function(e) {
   //      // loading, interactive or complete (https://developer.mozilla.org/en-US/docs/Web/API/Document/readyState)
   //     if (document.readyState === 'complete') {
   //         // Pause all scenes except Tools
   //          HYPE.documents['citation-builder'].pauseTimelineNamed('Main Timeline');
   //          HYPE.documents['reputation-manager'].pauseTimelineNamed('Main Timeline');
   //          HYPE.documents['leadgen'].pauseTimelineNamed('Main Timeline');
   //     }
   // };

   toolclick.addEventListener('click',function(e){
        goToTools();
    });
    cbclick.addEventListener('click',function(e){
        goToCB();
    });
    repclick.addEventListener('click',function(e){
        goToRM();
    });
    leadclick.addEventListener('click',function(e){
        goToLeads();
    });

    function goToTools(e) {
        navLinks.forEach( function(navLink) {
            if ( navLink.classList.contains('active') ) {
            navLink.classList.remove('active');
            navLink.classList.add('hidep');
            }
        });
        toolsnav.classList.add('active');
        toolsnav.classList.remove('hidep');
        
        slides.forEach( function(slide) {
            if ( slide.classList.contains('active') ) {
            slide.classList.remove('active');
            }
        });
        tools.classList.add('active');
        // HYPE.documents['Homepage-animation-tools'].showSceneNamed('Rank Checker');
        // HYPE.documents['citation-builder'].pauseTimelineNamed('Main Timeline');
        // HYPE.documents['reputation-manager'].pauseTimelineNamed('Main Timeline');
        // HYPE.documents['leadgen'].pauseTimelineNamed('Main Timeline');
    }
    
    function goToCB(e) {
        navLinks.forEach( function(navLink) {
            if ( navLink.classList.contains('active') ) {
            navLink.classList.remove('active');
            navLink.classList.add('hidep');
            }
        });
        cbnav.classList.add('active');
        cbnav.classList.remove('hidep');
        
        slides.forEach( function(slide) {
            if ( slide.classList.contains('active') ) {
            slide.classList.remove('active');
            }
        });
        cb.classList.add('active');
        // HYPE.documents['citation-builder'].showSceneNamed('CB');
        // HYPE.documents['Homepage-animation-tools'].pauseTimelineNamed('Main Timeline');
        // HYPE.documents['reputation-manager'].pauseTimelineNamed('Main Timeline');
        // HYPE.documents['leadgen'].pauseTimelineNamed('Main Timeline');
    }
    
    function goToRM(e) {
        navLinks.forEach( function(navLink) {
            if ( navLink.classList.contains('active') ) {
            navLink.classList.remove('active');
            navLink.classList.add('hidep');
            }
        });
        repnav.classList.add('active');
        repnav.classList.remove('hidep');
        
        slides.forEach( function(slide) {
            if ( slide.classList.contains('active') ) {
            slide.classList.remove('active');
            }
        });
        rep.classList.add('active');
        // HYPE.documents['reputation-manager'].showSceneNamed('CB');
        // HYPE.documents['Homepage-animation-tools'].pauseTimelineNamed('Main Timeline');
        // HYPE.documents['citation-builder'].pauseTimelineNamed('Main Timeline');
        // HYPE.documents['leadgen'].pauseTimelineNamed('Main Timeline');
    }

    function goToLeads(e) {
        navLinks.forEach( function(navLink) {
            if ( navLink.classList.contains('active') ) {
            navLink.classList.remove('active');
            navLink.classList.add('hidep');
            }
        });
        leadnav.classList.add('active');
        leadnav.classList.remove('hidep');
        
        slides.forEach( function(slide) {
            if ( slide.classList.contains('active') ) {
            slide.classList.remove('active');
            }
        });
        lead.classList.add('active');
        // HYPE.documents['leadgen'].showSceneNamed('CB');
        // HYPE.documents['Homepage-animation-tools'].pauseTimelineNamed('Main Timeline');
        // HYPE.documents['citation-builder'].pauseTimelineNamed('Main Timeline');
        // HYPE.documents['reputation-manager'].pauseTimelineNamed('Main Timeline');
    }

    window._wq = window._wq || [];

    // var toolsVid = Wistia.api("hc84vpiz5k");
    // var cbVid = Wistia.api("4ez48u2li3");
    // var rmVid = Wistia.api("0dge4d2pff");

    function playToolsVid() {
        _wq.push({ id: 'hc84vpiz5k', onReady: function(video) {
            console.log("Tools Vid!", video);
            var toolsVid = Wistia.api("hc84vpiz5k");
            toolsVid.popover.show();
            toolsVid.play();
        }});
    }
    function playCbVid() {
        _wq.push({ id: '4ez48u2li3', onReady: function(video) {
            console.log("CB vid!", video);
            var cbVid = Wistia.api("4ez48u2li3");
            cbVid.popover.show();
            cbVid.play();
        }});
    }
    function playRmVid() {
        _wq.push({ id: '0dge4d2pff', onReady: function(video) {
            console.log("RM vid!", video);
            var rmVid = Wistia.api("0dge4d2pff");
            rmVid.popover.show();
            rmVid.play();
        }});
    }
    function playLeadgenVid() {
        window.location.href = '/lead-generation-for-marketing-agencies/'
    }

}

// BL Team Department Chooser
if (document.getElementById('depChoose')) {

    const teamSelect = document.getElementById('depChoose');

    function showteam() {
        var department = document.getElementById('depChoose').value;

        document.querySelectorAll('#peeps .member').forEach( function(teamer) {
            if (teamer.classList.contains(department)) {
                // teamer.classList.add('hello');
                setTimeout( function(e) {
                    teamer.classList.add('grow');
                }, 200);
                setTimeout( function(e) {
                    teamer.classList.add('hello');
                }, 400);
                setTimeout( function(e) {
                    teamer.classList.add('fadein');
                }, 600);
            } else {
                teamer.classList.remove('fadein');
                setTimeout( function(e) {
                    teamer.classList.remove('hello');
                }, 150);
                setTimeout( function(e) {
                    teamer.classList.remove('grow');
                }, 300);
            }
        });
    }

    // teamSelect.addEventListener('input', function (evt) {
    teamSelect.addEventListener('change', function() {
        showteam(); // Do something damnit
    });

    showteam();
} // ends if

// reputation Manager review sites
if (document.getElementById('countryChoose')) {
    
    function showSites() {
        var country = document.getElementById('countryChoose').value;
        var category = document.getElementById('categoryChoose').value;
        
        if (country == 'usa') {
            document.querySelectorAll('#rm-sites .reviewsite').forEach( function(teamer) {
                if (teamer.classList.contains(country) && teamer.classList.contains(category) ) {
                  // teamer.classList.add('hello');
                  setTimeout( function(e) {
                      teamer.classList.add('grow');
                  }, 200);
                  setTimeout( function(e) {
                      teamer.classList.add('hello');
                  }, 400);
                  setTimeout( function(e) {
                      teamer.classList.add('fadein');
                  }, 600);
                } else {
                  teamer.classList.remove('fadein');
                  setTimeout( function(e) {
                      teamer.classList.remove('hello');
                  }, 150);
                  setTimeout( function(e) {
                      teamer.classList.remove('grow');
                  }, 300);
              }
          });
        } else {
            var categoryElement = document.getElementById('categoryChoose');
            
            categoryElement.classList.add('hideme');
            document.querySelectorAll('#rm-sites .reviewsite').forEach( function(teamer) {
                if (teamer.classList.contains(country)) {
                  // teamer.classList.add('hello');
                  setTimeout( function(e) {
                      teamer.classList.add('grow');
                  }, 200);
                  setTimeout( function(e) {
                      teamer.classList.add('hello');
                  }, 400);
                  setTimeout( function(e) {
                      teamer.classList.add('fadein');
                  }, 600);
                } else {
                  teamer.classList.remove('fadein');
                  setTimeout( function(e) {
                      teamer.classList.remove('hello');
                  }, 150);
                  setTimeout( function(e) {
                      teamer.classList.remove('grow');
                  }, 300);
              }
          });
        }
    }
    
    var countrySelect = document.getElementById('countryChoose');
    var categorySelect = document.getElementById('categoryChoose');
    
    countrySelect.addEventListener('change', function() {
        showSites(); // Do something damnit
    });
    categorySelect.addEventListener('change', function() {
        showSites(); // Do something damnit
    });

    showSites();
} // ends if
