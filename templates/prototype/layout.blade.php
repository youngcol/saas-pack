<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Showcases a horizontal menu that hides at
     small window widths, and which scrolls when revealed.">
    <title>&para;&para;PROTO </title>

    <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/pure-min.css" integrity="sha384-nn4HPE8lTHyVtfCBi5yW9d20FjT8BJwUXyWZT9InLYax14RDjBj46LmSztkmNP9w" crossorigin="anonymous">
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <link rel="stylesheet" href="/prototype/main.css">

    <script src="/prototype/datepicker/jquery.datetimepicker.js"></script>
    <link rel="stylesheet" href="/prototype/datepicker/jquery.datetimepicker.css">

</head>
<body>


<div class="custom-menu-wrapper">
    <div class="pure-menu custom-menu custom-menu-top">
        <a href="#" class="custom-menu-toggle" id="toggle"><s class="bar"></s><s class="bar"></s></a>
    </div>
    <div class="pure-menu pure-menu-horizontal pure-menu-scrollable custom-menu custom-menu-bottom custom-menu-tucked" id="tuckedMenu">
        <div class="custom-menu-screen"></div>
        <ul class="pure-menu-list">
            <li class="pure-menu-item {{($page=='home' ? 'active' : '')}}"><a href="{{route('proto.index')}}" class="pure-menu-link">Home</a></li>
            <li class="pure-menu-item {{($page=='posts' ? 'active' : '')}}"><a href="{{route('proto.posts')}}" class="pure-menu-link">Posts</a></li>
            <li class="pure-menu-item {{($page=='queue' ? 'active' : '')}} "><a href="#" class="pure-menu-link">Queue</a></li>
            <li class="pure-menu-item {{($page=='scheduled' ? 'active' : '')}}"><a href="#" class="pure-menu-link">Scheduled</a></li>

            {{--<li class="pure-menu-item"><a href="#" class="pure-menu-link">Blog</a></li>--}}
            {{--<li class="pure-menu-item"><a href="#" class="pure-menu-link">GitHub</a></li>--}}
            {{--<li class="pure-menu-item"><a href="#" class="pure-menu-link">Twitter</a></li>--}}
            {{--<li class="pure-menu-item"><a href="#" class="pure-menu-link">Apple</a></li>--}}
            {{--<li class="pure-menu-item"><a href="#" class="pure-menu-link">Google</a></li>--}}
            {{--<li class="pure-menu-item"><a href="#" class="pure-menu-link">Wang</a></li>--}}
            {{--<li class="pure-menu-item"><a href="#" class="pure-menu-link">Yahoo</a></li>--}}
            {{--<li class="pure-menu-item"><a href="#" class="pure-menu-link">W3C</a></li>--}}
        </ul>
    </div>
    <hr>
</div>


<style>
    .main
    {
        padding-left: 20px;
        padding-right: 20px;
    }
</style>

<div class="main">

    @yield('content')

</div>

@yield('js_scripts')

</body>
</html>

