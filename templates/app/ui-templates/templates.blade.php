@extends("app.layout")


@section('content')

    <div class="container templates">

        <h1 class="title m-top-20 has-text-centered">Templates</h1>

{{--        <div class="level">--}}
{{--            --}}
{{--            <h1>Templates</h1>--}}
{{--        </div>--}}

        <hr>
        <h2 id="" class="title">Section start</h2>

        <section class="section-start">

            <h2 class="title section-start">New ways to come onboard</h2>

            <div class="box">
                <h4 id="const" class="title is-3">const</h4>
                <article class="message is-primary">
                  <span class="icon has-text-primary">
                  <i class="fab fa-js"></i>
                  </span>
                    <div class="message-body">
                        Block-scoped. Cannot be re-assigned. Not immutable.
                    </div>
                </article>
                <pre class=" language-javascript"><code class=" language-javascript"><span class="token keyword">const</span> test <span class="token operator">=</span> <span class="token string">'test'</span><span class="token punctuation">;</span></code></pre>
            </div>
        </section>

        <section class="section-start">

            <h2 class="title section-start">How to make money on fb bots?</h2>

            <div class="box">
                <h4 id="const" class="title is-3">const</h4>
                <article class="message is-primary">
                  <span class="icon has-text-primary">
                  <i class="fab fa-js"></i>
                  </span>
                    <div class="message-body">
                        Block-scoped. Cannot be re-assigned. Not immutable.
                    </div>
                </article>
                <pre class=" language-javascript"><code class=" language-javascript"><span class="token keyword">const</span> test <span class="token operator">=</span> <span class="token string">'test'</span><span class="token punctuation">;</span></code></pre>
            </div>
        </section>


        <hr>
        <h2 id="" class="title">Menu</h2>

        <div class="column is-3 ">
            <aside class="menu is-hidden-mobile">
                <p class="menu-label">
                    General
                </p>
                <ul class="menu-list">
                    <li><a href="#" class="is-active">Account settings</a></li>
                    <li><a href="#" class="">Change password</a></li>
                </ul>

                <p class="menu-label">
                    Transactions
                </p>
                <ul class="menu-list">
                    <li><a>Payments</a></li>
                    <li><a>Transfers</a></li>
                    <li><a>Balance</a></li>
                </ul>
            </aside>
        </div>

        <hr>
        <h2 id="" class="title">Hero and info-tiles</h2>

        <nav class="breadcrumb" aria-label="breadcrumbs">
            <ul>
                <li><a href="../..">Bulma</a></li>
                <li><a href="../..">Templates</a></li>
                <li><a href="../..">Examples</a></li>
                <li class="is-active"><a href="#" aria-current="page">Admin</a></li>
            </ul>
        </nav>

        <section class="hero is-info welcome is-small">
            <div class="hero-body">
                <div class="container">
                    <h1 class="title">
                        Hello, Admin.
                    </h1>
                    <h2 class="subtitle">
                        I hope you are having a great day!
                    </h2>
                </div>
            </div>
        </section>

        <hr>

        <section class="info-tiles">
            <div class="tile is-ancestor has-text-centered">
                <div class="tile is-parent">
                    <article class="tile is-child box">
                        <p class="title">439k</p>
                        <p class="subtitle">Users</p>
                    </article>
                </div>
                <div class="tile is-parent">
                    <article class="tile is-child box">
                        <p class="title">59k</p>
                        <p class="subtitle">Products</p>
                    </article>
                </div>
                <div class="tile is-parent">
                    <article class="tile is-child box">
                        <p class="title">3.4k</p>
                        <p class="subtitle">Open Orders</p>
                    </article>
                </div>
                <div class="tile is-parent">
                    <article class="tile is-child box">
                        <p class="title">19</p>
                        <p class="subtitle">Exceptions</p>
                    </article>
                </div>
            </div>
        </section>

        <hr>
        <h2 id="" class="title">Form</h2>

        <div class="box">
            <div class="field">
                <label class="label">Name</label>
                <div class="control">
                    <input class="input" type="text" placeholder="Text input">
                </div>
            </div>

            <div class="field">
                <label class="label">Email</label>
                <div class="control has-icons-left">
                    <input class="input" type="email" placeholder="Email input" value="">
                    <span class="icon is-small is-left">
                  <i class="fas fa-envelope"></i>
                </span>
                </div>
            </div>

            <div class="field">
                <label class="label">Message</label>
                <div class="control">
                    <textarea class="textarea" placeholder="Textarea"></textarea>
                </div>
            </div>

            <div class="field is-grouped has-text-centered">
                <div class="control">
                    <button class="button is-link is-large"><span class="icon">
                    <i class="fas fa-envelope"></i>
                  </span>
                        <span>Submit</span></button>
                </div>
            </div>
        </div>

        <hr>
        <h2 id="" class="title">Cards</h2>

        <div class="columns">
            <div class="column is-6">
                <div class="card events-card">
                    <header class="card-header">
                        <p class="card-header-title">
                            Events
                        </p>
                        <a href="#" class="card-header-icon" aria-label="more options">
                  <span class="icon">
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                  </span>
                        </a>
                    </header>
                    <div class="card-table">
                        <div class="content">
                            <table class="table is-fullwidth is-striped">
                                <tbody>
                                <tr>
                                    <td width="5%"><i class="fa fa-bell-o"></i></td>
                                    <td>Lorum ipsum dolem aire</td>
                                    <td><a class="button is-small is-primary" href="#">Action</a></td>
                                </tr>
                                <tr>
                                    <td width="5%"><i class="fa fa-bell-o"></i></td>
                                    <td>Lorum ipsum dolem aire</td>
                                    <td><a class="button is-small is-primary" href="#">Action</a></td>
                                </tr>
                                <tr>
                                    <td width="5%"><i class="fa fa-bell-o"></i></td>
                                    <td>Lorum ipsum dolem aire</td>
                                    <td><a class="button is-small is-primary" href="#">Action</a></td>
                                </tr>
                                <tr>
                                    <td width="5%"><i class="fa fa-bell-o"></i></td>
                                    <td>Lorum ipsum dolem aire</td>
                                    <td><a class="button is-small is-primary" href="#">Action</a></td>
                                </tr>
                                <tr>
                                    <td width="5%"><i class="fa fa-bell-o"></i></td>
                                    <td>Lorum ipsum dolem aire</td>
                                    <td><a class="button is-small is-primary" href="#">Action</a></td>
                                </tr>
                                <tr>
                                    <td width="5%"><i class="fa fa-bell-o"></i></td>
                                    <td>Lorum ipsum dolem aire</td>
                                    <td><a class="button is-small is-primary" href="#">Action</a></td>
                                </tr>
                                <tr>
                                    <td width="5%"><i class="fa fa-bell-o"></i></td>
                                    <td>Lorum ipsum dolem aire</td>
                                    <td><a class="button is-small is-primary" href="#">Action</a></td>
                                </tr>
                                <tr>
                                    <td width="5%"><i class="fa fa-bell-o"></i></td>
                                    <td>Lorum ipsum dolem aire</td>
                                    <td><a class="button is-small is-primary" href="#">Action</a></td>
                                </tr>
                                <tr>
                                    <td width="5%"><i class="fa fa-bell-o"></i></td>
                                    <td>Lorum ipsum dolem aire</td>
                                    <td><a class="button is-small is-primary" href="#">Action</a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <footer class="card-footer">
                        <a href="#" class="card-footer-item">View All</a>
                    </footer>
                </div>

                <div class="card">
                    <header class="card-header">
                        <p class="card-header-title">
                            <span>Login</span>
                            <span class="is-pulled-right">
                    <a href="https://github.com/jgthms/bulma/releases/tag/0.7.2">
                      <span class="tag is-default">v0.7.2</span>
                    </a>
                    <span class="tag is-default">Desktop</span>
                    <span class="tag is-default">Mobile</span>
                  </span>
                        </p>
                    </header>
                    <div class="card-content">
                        <figure class="image">
                            <img src="../images/login.png" alt="Login template screenshot">
                        </figure>
                    </div>
                    <footer class="card-footer">
                        <a href="templates/login.html" class="card-footer-item">Preview</a>
                        <a href="https://github.com/dansup/bulma-templates/blob/master/templates/login.html" class="card-footer-item">Source
                            Code</a>
                    </footer>
                </div>

            </div>
            <div class="column is-6">
                <div class="card">
                    <header class="card-header">
                        <p class="card-header-title">
                            Inventory Search
                        </p>
                        <a href="#" class="card-header-icon" aria-label="more options">
                  <span class="icon">
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                  </span>
                        </a>
                    </header>
                    <div class="card-content">
                        <div class="content">
                            <div class="control has-icons-left has-icons-right">
                                <input class="input is-large" type="text" placeholder="">
                                <span class="icon is-medium is-left">
                      <i class="fa fa-search"></i>
                    </span>
                                <span class="icon is-medium is-right">
                      <i class="fa fa-check"></i>
                    </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <header class="card-header">
                        <p class="card-header-title">
                            User Search
                        </p>
                        <a href="#" class="card-header-icon" aria-label="more options">
                  <span class="icon">
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                  </span>
                        </a>
                    </header>
                    <div class="card-content">
                        <div class="content">
                            <div class="control has-icons-left has-icons-right">
                                <input class="input is-large" type="text" placeholder="">
                                <span class="icon is-medium is-left">
                      <i class="fa fa-search"></i>
                    </span>
                                <span class="icon is-medium is-right">
                      <i class="fa fa-check"></i>
                    </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <hr>
        <h2 id="tabs" class="title">Tabs</h2>

        <div class="tabs is-centered">
            <ul>
                <li class="is-active"><a>Pictures</a></li>
                <li><a>Music</a></li>
                <li><a>Videos</a></li>
                <li><a>Documents</a></li>
            </ul>
        </div>

        <div class="tabs is-large">
            <ul>
                <li class="is-active"><a>Pictures</a></li>
                <li><a>Music</a></li>
                <li><a>Videos</a></li>
                <li><a>Documents</a></li>
            </ul>
        </div>

        <div class="tabs is-centered is-boxed is-medium">
            <ul>
                <li class="is-active">
                    <a>
                        <span class="icon is-small"><i class="fas fa-image" aria-hidden="true"></i></span>
                        <span>Pictures</span>
                    </a>
                </li>
                <li>
                    <a>
                        <span class="icon is-small"><i class="fas fa-music" aria-hidden="true"></i></span>
                        <span>Music</span>
                    </a>
                </li>
                <li>
                    <a>
                        <span class="icon is-small"><i class="fas fa-film" aria-hidden="true"></i></span>
                        <span>Videos</span>
                    </a>
                </li>
                <li>
                    <a>
                        <span class="icon is-small"><i class="far fa-file-alt" aria-hidden="true"></i></span>
                        <span>Documents</span>
                    </a>
                </li>
            </ul>
        </div>


        <hr>
        <h2 id="tabs" class="title">Box</h2>

        <div class="box">
            <h4 id="const" class="title is-3">const</h4>
            <article class="message is-primary">
                  <span class="icon has-text-primary">
                  <i class="fab fa-js"></i>
                  </span>
                <div class="message-body">
                    Block-scoped. Cannot be re-assigned. Not immutable.
                </div>
            </article>
            <pre class=" language-javascript"><code class=" language-javascript"><span class="token keyword">const</span> test <span class="token operator">=</span> <span class="token string">'test'</span><span class="token punctuation">;</span></code></pre>
        </div>


        <hr>
        <h2 id="tabs" class="title">Columns multiline</h2>

        <section class="section">
            <div class="container">
                <div class="columns is-multiline">
                    <div class="column is-one-third">
                        <article class="notification media has-background-white">
                            <figure class="media-left">
            <span class="icon">
              <i class="has-text-warning fas fa-columns fa-lg"></i>
            </span>
                            </figure>
                            <div class="media-content">
                                <div class="content">
                                    <h1 class="title is-size-4">Columns</h1>
                                    <p class="is-size-5 subtitle">
                                        The power of <strong>Flexbox</strong> in a simple interface
                                    </p>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="column is-one-third">
                        <article class="notification has-background-white media">
                            <figure class="media-left">
            <span class="icon has-text-info">
              <i class="fab fa-lg fa-wpforms"></i>
            </span>
                            </figure>
                            <div class="media-content">
                                <div class="content">
                                    <h1 class="title is-size-4">Form</h1>
                                    <p class="is-size-5 subtitle">
                                        The indispensable <strong>form controls</strong>, designed for maximum clarity
                                    </p>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="column is-one-third">
                        <article class="notification has-background-white media">
                            <figure class="media-left">
            <span class="icon has-text-danger">
              <i class="fas fa-lg fa-cubes"></i>
            </span>
                            </figure>
                            <div class="media-content">
                                <div class="content">
                                    <h1 class="title is-size-4">Components</h1>
                                    <p class="is-size-5 subtitle">
                                        Advanced multi-part components with lots of possibilities
                                    </p>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="column is-one-third">
                        <article class="notification has-background-white media">
                            <figure class="media-left">
            <span class="icon has-text-grey">
              <i class="fas fa-lg fa-cogs"></i>
            </span>
                            </figure>
                            <div class="media-content">
                                <div class="content">
                                    <h1 class="title is-size-4">Modifiers</h1>
                                    <p class="is-size-5 subtitle">
                                        An <strong>easy-to-read</strong> naming system designed for humans
                                    </p>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="column is-one-third">
                        <article class="notification has-background-white media">
                            <figure class="media-left">
            <span class="icon has-text-primary">
              <i class="fas fa-lg fa-warehouse"></i>
            </span>
                            </figure>
                            <div class="media-content">
                                <div class="content">
                                    <h1 class="title is-size-4">Layout</h1>
                                    <p class="is-size-5 subtitle">
                                        Design the <strong>structure</strong> of your webpage with these CSS classes
                                    </p>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="column is-one-third">
                        <article class="notification has-background-white media">
                            <figure class="media-left">
            <span class="icon has-text-danger">
              <i class="fas fa-lg fa-cube"></i>
            </span>
                            </figure>
                            <div class="media-content">
                                <div class="content">
                                    <h1 class="title is-size-4">Elements</h1>
                                    <p class="is-size-5 subtitle">
                                        Essential interface elements that only require a <strong>single CSS class</strong>
                                    </p>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>


        <hr>
        <h2 id="tabs" class="title">Forum</h2>



        <div class="column is-9">

            <a class="button is-primary is-block is-alt is-large" href="#">New Post</a>

            <div class="box content">
                <article class="post">
                    <h4>Bulma: How do you center a button in a box?</h4>
                    <div class="media">
                        <div class="media-left">
                            <p class="image is-32x32">
                                <img src="http://bulma.io/images/placeholders/128x128.png">
                            </p>
                        </div>
                        <div class="media-content">
                            <div class="content">
                                <p>
                                    <a href="#">@jsmith</a> replied 34 minutes ago &nbsp;
                                    <span class="tag">Question</span>
                                </p>
                            </div>
                        </div>
                        <div class="media-right">
                            <span class="has-text-grey-light"><i class="fa fa-comments"></i> 1</span>
                        </div>
                    </div>
                </article>
                <article class="post">
                    <h4>How can I make a bulma button go full width?</h4>
                    <div class="media">
                        <div class="media-left">
                            <p class="image is-32x32">
                                <img src="http://bulma.io/images/placeholders/128x128.png">
                            </p>
                        </div>
                        <div class="media-content">
                            <div class="content">
                                <p>
                                    <a href="#">@red</a> replied 40 minutes ago &nbsp;
                                    <span class="tag">Question</span>
                                </p>
                            </div>
                        </div>
                        <div class="media-right">
                            <span class="has-text-grey-light"><i class="fa fa-comments"></i> 0</span>
                        </div>
                    </div>
                </article>
                <article class="post">
                    <h4>TypeError: Data must be a string or a buffer when trying touse vue-bulma-tabs</h4>
                    <div class="media">
                        <div class="media-left">
                            <p class="image is-32x32">
                                <img src="http://bulma.io/images/placeholders/128x128.png">
                            </p>
                        </div>
                        <div class="media-content">
                            <div class="content">
                                <p>
                                    <a href="#">@jsmith</a> replied 53 minutes ago &nbsp;
                                    <span class="tag">Question</span>
                                </p>
                            </div>
                        </div>
                        <div class="media-right">
                            <span class="has-text-grey-light"><i class="fa fa-comments"></i> 13</span>
                        </div>
                    </div>
                </article>
                <article class="post">
                    <h4>How to vertically center elements in Bulma?</h4>
                    <div class="media">
                        <div class="media-left">
                            <p class="image is-32x32">
                                <img src="http://bulma.io/images/placeholders/128x128.png">
                            </p>
                        </div>
                        <div class="media-content">
                            <div class="content">
                                <p>
                                    <a href="#">@brown</a> replied 3 hours ago &nbsp;
                                    <span class="tag">Question</span>
                                </p>
                            </div>
                        </div>
                        <div class="media-right">
                            <span class="has-text-grey-light"><i class="fa fa-comments"></i> 2</span>
                        </div>
                    </div>
                </article>
                <article class="post">
                    <h4>I'm trying to use hamburger menu on bulma css, but it doesn't work. What is wrong?</h4>
                    <div class="media">
                        <div class="media-left">
                            <p class="image is-32x32">
                                <img src="http://bulma.io/images/placeholders/128x128.png">
                            </p>
                        </div>
                        <div class="media-content">
                            <div class="content">
                                <p>
                                    <a href="#">@hamburgler</a> replied 5 hours ago &nbsp;
                                    <span class="tag">Question</span>
                                </p>
                            </div>
                        </div>
                        <div class="media-right">
                            <span class="has-text-grey-light"><i class="fa fa-comments"></i> 2</span>
                        </div>
                    </div>
                </article>
                <article class="post">
                    <h4>How to make tiles wrap with Bulma CSS?</h4>
                    <div class="media">
                        <div class="media-left">
                            <p class="image is-32x32">
                                <img src="http://bulma.io/images/placeholders/128x128.png">
                            </p>
                        </div>
                        <div class="media-content">
                            <div class="content">
                                <p>
                                    <a href="#">@rapper</a> replied 3 hours ago &nbsp;
                                    <span class="tag">Question</span>
                                </p>
                            </div>
                        </div>
                        <div class="media-right">
                            <span class="has-text-grey-light"><i class="fa fa-comments"></i> 2</span>
                        </div>
                    </div>
                </article>
            </div>
        </div>




        <hr>
        <h2 id="tabs" class="title">Some block with cards</h2>

        <div class="columns">
            <div class="column">
                <article class="media">
                    <div class="media-left">
                        <i class="fab fa-github-square fa-4x"></i>
                    </div>
                    <div class="media-content">
                        <div class="content">
                            <p>
                                <strong>Dominic Ipsum</strong>
                                <br> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur sit amet massa fringilla egestas. Nullam condimentum luctus turpis.
                            </p>
                        </div>
                    </div>
                </article>
                <article class="media">
                    <div class="media-left">
                        <i class="fab fa-empire fa-4x"></i>
                    </div>
                    <div class="media-content">
                        <div class="content">
                            <p>
                                <strong>Cassie Ipsum</strong>
                                <br> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur sit amet massa fringilla egestas. Nullam condimentum luctus turpis.
                            </p>
                        </div>
                    </div>
                </article>
            </div>
            <div class="column">
                <article class="media">
                    <div class="media-left">
                        <i class="fab fa-ravelry fa-4x"></i>
                    </div>
                    <div class="media-content">
                        <div class="content">
                            <p>
                                <strong>Avery Ipsum</strong>
                                <br> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur sit amet massa fringilla egestas. Nullam condimentum luctus turpis.
                            </p>
                        </div>
                    </div>
                </article>
                <article class="media">
                    <div class="media-left">
                        <i class="fab fa-github-alt fa-4x"></i>
                    </div>
                    <div class="media-content">
                        <div class="content">
                            <p>
                                <strong>io Ipsum</strong>
                                <br> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean efficitur sit amet massa fringilla egestas. Nullam condimentum luctus turpis. ╳
                            </p>
                        </div>
                    </div>
                </article>
            </div>
        </div>

        <hr>
        <h2 id="tabs" class="title">Kanban table</h2>

        <section class="container">
            <div class="level-item">
                <div class="columns is-multiline is-centered cards-container" id="sectioncontainer">
                    <div class="column is-narrow">
                        <article class="message is-black">
                            <div class="message-header">
                                <p>Season 1</p>
                                <button class="delete" aria-label="delete"></button>
                            </div>
                            <div class="message-body">
                                <div class="board-item">
                                    <div class="board-item-content"><span>The Fort</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Fist Like a bullet</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>White Stork Spreads Wings</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Two Tigers Subdue Dragons</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Snake Creeps Down</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Hand of Five Poisons</span></div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="column is-narrow">
                        <article class="message is-primary">
                            <div class="message-header">
                                <p>Season 2</p>
                                <button class="delete" aria-label="delete"></button>
                            </div>
                            <div class="message-body">
                                <div class="board-item">
                                    <div class="board-item-content"><span>Tiger Pushes Mountain</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Force of Eagle's Claw</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Red Sun, Silver Moon</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Palm of the Iron Fox</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Monkey Leaps Through Mist</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Leopard Stalks in Snow</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Black Heart, White Mountain</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Sting of the Scorpion's Tail</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Nightingale Sings No More</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Wolf's Breath, Dragon Fire</span></div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="column is-narrow">
                        <article class="message is-link">
                            <div class="message-header">
                                <p>Season 3</p>
                                <button class="delete" aria-label="delete"></button>
                            </div>
                            <div class="message-body">
                                <div class="board-item">
                                    <div class="board-item-content"><span>Enter the Phoenix</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Moon Rises, Raven Seeks</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Leopard Snares Rabbit</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Blind Cannibal Assassins</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Carry Tiger to Mountain</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Black Wind Howls</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Dragonfly's Last Dance</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Leopard Catches Cloud</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Chamber of the Scorpion</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Raven's Feather, Phoenix Blood</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>The Boar And The Butterfly</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Cobra Fang, Panther Claw</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Black Lothus, White Rose</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Curse of the Red Rain</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Requiem for the Fallen</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Seven Strike as One</span></div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="column is-narrow">
                        <article class="message is-info">
                            <div class="message-header">
                                <p>Info</p>
                                <button class="delete" aria-label="delete"></button>
                            </div>
                            <div class="message-body">
                                <div class="board-item">
                                    <div class="board-item-content"><span>Bronchy</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Aorta</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Alveolae</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>TALISMAN</span></div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="column is-narrow">
                        <article class="message is-success">
                            <div class="message-header">
                                <p>Success</p>
                                <button class="delete" aria-label="delete"></button>
                            </div>
                            <div class="message-body">
                                <div class="board-item">
                                    <div class="board-item-content"><span>signature</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>weasel</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>solana</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>hydro</span></div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="column is-narrow">
                        <article class="message is-warning">
                            <div class="message-header">
                                <p>Warning</p>
                                <button class="delete" aria-label="delete"></button>
                            </div>
                            <div class="message-body">
                                <div class="board-item">
                                    <div class="board-item-content"><span>Ganimede</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Europa</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Tycho</span></div>
                                </div>
                                <div class="board-item">
                                    <div class="board-item-content"><span>Io</span></div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>


        {{--        end template--}}
    </div>


@endsection('content')
