
<nav class="navbar is-white bottomNav">
    <div class="container">
        <div class="navbar-menu bottomNav">

            <div class="navbar-start is-expanded">

                @foreach($submenu as $subitem)
                    <a class="navbar-item {{ ($subitem[2] === $current_submenu ? 'is-active' : '') }}" href="{{$subitem[1]}}">
                        {{$subitem[0]}}
                    </a>
                @endforeach

            </div>

        </div>
    </div>
</nav>
