<aside class="menu is-hidden-mobile">
    <p class="menu-label">
        General
    </p>

    <ul class="menu-list">
        <li><a href="{{route('app.settings')}}" class="{{isActive($page, 'account-settings')}}">Account settings</a></li>
        <li><a href="{{route('app.settings.change-password')}}" class="{{isActive($page, 'change-password')}}">Change password</a></li>
    </ul>

    <p class="menu-label">
        Transactions
    </p>
    <ul class="menu-list">
        <li><a>Payments</a></li>
        <li><a>Transfers</a></li>
        <li><a>Balance</a></li>
    </ul>
</aside>
