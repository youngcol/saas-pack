@extends("app.layout")


@section('content')

    <div class="container settings">

        <div class="columns">
            <div class="column is-3 ">
                @include('app/account-settings/menu')
            </div>


            <div class="column is-5 p-left-50">

                <account-settings-general
                    :show={{($page == 'account-settings' ? 'true' : 'false')}}
                ></account-settings-general>


                <account-settings-password
                        :show={{($page == 'change-password' ? 'true' : 'false')}}
                >
                </account-settings-password>

            </div>
        </div>
    </div>


    {{--    --}}
    {{--    <section class="site-section py-sm pt-5">--}}
    {{--        <div class="container">--}}
    {{--            <div class="row">--}}
    {{--                <div class="col-md-6">--}}
    {{--                    <h2 class="mb-4">Settings</h2>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <div class="row blog-entries">--}}
    {{--                <div class="col-md-12 col-lg-8 main-content">--}}
    {{--                    <div class="row">--}}

    {{--                        <div class="col-md-12">--}}
    {{--                            <p>--}}
    {{--                                Name: {{$current_user['name']}}--}}
    {{--                            </p>--}}

    {{--                            <p>--}}
    {{--                                Registered email: {{$current_user['email']}}--}}
    {{--                            </p>--}}

    {{--                            <p>--}}
    {{--                                Registered date:--}}
    {{--                            </p>--}}
    {{--                        </div>--}}


    {{--                        --}}{{--<div class="col-md-6">--}}
    {{--                            --}}{{--<a href="blog-single.html" class="blog-entry element-animate" >--}}
    {{--                                --}}{{--<img src="/app_/assets/images/img_5.jpg" alt="Image placeholder">--}}
    {{--                                --}}{{--<div class="blog-content-body">--}}
    {{--                                    --}}{{--<div class="post-meta">--}}
    {{--                                        --}}{{--<span class="author mr-2"><img src="/app_/assets/images/person_1.jpg" alt="Colorlib"> Colorlib</span>&bullet;--}}
    {{--                                        --}}{{--<span class="mr-2">March 15, 2018 </span> &bullet;--}}
    {{--                                        --}}{{--<span class="ml-2"><span class="fa fa-comments"></span> 3</span>--}}
    {{--                                    --}}{{--</div>--}}
    {{--                                    --}}{{--<h2>How to Find the Video Games of Your Youth</h2>--}}
    {{--                                --}}{{--</div>--}}
    {{--                            --}}{{--</a>--}}
    {{--                        --}}{{--</div>--}}

    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </section>--}}

@endsection
