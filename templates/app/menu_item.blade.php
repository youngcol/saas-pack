
@php($hasMenu = count($submenu) ? true : false)
@php($isActive = $page_name === $current_page)
@php($isSubmenuActive = false)

<?php
    $fil = array_filter($submenu, function($item) use ($current_submenu){
        return $item[2] === $current_submenu;
    });

    $isSubmenuActive = (count($fil) > 0);
?>

@if($hasMenu)

@else

@endif

<a class="navbar-item {{$isActive ? 'active': ''}}" href={{$url}}>{{$title}}</a>
