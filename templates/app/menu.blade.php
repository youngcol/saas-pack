
@if(!isset($submenu))
    @php($submenu = null)
@endif

<nav class="navbar is-white topNav">
    <div class="container">
        <div class="navbar-brand">
            <a class="navbar-item" href="{{route('app.index')}}">
                <img src="https://bulma.io/images/bulma-logo.png" alt="Logo" width="112" height="28">
            </a>

            <a @click="onClickCreatePost" class="navbar-item is-hidden-desktop">
                Create post
            </a>

            <div class="navbar-burger burger" data-target="topNav">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>

        <div id="topNav" class="navbar-menu">
            <div class="navbar-start">

                {!!menu_item(
                    'Dashboard', route('app.index'), 'dashboard',
                    [
                        ['Dashboard', route('app.index'), 'dashboard'],
                        ['Social accounts', route('app.accounts'), 'accounts']
                    ], $page, $submenu
                )!!}

                {!!menu_item(
                    'Actions', route('app.actions'), 'actions',
                    [], $page, $submenu
                )!!}

                {!!menu_item(
                    'Publish', route('app.publish.queue'), 'publish',
                    [
                        ['Queue', route('app.publish.queue'), 'queue'],
                        ['Sent', route('app.publish.sent'), 'sent'],
                        ['Failed', route('app.publish.failed'), 'failed'],
                        ['Drafts', route('app.publish.drafts'), 'drafts']
                    ], $page, $submenu
                )!!}

                {!!menu_item(
                    'Wellcome', route('app.wellcome'), 'wellcome',
                    [], $page, $submenu
                )!!}

                {!!menu_item(
                    'CRM', route('app.crm.contacts'), 'crm',
                    [
                        ['Contacts', route('app.crm.contacts'), 'contacts'],
                        ['Contact lists', route('app.crm.contacts.lists'), 'contacts.lists'],
                        ['Pitches', route('app.crm.pitches'), 'pitches'],
                        ['Promotions', route('app.crm.promotions'), 'promotions'],
                    ], $page, $submenu
                )!!}

            </div>

            <div class="navbar-end">

                <a @click="isCreatePostModalActive = true" class="navbar-item">
                    Create post
                </a>

                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link">
                        <span class="icon">
                            <i class="fab fa-twitter"></i>
                        </span>

                        <span class="m-left-10">
                            Account
                        </span>
                    </a>

                    <div class="navbar-dropdown">

                        <a href="{{route('app.settings')}}" class="navbar-item">
                            Settings
                        </a>

                        <hr class="navbar-divider">

                        <a href="{{route('logout')}}" class="navbar-item">
                            Logout
                        </a>

                        @if(settings('env') == 'dev')

                            <hr class="navbar-divider">
                            <hr class="navbar-divider">

                            <a href="{{route('app.templates')}}" class="navbar-item">
                                Templates
                            </a>

                            <a href="{{route('app.whole-ui-kit')}}" class="navbar-item">
                                Whole ui kit
                            </a>

                            <a href="{{route('app.vue.stories')}}" class="navbar-item">
                                Vue stories
                            </a>

                        @endif
                    </div>
                </div>

                <a class="navbar-item">
                    Help
                </a>
            </div>
        </div>
    </div>
</nav>

{!! render_submenu($page, $submenu) !!}
