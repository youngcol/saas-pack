
@extends("app.layout")


@section('content')

    <div class="container wellcome-page">

        <section class="section-start">

            <h2 class="title has-text-centered">It's easy to setup your booking system</h2>
            <hr>

            <div class="columns m-top-50">
                <div class="column is-9">
                    <div class="columns m-top-0  is-multiline">
                        <div class="column is-one-third">
                            <article class="media p-10">
                                <div class="media-content has-text-centered">
                                    <div class="has-text-centered">
                                        <span class="icon">
                                            <i class="has-text-warning fas fa-columns fa-3x"></i>
                                        </span>
                                    </div>

                                    <div class="content m-top-20">
                                        <h3 class="subtitle">1) Customize design</h3>
                                        <p class="is-size-5 subtitle has-text-centered">
                                            <a href="#">Watch now</a>
                                        </p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="column is-one-third">

                            <article class="media p-10">
                                <div class="media-content has-text-centered">
                                    <div class="has-text-centered">
                                        <span class="icon ">
                                            <i class="has-text-warning fab fa-wpforms fa-3x"></i>
                                        </span>
                                    </div>

                                    <div class="content m-top-20">
                                        <h3 class="subtitle">2) Add service providers</h3>
                                        <p class="is-size-5 subtitle has-text-centered">
                                            <a href="#">Watch now</a>
                                        </p>
                                    </div>
                                </div>
                            </article>

                        </div>

                        <div class="column is-one-third">
                            <article class="media p-10">
                                <div class="media-content has-text-centered">
                                    <div class="has-text-centered">
                                        <span class="icon ">
                                            <i class="has-text-danger fas fa-cubes fa-3x"></i>
                                        </span>
                                    </div>

                                    <div class="content m-top-20">
                                        <h3 class="subtitle">3) Set schedules</h3>
                                        <p class="is-size-5 subtitle has-text-centered">
                                            <a href="#">Watch now</a>
                                        </p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="column is-one-third">
                            <article class="notification has-background-white media">
                                <figure class="media-left">
            <span class="icon has-text-grey">
              <i class="fas fa-lg fa-cogs"></i>
            </span>
                                </figure>
                                <div class="media-content">
                                    <div class="content">
                                        <h1 class="title is-size-4">Modifiers</h1>
                                        <p class="is-size-5 subtitle">
                                            An <strong>easy-to-read</strong> naming system designed for humans
                                        </p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="column is-one-third">
                            <article class="notification has-background-white media">
                                <figure class="media-left">
            <span class="icon has-text-primary">
              <i class="fas fa-lg fa-warehouse"></i>
            </span>
                                </figure>
                                <div class="media-content">
                                    <div class="content">
                                        <h1 class="title is-size-4">Layout</h1>
                                        <p class="is-size-5 subtitle">
                                            Design the <strong>structure</strong> of your webpage with these CSS classes
                                        </p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="column is-one-third">
                            <article class="notification has-background-white media">
                                <figure class="media-left">
            <span class="icon has-text-danger">
              <i class="fas fa-lg fa-cube"></i>
            </span>
                                </figure>
                                <div class="media-content">
                                    <div class="content">
                                        <h1 class="title is-size-4">Elements</h1>
                                        <p class="is-size-5 subtitle">
                                            Essential interface elements that only require a <strong>single CSS class</strong>
                                        </p>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>

                <div class="column is-3">

                    <h5 class="subtitle has-text-centered">My account</h5>



                </div>
            </div>


        </section>
    </div>

@endsection('content')
