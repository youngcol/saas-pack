@extends("admin.layout")

@section('content')

    @include('admin.breadcrumbs', [
        'title' => 'Tasks viewer',
        'links' => [
            ['route' => 'admin.dashboard', 'title' => 'Dashboard'],
        ]
    ])

    <div class="row">

        @foreach($tasks as $name => $files)
            <div class="col-6">
                <div class="card">
                    <article class="card-group-item">
                        <header class="card-header">
                            <b>
                                <h2 class="title">
                                    <b>
                                        {{$name}}
                                    </b>

                                </h2>
                            </b>

                        </header>
                        <div class="filter-content">
                            <div class="card-body">
                                @foreach($files as $file)
                                    <h3>{{$file}}</h3>
                                @endforeach
                            </div> <!-- card-body.// -->
                        </div>
                    </article> <!-- card-group-item.// -->
                </div>
            </div>
        @endforeach


    </div>

@endsection
