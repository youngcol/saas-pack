
@extends("admin.layout")

@section('content')

    @include('admin.breadcrumbs', [
        'title' => 'Mysql table viewer',
        'links' => [
            ['route' => 'admin.dashboard', 'title' => 'Dashboard'],
            ['route' => 'admin.mysql_viewer', 'title' => 'Mysql viewer']
        ]
    ])

    <div class="section">
{{--        <div class="col-12">--}}
        <div class="row">
            @foreach($tableTiers['up'] as $table)

                <?php
                    $thisTableFields = sp($tableTiers['bold_fields'], $table['name']);
                ?>


                <div class="col-4 ml-2 #table-{{$table['name']}}">
                    <h3>
                        {{$table['name']}}
                        <a class="ml-2" href="{{route('admin.mysql_viewer.table', ['table' => $table['name']])}}#current_table">view</a>
                    </h3>
                    <table class="mt-2 table table-striped">
                        @foreach($table['fields'] as $field)
                            <tr>
                                @if(in_array($field->Field, $thisTableFields))
                                    <td><b>{{$field->Field}}</b></td>
                                    <td><b>{{$field->Type}}</b></td>
                                @else
                                    <td>{{$field->Field}}</td>
                                    <td>{{$field->Type}}</td>
                                @endif
                            </tr>
                        @endforeach
                    </table>
                </div>

            @endforeach
        </div>
        <hr>

        <?php
            $thisTableFields = sp($tableTiers['bold_fields'], $tableTiers['table']['name']);
        ?>

        <hr>
        <a href="#current_table">
            <h2 id="current_table">
                <b>{{$tableTiers['table']['name']}}</b>
            </h2>

            <table class="mt-2 table table-striped">
                @foreach($tableTiers['table']['fields'] as $field)
                    <tr>
                        @if(in_array($field->Field, $thisTableFields))
                            <td><h3><b>{{$field->Field}}</b></h3></td>
                            <td><h3><b>{{$field->Type}}</b></h3></td>
                        @else
                            <td><h4>{{$field->Field}}</h4></td>
                            <td><h4>{{$field->Type}}</h4></td>
                        @endif
                    </tr>
                @endforeach
            </table>
        </a>
        <hr>

        <div class="row">
        @foreach($tableTiers['down'] as $table)

            <?php
                $thisTableFields = sp($tableTiers['bold_fields'], $table['name']);
            ?>

            <div class="col-4 ml-2 #table-{{$table['name']}}">
                <h3>
                    {{$table['name']}}
                    <a class="ml-2" href="{{route('admin.mysql_viewer.table', ['table' => $table['name']])}}#current_table">view</a>
                </h3>
                <table class="mt-2 table table-striped">
                    @foreach($table['fields'] as $field)
                        <tr>
                            @if(in_array($field->Field, $thisTableFields))
                                <td><b>{{$field->Field}}</b></td>
                                <td><b>{{$field->Type}}</b></td>
                            @else
                                <td>{{$field->Field}}</td>
                                <td>{{$field->Type}}</td>
                            @endif
                        </tr>
                    @endforeach
                </table>
            </div>

        @endforeach
        </div>
        <hr>
{{--        </div>--}}
    </div>

@endsection

@section('scripts')
    <script>

        jQuery(document).ready(function($) {

        });

    </script>
@endsection
