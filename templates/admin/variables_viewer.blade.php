@extends("admin.layout")

@section('content')

    @include('admin.breadcrumbs', [
        'title' => 'Variables viewer',
        'links' => [
            ['route' => 'admin.dashboard', 'title' => 'Dashboard'],
        ]
    ])

    <div class="row">
        @foreach($dumps as $var)
            <div class="col-9 mb-3">
                <h2 class="mb-3">{{$var['name']}}</h2>
                @php(dump($var['var']))
            </div>
            <hr>
            <hr>
        @endforeach
    </div>
@endsection;
