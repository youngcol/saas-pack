@extends("admin.layout")

@section('content')

    @include('admin.breadcrumbs', [
        'title' => 'Tasks invoke log',
        'links' => [
            ['route' => 'admin.dashboard', 'title' => 'Dashboard'],
        ]
    ])

    <div class="row">

        <div class="col-12">

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Created at</th>
                        <th>Class name</th>
                        <th>Duration</th>
                        <th>Status</th>
                    </tr>
                </thead>
                @foreach ($logs as $log)
                    <tr>
                        <td>{{$log['id']}}</td>
                        <td>{{$log['created_at']}}</td>
                        <td>{{$log['class_name']}}</td>
{{--                        <td>{!! dump(unserialize($log['args'])) !!}</td>--}}
                        <td>{{$log['duration']}}</td>
                        <td>{{$log['status']}}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            @php(dump(unserialize($log['args'])))
                            @php(dump($log['backtrace']))
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                @endforeach
            </table>


            @foreach($logs as $log)
                {{--<div class="mb-4">--}}
                    {{--<h2>{{$diff['message']}}</h2>--}}
                    {{--                    <p>{{$diff['locale1']}} -> {{$diff['locale2']}}</p>--}}

                    {{--@foreach($diff['keys'] as $key)--}}
                        {{--<h4 class="ml-5">{{$key}}</h4>--}}
                    {{--@endforeach--}}
                {{--</div>--}}
            @endforeach
        </div>


    </div>

@endsection
