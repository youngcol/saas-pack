
@extends("admin.layout")

@section('content')

    <div class="">
        <h3>Routes</h3>
        <table class="table table-striped">
            @foreach ($routes as $route)
                <tr>
                    <td>{{implode(',', $route->getMethods())}}</td>
                    <td>{{$route->getPattern()}}</td>
                    <td>{{$route->getName()}}</td>
                </tr>
            @endforeach
        </table>
    </div>


    <style>
        .content {
            font-family: monospace, fixed;
            font-family: 'Inconsolata', monospace;
            font-size: 18px;
            line-height: 18px;
        }
        .command-form .console {
            margin: 10px;
        }
    </style>

@endsection

@section('scripts')
    <script>

        jQuery(document).ready(function($) {



        });

    </script>
@endsection
