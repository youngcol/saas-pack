@extends("admin.layout")

@section('content')

    @include('admin.breadcrumbs', [
        'title' => 'Php stories viewer',
        'links' => [
            ['route' => 'admin.dashboard', 'title' => 'Dashboard'],
        ]
    ])

    <div class="row">
        <table class="table table-striped">
            <thead>
                <tr>
                    <td>ID</td>
                    <td>Story class</td>
                    <td>Version</td>
                    <td>Finished step</td>
                    <td>Status</td>
                    <td>Updated at</td>
                </tr>
            </thead>
            @foreach ($stories as $story)
                <tr>
                    <td>{{$story->id}}</td>
                    <td>{{$story->class_name}}</td>
                    <td>{{$story->version}}</td>
                    <td>{{$story->finished_step}}</td>
                    <td>{{$story->status}}</td>
                    <td>{{$story->updated_at}}</td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection;
