@extends("admin.layout")

@section('content')

    @include('admin.breadcrumbs', [
        'title' => 'Languages',
        'links' => [
            ['route' => 'admin.dashboard', 'title' => 'Dashboard'],
        ]
    ])

    <div class="row">

        @if(count($diffs))
            <div class="col-8">
                <h1 class="alert alert-danger">Founded missed keys</h1>

                @foreach($diffs as $diff)
                    <div class="mb-4">
                        <h2>{{$diff['message']}}</h2>
                        {{--                    <p>{{$diff['locale1']}} -> {{$diff['locale2']}}</p>--}}

                        @foreach($diff['keys'] as $key)
                            <h4 class="ml-5">{{$key}}</h4>
                        @endforeach
                    </div>
                @endforeach
            </div>
        @else
            <h1 class="alert alert-success">No missed keys in `lang` folder</h1>
        @endif

    </div>

@endsection
