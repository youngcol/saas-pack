
@extends("public.layout")

@section('content')

    <section id="signup" class="section-padding1">

        <div class="container">
            <div class="row">

                <div class="mx-auto col-6 text-center">
                    <h2>Email verify</h2>

                    @if($isVerified)
                        <p class="lead">Email successfuly verified!</p>
                    @else
                        <p class="lead"> >> Error. Can't find such code for verification! << </p>
                    @endif

                </div>
            </div>
        </div>

    </section>

@endsection
