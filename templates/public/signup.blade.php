
@extends("public.layout")

@section('content')

    <section id="signup" class="section-padding1">

        <div class="container">
            <div class="row">

                <div class="col-md-6 col-sm-12 text-center">
                    <div class="about-content">
                        <h2>Sign up</h2>

                        <div class="mt-5">
                            <form method="post" action="{{ route('signup.store') }}">
                                {{ csrf($csrf) }}
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <input type="text" name="username" class="form-control" required placeholder="Name">
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <input type="email" name="email" class="form-control" required placeholder="Email">
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <input type="password" name="password" class="form-control" required placeholder="Password">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-block btn-primary">Signup</button>
                                </div>
                            </form>
                        </div>


                        <div class="form-group mt-5">
                            <p>
                                Already have an account? <a href="{{route('signin')}}"> Sign In</a>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-12 text-center">
                    &nbsp;
                </div>
            </div>
        </div>

    </section><!-- #signup -->

@endsection
