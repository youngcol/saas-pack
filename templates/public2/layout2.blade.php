<!doctype html>
<html lang="en-US">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    {{--modules.033c408dbfcdfb3e35ff.js--}}
    {{--autoptimize_single_8b43dd001fa78d34079efafb4b4a78b8.css--}}

    <link rel="stylesheet" id="contact-form-7-css" href="/public/assets2/autoptimize_single_5ad1cfa3f5175f627385651790ed0bbd.css" type="text/css" media="all">
    <link rel="stylesheet" id="contact-form-7-css" href="/public/assets2/autoptimize_single_8b43dd001fa78d34079efafb4b4a78b8.css" type="text/css" media="all">
    <link rel="stylesheet" id="contact-form-7-css" href="/public/assets2/autoptimize_single_2b5d3d364fc2acef5dcf947027c127b7.css" type="text/css" media="all">
    <link rel="stylesheet" id="contact-form-7-css" href="/public/assets2/autoptimize_single_1835f5508378bea238b78a76470517dc.css" type="text/css" media="all">

    <script type='text/javascript' src='/public/assets2/jquery.js?ver=1.12.4-wp'></script>
    <script type='text/javascript' src='/public/assets2/jquery-migrate.min.js?ver=1.4.1'></script>

    <style type="text/css">
        @font-face {
            font-weight: 400;
            font-style:  normal;
            font-family: 'Inter-Loom';

            src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-Regular.woff2') format('woff2');
        }
        @font-face {
            font-weight: 400;
            font-style:  italic;
            font-family: 'Inter-Loom';

            src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-Italic.woff2') format('woff2');
        }

        @font-face {
            font-weight: 500;
            font-style:  normal;
            font-family: 'Inter-Loom';

            src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-Medium.woff2') format('woff2');
        }
        @font-face {
            font-weight: 500;
            font-style:  italic;
            font-family: 'Inter-Loom';

            src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-MediumItalic.woff2') format('woff2');
        }

        @font-face {
            font-weight: 700;
            font-style:  normal;
            font-family: 'Inter-Loom';

            src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-Bold.woff2') format('woff2');
        }
        @font-face {
            font-weight: 700;
            font-style:  italic;
            font-family: 'Inter-Loom';

            src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-BoldItalic.woff2') format('woff2');
        }

        @font-face {
            font-weight: 900;
            font-style:  normal;
            font-family: 'Inter-Loom';

            src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-Black.woff2') format('woff2');
        }
        @font-face {
            font-weight: 900;
            font-style:  italic;
            font-family: 'Inter-Loom';

            src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-BlackItalic.woff2') format('woff2');
        }

        #nav-container .logo, #nav-container .login-navigation
        {
            background-color: white;
            height: 45px;

            /*box-shadow: 0 4px 2px -2px rgba(123,124,122,.42);*/
            /*box-shadow:0 1px 4px 0 rgba(123,124,122,.42),inset 0 -1px 0 0 rgba(255,255,255,.12);*/
        }

        #nav-container .main-navigation
        {
            height: 53px;
            background-color: white;
        }

        #page-header
        {
            -webkit-box-shadow: 0px 3px 6px 0px rgba(229, 229, 229, 0.59);
            -moz-box-shadow:    0px 3px 6px 0px rgba(229, 229, 229, 0.59);
            box-shadow:         0px 3px 6px 0px rgba(229, 229, 229, 0.59);
        }


    </style>


</head>

<body>

<header id="page-header" class="hover">

    <div id="nav-container" class="flex">
        <div class="logo">
            <a href="{{route('index')}}" class="custom-logo-link" rel="home">
                <img src="https://corp-brightlocal.imgix.net/2019/04/brightlocal_logo.svg?auto=compress%2Cformat&amp;ixlib=php-1.2.1&amp;q=70&amp;s=d5e025f3cbe6b38e93a207b4b280bdee" class="custom-logo" alt="BrightLocal" height="31" width="181">
            </a>
        </div>

        <nav id="site-navigation" class="main-navigation" role="navigation" aria-label="Top Menu">
            <div class="menu-top-menu-container">
                <ul id="top-menu" class="menu">
                    <li id="menu-item-2424" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2424">
                        <a href="#" class="menu-image-title-after">
                            <span class="menu-image-title">Products</span>
                            <svg class="icon icon-angle-down" aria-hidden="true" role="img">
                                <use href="#icon-angle-down" xlink:href="#icon-angle-down"></use>
                            </svg>
                        </a>
                        <div class="sub-menu-wrap">
                            <ul class="sub-menu">
                                <li id="menu-item-36" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-36">
                                    <a href="/local-seo-tools/" class="menu-image-title-below menu-image-not-hovered"><img src="https://corp-brightlocal.imgix.net/2019/04/Local-SEO-Tools.svg?auto=compress%2Cformat&amp;fit=scale&amp;h=300&amp;ixlib=php-1.2.1&amp;q=70&amp;w=300&amp;wpsize=medium&amp;s=b1487e4f32725ee41de099e178e58548" class="menu-image menu-image-title-below" alt="" height="48" width="48"><span class="menu-image-title">Local SEO Tools</span><span class="menu-item-description">Unrivaled local SEO tools trusted by over 3,500 marketing agencies.</span></a>
                                </li>
                                <li id="menu-item-38" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-38">
                                    <a href="/reputation-manager/" class="menu-image-title-below menu-image-not-hovered"><img src="https://corp-brightlocal.imgix.net/2019/04/Reputation-Manager.svg?auto=compress%2Cformat&amp;fit=scale&amp;h=294&amp;ixlib=php-1.2.1&amp;q=70&amp;w=300&amp;wpsize=medium&amp;s=2bb4c4f57a0570e2e4e6da9d52a27fe3" class="menu-image menu-image-title-below" alt="" height="48" width="49"><span class="menu-image-title">Reputation Manager</span><span class="menu-item-description">Generate, monitor, and respond to reviews to build a 5-star reputation.</span></a>
                                </li>
                                <li id="menu-item-37" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-37">
                                    <a href="/citation-builder/" class="menu-image-title-below menu-image-not-hovered"><img src="https://corp-brightlocal.imgix.net/2019/04/Citation-Builder.svg?auto=compress%2Cformat&amp;fit=scale&amp;h=275&amp;ixlib=php-1.2.1&amp;q=70&amp;w=300&amp;wpsize=medium&amp;s=959fd87f54affc58c33444f627f002bf" class="menu-image menu-image-title-below" alt="" height="44" width="48"><span class="menu-image-title">Citation Builder</span><span class="menu-item-description">Maximize visibility with local citation building and data aggregator submissions.</span></a>
                                </li>
                                <li id="menu-item-39" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-39">
                                    <a href="/lead-generation-for-marketing-agencies/" class="menu-image-title-below menu-image-not-hovered"><img src="https://corp-brightlocal.imgix.net/2019/04/Agency-Lead-Generator.svg?auto=compress%2Cformat&amp;fit=scale&amp;h=300&amp;ixlib=php-1.2.1&amp;q=70&amp;w=300&amp;wpsize=medium&amp;s=06fc4688b00645de1743d843d7efdbda" class="menu-image menu-image-title-below" alt="" height="48" width="48"><span class="menu-image-title">Agency Lead Generator</span><span class="menu-item-description">Engage visitors. Get more leads. Grow your agency</span></a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li id="menu-item-51760" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-51760">
                        <a href="https://www.brightlocal.com/citation-builder/" class="menu-image-title-after">
                            <span class="menu-image-title">Solutions</span>
                            <svg class="icon icon-angle-down" aria-hidden="true" role="img">
                                <use href="#icon-angle-down" xlink:href="#icon-angle-down"></use>
                            </svg>
                        </a>
                        <div class="sub-menu-wrap">
                            <ul class="sub-menu">
                                <li id="menu-item-54292" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-54292">
                                    <a href="/agencies" class="menu-image-title-below menu-image-not-hovered"><img src="https://corp-brightlocal.imgix.net/2019/06/agency.svg?auto=compress%2Cformat&amp;fit=scale&amp;h=48&amp;ixlib=php-1.2.1&amp;q=70&amp;w=48&amp;wpsize=menu-48x48&amp;s=31480876e0104d795802a9fc656bdc0d" class="menu-image menu-image-title-below" alt="" height="112.5" width="112.5"><span class="menu-image-title">Agencies</span><span class="menu-item-description">Win, impress, and retain clients with our white-label agency solutions.</span></a>
                                </li>
                                <li id="menu-item-54286" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-54286">
                                    <a href="/multi-location/" class="menu-image-title-below menu-image-not-hovered"><img src="https://corp-brightlocal.imgix.net/2019/06/multi-biz-1.svg?auto=compress%2Cformat&amp;fit=scale&amp;h=48&amp;ixlib=php-1.2.1&amp;q=70&amp;w=48&amp;wpsize=menu-48x48&amp;s=0d171c0787fe2ddd1df2f07994d71998" class="menu-image menu-image-title-below" alt="" height="111.77" width="111.77"><span class="menu-image-title">Multi-location</span><span class="menu-item-description">Scalable solutions for franchises and multi-location businesses.</span></a>
                                </li>
                                <li id="menu-item-54293" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-54293">
                                    <a href="https://www.brightlocal.com/small-businesses/" class="menu-image-title-below menu-image-not-hovered"><img src="https://corp-brightlocal.imgix.net/2019/06/small-biz.svg?auto=compress%2Cformat&amp;fit=scale&amp;h=48&amp;ixlib=php-1.2.1&amp;q=70&amp;w=48&amp;wpsize=menu-48x48&amp;s=95df2c0f2117d776a5ae2fe957ed2ecc" class="menu-image menu-image-title-below" alt="" height="104.5" width="104.5"><span class="menu-image-title">Small Businesses</span><span class="menu-item-description">Increase your search visibility and be found by local customers.</span></a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li id="menu-item-32" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32">
                        <a href="https://www.brightlocal.com/pricing/" class="menu-image-title-after"><span class="menu-image-title">Pricing</span></a>
                    </li>

                    <li id="menu-item-33" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33">
                        <a href="https://www.brightlocal.com/free-online-demo/" class="menu-image-title-after">
                            <span class="menu-image-title">Book a Demo</span>
                        </a>
                    </li>

                    <li id="menu-item-807" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-807">
                        <a href="https://www.brightlocal.com/about-us/" class="menu-image-title-after">
                            <span class="menu-image-title">About Us</span>
                        </a>
                    </li>

                    <li id="menu-item-35" class="brightideas menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-35">
                        <a href="https://www.brightlocal.com/bright-ideas/" class="menu-image-title-after">
                            <span class="menu-image-title">Bright Ideas</span>
                            <svg class="icon icon-angle-down" aria-hidden="true" role="img">
                                <use href="#icon-angle-down" xlink:href="#icon-angle-down"></use>
                            </svg>
                        </a>

                        <div class="sub-menu-wrap">
                            <ul class="sub-menu">
                                <li id="menu-item-45055" class="bigcheese menu-item menu-item-type-post_type menu-item-object-page menu-item-45055">
                                    <a href="https://www.brightlocal.com/bright-ideas/" class="menu-image-title-after menu-image-not-hovered"><img src="https://corp-brightlocal.imgix.net/2019/04/brightideas_icon.svg?auto=compress%2Cformat&amp;ixlib=php-1.2.1&amp;q=70&amp;s=7079be775123a829172fb4ae0695c632" class="menu-image menu-image-title-after" alt="" height="46" width="46"><span class="menu-image-title">Bright Ideas</span><span class="menu-item-description">Everything you need to deliver amazing local marketing results. News, data, videos, and more.</span></a>
                                </li>
                                <li id="menu-item-46257" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-46257">
                                    <a href="https://www.brightlocal.com/insights/blog/" class="menu-image-title-after menu-image-not-hovered"><img src="https://corp-brightlocal.imgix.net/2019/04/blog_icon.svg?auto=compress%2Cformat&amp;fit=scale&amp;h=36&amp;ixlib=php-1.2.1&amp;q=70&amp;w=35&amp;wpsize=menu-36x36&amp;s=3d54e26dde253e19e47847b8e88a5878" class="menu-image menu-image-title-after" alt="" height="29" width="28"><span class="menu-image-title">Blog</span><span class="menu-item-description">Read the latest news, tips, and expert guidance on local marketing.</span></a>
                                </li>
                                <li id="menu-item-46259" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-46259">
                                    <a href="https://www.brightlocal.com/insights/research/" class="menu-image-title-after menu-image-not-hovered"><img src="https://corp-brightlocal.imgix.net/2019/04/research_icon.svg?auto=compress%2Cformat&amp;fit=scale&amp;h=36&amp;ixlib=php-1.2.1&amp;q=70&amp;w=35&amp;wpsize=menu-36x36&amp;s=0f292e870c7fffc639d7ce5253e00f23" class="menu-image menu-image-title-after" alt="" height="29" width="28"><span class="menu-image-title">Research</span><span class="menu-item-description">All of BrightLocal’s respected and widely-cited research in one place.</span></a>
                                </li>
                                <li id="menu-item-46258" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-46258">
                                    <a href="https://www.brightlocal.com/insights/webinars/" class="menu-image-title-after menu-image-not-hovered"><img src="https://corp-brightlocal.imgix.net/2019/04/webinars_icon.svg?auto=compress%2Cformat&amp;fit=scale&amp;h=36&amp;ixlib=php-1.2.1&amp;q=70&amp;w=35&amp;wpsize=menu-36x36&amp;s=7db8f5d71fa8c6810f9faa3548174fff" class="menu-image menu-image-title-after" alt="" height="29" width="28"><span class="menu-image-title">Webinars</span><span class="menu-item-description">Get actionable advice and insights from the best in the business.</span></a>
                                </li>
                                <li id="menu-item-46256" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-46256">
                                    <a href="https://www.brightlocal.com/insights/resources/" class="menu-image-title-after menu-image-not-hovered"><img src="https://corp-brightlocal.imgix.net/2019/04/resources_icon.svg?auto=compress%2Cformat&amp;fit=scale&amp;h=36&amp;ixlib=php-1.2.1&amp;q=70&amp;w=35&amp;wpsize=menu-36x36&amp;s=fec40b42699dcb8d1f27ef53841fada2" class="menu-image menu-image-title-after" alt="" height="29" width="28"><span class="menu-image-title">Resources</span><span class="menu-item-description">Top review and citation sites, lists, guidelines, graphics, and more.</span></a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>

        <nav id="login-navigation" class="login-navigation">
            <div class="menu-login-menu-container">
                <ul id="login-menu" class="menu">
                    <li id="menu-item-40" class="login menu-item menu-item-type-custom menu-item-object-custom menu-item-40"><a href="https://tools.brightlocal.com/" class="menu-image-title-after"><span class="menu-image-title">Log in</span></a></li>
                    <li id="menu-item-41" class="menu_button menu-item menu-item-type-custom menu-item-object-custom menu-item-41">
                        <a href="{{route('signup')}}" class="menu-image-title-after">
                            <span class="menu-image-title">Try For Free</span>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>

        <button class="menu-toggle hamburger hamburger--3dy" type="button" aria-controls="top-menu" aria-expanded="false">
            <span class="hamburger-box">
            <span class="hamburger-inner"></span>
            </span>
        </button>
    </div>

</header>


@yield('content')


<footer id="page-footer">
    <div class="footer-cols">
        <div class="footer-col col-left">
            <aside id="text-3" class="widget widget_text">
                <h3 class="widget-title">Products</h3>
                <div class="textwidget">
                    <ul>
                        <li><strong>Platform</strong></li>
                        <li><a href="/local-seo-tools/">Local SEO Tools</a>
                            <ul>
                                <li><a href="/local-seo-tools/local-search-rank-checker/">Local Search Rank Checker</a></li>
                                <li><a href="/local-seo-tools/citation-tracker/">Citation Tracker</a></li>
                                <li><a href="/local-seo-tools/local-search-audit/">Local Search Audit</a></li>
                                <li><a href="/local-seo-tools/google-my-business-audit/">Google My Business Audit</a></li>
                            </ul>
                        </li>
                        <li><a href="/citation-builder/">Citation Builder</a>
                            <ul>
                                <li><a href="/citation-builder/local-data-aggregators/">Data Aggregator Submissions</a></li>
                            </ul>
                        </li>
                        <li><a href="/reputation-manager/">Reputation Manager</a></li>
                        <li><a href="/lead-generation-for-marketing-agencies/">Agency Lead Generator</a></li>
                        <li><a href="/pricing/">Plans &amp; Pricing</a></li>
                    </ul>
                    <ul>
                        <li><strong>Solutions</strong></li>
                        <li><a href="https://www.brightlocal.com/agencies">Agency</a>
                            <ul>
                                <li><a href="/white-label-seo-reports/">White-Label Tools and Reporting</a></li>
                            </ul>
                        </li>
                        <li><a href="https://www.brightlocal.com/multi-location/">Multi-location Business</a></li>
                        <li><a href="https://www.brightlocal.com/small-businesses/">Small Business</a></li>
                        <li><a href="https://www.brightlocal.com/enterprise-packages/">Enterprise</a></li>
                        <li><a href="https://www.brightlocal.com/local-seo-apis/">Local SEO APIs</a></li>
                        <li style="list-style-type: none;">&nbsp;</li>
                        <li><strong>Free Tools</strong></li>
                        <li><a href="/local-search-results-checker/">(Free) Check Local Search Results</a></li>
                        <li><a href="/free-local-seo-tools/google-id-and-review-link-generator/">(Free) Google Link &amp; ID Generator</a></li>
                    </ul>
                </div>
            </aside>
        </div>
        <div class="footer-col col-center">
            <aside id="text-4" class="widget widget_text">
                <h3 class="widget-title">Company</h3>
                <div class="textwidget">
                    <ul>
                        <li><a href="https://www.brightlocal.com/about-us/">About Us</a></li>
                        <li><a href="https://www.brightlocal.com/about-us/#contact">Contact Us</a></li>
                        <li><a href="/case-studies/">Case Studies</a></li>
                        <li><a href="https://www.brightlocal.com/customers/">Customers</a></li>
                        <li><a href="http://www.shareasale.com/shareasale.cfm?merchantID=33269">Affiliate Scheme</a></li>
                        <li><a href="https://www.brightlocal.com/terms-and-conditions/">Terms &amp; Conditions</a></li>
                        <li><a href="https://www.brightlocal.com/privacy-policy/">Privacy Policy</a></li>
                        <li><a href="https://www.brightlocal.com/cookies/">Cookie Policy</a></li>
                    </ul>
                </div>
            </aside>
        </div>
        <div class="footer-col col-right">
            <aside id="text-2" class="widget widget_text">
                <h3 class="widget-title">Community</h3>
                <div class="textwidget">
                    <ul>
                        <li style="list-style-type: none;">
                        </li>
                        <li><strong>Resources</strong></li>
                        <li><a href="/free-online-demo/">Free Live Demo</a></li>
                        <li><a href="https://www.brightlocal.com/insights/webinars/">InsideLocal Webinars</a></li>
                        <li><a href="https://www.brightlocal.com/insights/blog/">Blog</a></li>
                        <li><a href="https://www.brightlocal.com/insights/research/">Research</a></li>
                        <li><a href="/top-seo-companies/">Top SEO Companies</a></li>
                        <p></p>
                        <li><strong>Customers</strong></li>
                        <li><a href="https://tools.brightlocal.com/">Login</a></li>
                        <li><a href="https://help.brightlocal.com/hc/en-us">Help Center</a></li>
                        <li><a href="https://status.brightlocal.com/">Service Status</a></li>
                    </ul>
                </div>
            </aside>
        </div>
    </div>
    <div class="footer-social">
        <aside id="custom_html-2" class="widget_text widget widget_custom_html">
            <div class="textwidget custom-html-widget">
                <ul class="footer-social-icons-list">
                    <li class="social-icons-list-item">
                        <a class="social-icon-link" href="https://twitter.com/bright_local" target="_blank" rel="noopener noreferrer">
                            <span class="screen-reader-text">twitter</span>
                            <i class="icon icon-twitter"></i>
                        </a>
                    </li>
                    <li class="social-icons-list-item">
                        <a class="social-icon-link" href="https://www.facebook.com/brightlocal/" target="_blank" rel="noopener noreferrer">
                            <span class="screen-reader-text">facebook</span>
                            <i class="icon icon-facebook"></i>
                        </a>
                    </li>
                    <li class="social-icons-list-item">
                        <a class="social-icon-link" href="http://www.linkedin.com/company/bright-local-seo" target="_blank" rel="noopener noreferrer">
                            <span class="screen-reader-text">linkedin</span>
                            <i class="icon icon-linkedin"></i>
                        </a>
                    </li>
                    <li class="social-icons-list-item">
                        <a class="social-icon-link" href="https://www.instagram.com/brightlocal/" target="_blank" rel="noopener noreferrer">
                            <span class="screen-reader-text">instagram</span>
                            <i class="icon icon-instagram"></i>
                        </a>
                    </li>
                    <li class="social-icons-list-item">
                        <a class="social-icon-link" href="https://www.youtube.com/user/BrightLocalSEO" target="_blank" rel="noopener noreferrer">
                            <span class="screen-reader-text">youtube</span>
                            <i class="icon icon-youtube"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </aside>
    </div>
    <div class="footer-links">
        <div class="links-center">
            <a href="https://www.brightlocal.com/" class="custom-logo-link" rel="home"><img src="https://corp-brightlocal.imgix.net/2019/04/brightlocal_logo.svg?auto=compress%2Cformat&amp;ixlib=php-1.2.1&amp;q=70&amp;s=d5e025f3cbe6b38e93a207b4b280bdee" class="custom-logo" alt="BrightLocal" height="31" width="181"></a>
        </div>
        <div class="links-left">
        </div>
        <div class="links-right">
            <span>© {{date('Y')}} BrightLocal Ltd</span>
        </div>
    </div>

</footer>

<script type="text/javascript" src="/public/assets2/main.js"></script>
<script type="text/javascript" src="/public/assets2/jquery-scripts.js"></script>

<script src="/public/build/public_app.js"></script>

</body>
</html>
