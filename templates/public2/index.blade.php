
@extends("public2.layout2")

@section('content')
<div id="lightsout">
</div>

<div id="page-container">
<section id="page-content">

    <div class="panel header_hcg sea-green" role="main">
        <div class="panel-content">

            <div class="panel-row">
                <h1>Local Marketing Made Simple</h1>
            </div>

            <div class="panel-row">
                <h3>The local marketing platform that puts you in control of listings, SEO, and reputation. <br>Trusted by marketers. Built by search experts.<br><br>
                    <a class="button green" href="{{route('signup')}}"
                       title="">14 day free trial<sub>No Card Needed</sub></a>
                </h3>
            </div>

            <div class="panel-row code-panel flourish">
                <div class="code  top-tools bottom-leadgen">
                    <section class="hero" id="heroAnime">
                        <div class="slides flex">
                            <div class="slide-display">
                                <div class="hero-carousel carousel-main">
                                    <div id="tools" class="cell active">
                                        <div id="homepageanimationtools_hype_container" hyp_dn="Homepage-animation-tools" style="width: 100%; height: 100%; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); transform-style: flat;">
                                            slide 1
                                        </div>
                                    </div>
                                    <div id="cb" class="cell">
                                        <div id="citationbuilder_hype_container" hyp_dn="citation-builder" style="width: 100%; height: 100%; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); transform-style: flat;">
                                            slide 2

                                        </div>
                                    </div>

                                    <div id="rep" class="cell ">
                                        <div id="reputationmanager_hype_container" hyp_dn="reputation-manager" style="width: 100%; height: 100%; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); transform-style: flat;">
                                            slide 3

                                        </div>
                                    </div>
                                    <div id="lead" class="cell">
                                        <div id="leadgen_hype_container" hyp_dn="leadgen" style="width: 100%; height: 100%; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); transform-style: flat;">
                                            slide 4

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slide-nav">
                                <div class="carousel">
                                    <div id="toolsnav" class="carousel-cell active">
                                        <div id="toolclick" class="title flex">
                                            <div class="icon icon-Local_SEO_Tools icon-Local_SEO_Tools-dims"></div>
                                            <h3>Local SEO Tools</h3>
                                        </div>
                                        <p>Track rankings and citations, perform full local audits, and manage Google My Business with a suite of local SEO tools trusted by over 3,500 marketing agencies.
                                            <br>
                                            <b><a href="/local-seo-tools/">Learn more...</a></b></p>
                                    </div>
                                    <div id="cbnav" class="carousel-cell citation hidep">
                                        <div id="cbclick" class="title flex">
                                            <div class="icon icon-Citation_Builder icon-Citation_Builder-dims"></div>
                                            <h3>Citation Builder</h3>
                                        </div>
                                        <p>Maximize local search rankings and visibility with our expert citation building and data aggregator submission service.
                                            <br><b><a href="/citation-builder/">Learn more...</a></b></p>
                                    </div>
                                    <div id="repnav" class="carousel-cell reputation hidep">
                                        <div id="repclick" class="title flex">
                                            <div class="icon icon-Reputation_Manager icon-Reputation_Manager-dims"></div>
                                            <h3>Reputation Manager</h3>
                                        </div>
                                        <p>Build a brilliant business with a 5-star reputation by generating, monitoring, and responding to reviews in one place.
                                            <br><b><a href="/reputation-manager/">Learn more...</a></b></p>
                                    </div>
                                    <div id="leadnav" class="carousel-cell leadgen hidep">
                                        <div id="leadclick" class="title flex">
                                            <div class="icon icon-Agency_Lead_Generator icon-Agency_Lead_Generator-dims"></div>
                                            <h3>Agency Lead Generator</h3>
                                        </div>
                                        <p>Use our agency lead generation widget to convert more visitors to your agency website without lifting a finger.
                                            <br><b><a href="/lead-generation-for-marketing-agencies/">Learn more...</a></b></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <span class="wistia_embed wistia_async_hc84vpiz5k popover=true popoverOverlayColor=1D2B3B popoverOverlayOpacity=0.95 popoverContent=link wistia_embed_initialized" style="display:inline;position:relative" id="wistia-hc84vpiz5k-1"><div id="wistia_35.thumb_container" class="wistia_click_to_play" style="position: relative; display: inline;"></div></span>
                    <span class="wistia_embed wistia_async_4ez48u2li3 popover=true popoverOverlayColor=1D2B3B popoverOverlayOpacity=0.95 popoverContent=link wistia_embed_initialized" style="display:inline;position:relative" id="wistia-4ez48u2li3-1"><div id="wistia_47.thumb_container" class="wistia_click_to_play" style="position: relative; display: inline;"></div></span>
                    <span class="wistia_embed wistia_async_0dge4d2pff popover=true popoverOverlayColor=1D2B3B popoverOverlayOpacity=0.95 popoverContent=link wistia_embed_initialized" style="display:inline;position:relative" id="wistia-0dge4d2pff-1"><div id="wistia_59.thumb_container" class="wistia_click_to_play" style="position: relative; display: inline;"></div></span> </div>
            </div>

        </div>
    </div>

    <div class="panel gallery white-to-blue top-small bottom-small" role="main" id="">
        <div class="panel-content">

            <div class="panel-row gallery">
                <div class="headings">
                    <h3 class="panel_heading">Helping top brands and agencies deliver local marketing success</h3>
                </div>

                <div class="bl_gallery flex">
                    <div class="galleryimage">
                        <div class="picin">
                            <picture>
                                <img srcset="https://corp-brightlocal.imgix.net/2019/02/Specsavers-1.png?auto=compress%2Cformat&amp;fit=scale&amp;h=120&amp;ixlib=php-1.2.1&amp;q=70&amp;w=209&amp;wpsize=logo-gallery&amp;s=3fb226c320af4eb822bbf80fd3888209" alt="">
                            </picture>
                        </div>
                    </div>

                    <div class="galleryimage">
                        <div class="picin">
                            <picture>
                                <img srcset="https://corp-brightlocal.imgix.net/2019/02/NaNDOS.png?auto=compress%2Cformat&amp;fit=scale&amp;h=120&amp;ixlib=php-1.2.1&amp;q=70&amp;w=209&amp;wpsize=logo-gallery&amp;s=c89ed03c072b3bfc5bc41e5224cb765d" alt="">
                            </picture>
                        </div>
                    </div>

                    <div class="galleryimage">
                        <div class="picin">
                            <picture>
                                <img srcset="https://corp-brightlocal.imgix.net/2019/02/IKEA.png?auto=compress%2Cformat&amp;fit=scale&amp;h=120&amp;ixlib=php-1.2.1&amp;q=70&amp;w=209&amp;wpsize=logo-gallery&amp;s=733360c8080671027cacde5c7d45f706" alt="">
                            </picture>
                        </div>
                    </div>

                    <div class="galleryimage">
                        <div class="picin">
                            <picture>
                                <img srcset="https://corp-brightlocal.imgix.net/2019/02/Roto-rooter.png?auto=compress%2Cformat&amp;fit=scale&amp;h=120&amp;ixlib=php-1.2.1&amp;q=70&amp;w=209&amp;wpsize=logo-gallery&amp;s=2aaafbfe1f98485fb902d340c898051b" alt="">
                            </picture>
                        </div>
                    </div>

                    <div class="galleryimage">
                        <div class="picin">
                            <picture>
                                <img srcset="https://corp-brightlocal.imgix.net/2019/02/Halfords.png?auto=compress%2Cformat&amp;fit=scale&amp;h=120&amp;ixlib=php-1.2.1&amp;q=70&amp;w=209&amp;wpsize=logo-gallery&amp;s=52db160774900dbf4c8db3a80d4b6974" alt="">
                            </picture>
                        </div>
                    </div>

                    <div class="galleryimage">
                        <div class="picin">
                            <picture>
                                <img srcset="https://corp-brightlocal.imgix.net/2019/02/iProspect.png?auto=compress%2Cformat&amp;fit=scale&amp;h=120&amp;ixlib=php-1.2.1&amp;q=70&amp;w=209&amp;wpsize=logo-gallery&amp;s=2f6b38dc104a1d7297d8b4ddc5584e72" alt="">
                            </picture>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel slideshow   " role="main" id="">
        <div class="panel-content">
            <div class="panel-row slideshow">

                <div class="bl_slides carousel flickity-enabled is-draggable" data-flickity="{ &quot;prevNextButtons&quot;: false, &quot;pageDots&quot;: false, &quot;imagesLoaded&quot;: true, &quot;adaptiveHeight&quot;: true  }" tabindex="0">

                    <div class="flickity-viewport" style="height: 410.558px; touch-action: pan-y;">
                        <div class="flickity-slider" style="left: 0px; transform: translateX(0%);">
                            <div class="slide carousel-cell is-selected" aria-selected="true" style="position: absolute; left: 0%;">
                                <div class="outer flex">

                                    <div class="slideimage">
                                        <picture>
                                            <img srcset="https://corp-brightlocal.imgix.net/2019/01/Somnowell-team.png?auto=compress%2Cformat&amp;fit=scale&amp;h=253&amp;ixlib=php-1.2.1&amp;q=70&amp;w=398&amp;wpsize=slide-mob&amp;s=1d4dc476de6463f36bef0b9ba2b8dd88" alt="Somnowell team">
                                        </picture>
                                        <a class="slidelink button small" href="/case-studies/somnowell-marketing/">READ CASE STUDY</a>
                                    </div>

                                    <div class="slidecontent">
                                        <blockquote>
                                            <p>BrightLocal's detailed, white-label reports are fantastic, and the Agency Lead Generator widget saves us a lot of time, as we would normally have to manually generate and send these types of SEO reports ourselves. We’re saving around 30 mins per lead. That’s a saving of $500 per month on 25 leads.</p>
                                        </blockquote>
                                        <cite>
                                            <div class="name">Loran Simon</div>
                                            <div class="job">Managing Director, Somnowell Marketing</div>
                                        </cite>
                                    </div>
                                </div>

                            </div>
                            <div class="slide carousel-cell" aria-selected="false" style="position: absolute; left: 100%;">
                                <div class="outer flex">

                                    <div class="slideimage">
                                        <picture>
                                            <img srcset="https://corp-brightlocal.imgix.net/2019/03/Lead-to-Conversion-Team-photo-1.png?auto=compress%2Cformat&amp;fit=scale&amp;h=193&amp;ixlib=php-1.2.1&amp;q=70&amp;w=398&amp;wpsize=slide-mob&amp;s=7c46289e6bb16d528bdb09ba86bf401b" alt="Lead to Conversion">
                                        </picture>
                                        <a class="slidelink button small" href="/case-studies/lead-to-conversion/">READ CASE STUDY</a>
                                    </div>

                                    <div class="slidecontent">
                                        <blockquote>
                                            <p>What used to take the agency 10 hours manually, we can now do in 10 minutes with BrightLocal. Not only does BrightLocal increase efficiencies and reduce cost agency-wide, but they're also passed on to our clients as well.</p>
                                        </blockquote>
                                        <cite>
                                            <div class="name">Matthew Travers</div>
                                            <div class="job">Executive Vice President, Lead to Conversion</div>
                                        </cite>
                                    </div>
                                </div>

                            </div>
                            <div class="slide carousel-cell" aria-selected="false" style="position: absolute; left: 200%;">
                                <div class="outer flex">

                                    <div class="slideimage">
                                        <picture>

                                            <img srcset="https://corp-brightlocal.imgix.net/2019/01/slideimage-2.jpg?auto=compress%2Cformat&amp;fit=scale&amp;h=253&amp;ixlib=php-1.2.1&amp;q=70&amp;w=398&amp;wpsize=slide-mob&amp;s=59156f0b40ccfb7c6195a6164f966c5c" alt="">
                                        </picture>
                                        <a class="slidelink button small" href="/case-studies/vortala-digital/">READ CASE STUDY</a>
                                    </div>

                                    <div class="slidecontent">
                                        <blockquote>
                                            <p>We can see at a glance which of our clients are ranking well and which ones need more attention. We’re so happy with the reporting that we’re now in the process of moving our citation creation to BrightLocal as well.</p>

                                        </blockquote>
                                        <cite>
                                            <div class="name">Alicia Hardy</div>
                                            <div class="job">Director of Digital Marketing, Vortala Digital</div>
                                        </cite>
                                    </div>
                                </div>

                            </div>
                            <div class="slide carousel-cell" aria-selected="false" style="position: absolute; left: 300%;">
                                <div class="outer flex">

                                    <div class="slideimage">
                                        <picture>

                                            <img srcset="https://corp-brightlocal.imgix.net/2019/01/slideimage-3.jpg?auto=compress%2Cformat&amp;fit=scale&amp;h=253&amp;ixlib=php-1.2.1&amp;q=70&amp;w=398&amp;wpsize=slide-mob&amp;s=9004035fc89060051499730c5fb04bb5" alt="">
                                        </picture>
                                        <a class="slidelink button small" href="/case-studies/staylisted/">READ CASE STUDY</a>
                                    </div>

                                    <div class="slidecontent">
                                        <blockquote>
                                            <p>I don't know how we ever got by without BrightLocal's citation service. We get high-quality citations built quickly with interactive reports that clients understand. Now, we're not only saving time and money for our agency, but we're offering a superior product to our clients.</p>
                                        </blockquote>
                                        <cite>
                                            <div class="name">Sergio Salazar</div>
                                            <div class="job">CEO, Staylisted</div>
                                        </cite>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="slideshow-nav carousel-nav flickity-enabled is-draggable" data-flickity="{ &quot;asNavFor&quot;: &quot;.bl_slides&quot;, &quot;contain&quot;: true, &quot;imagesLoaded&quot;: true, &quot;prevNextButtons&quot;: false, &quot;pageDots&quot;: false }" tabindex="0">

                    <div class="flickity-viewport" style="height: 127.973px; touch-action: pan-y;">
                        <div class="flickity-slider" style="left: 0px; transform: translateX(0%);">
                            <div class="carousel-cell is-nav-selected is-selected" aria-selected="true" style="position: absolute; left: 0%;">
                                <div class="naveimage">
                                    <picture>

                                        <img srcset="https://corp-brightlocal.imgix.net/2019/02/Artboard.png?auto=compress%2Cformat&amp;fit=crop&amp;h=88&amp;ixlib=php-1.2.1&amp;q=70&amp;w=228&amp;wpsize=slidenav&amp;s=8d28b93e225e386c2ee84644a1c9a2c1" alt="Somnowell marketing">
                                    </picture>
                                </div>
                            </div>
                            <div class="carousel-cell" aria-selected="false" style="position: absolute; left: 25%;">
                                <div class="naveimage">
                                    <picture>
                                        <img srcset="https://corp-brightlocal.imgix.net/2018/12/lead-to-conversion-logo-536x213.png?auto=compress%2Cformat&amp;fit=crop&amp;h=88&amp;ixlib=php-1.2.1&amp;q=70&amp;w=228&amp;wpsize=slidenav&amp;s=772c6491456cba1e8cd89f4142e74ccc" alt="Lead to Conversion Logo">
                                    </picture>
                                </div>
                            </div>
                            <div class="carousel-cell" aria-selected="false" style="position: absolute; left: 50%;">
                                <div class="naveimage">
                                    <picture>
                                        <img srcset="https://corp-brightlocal.imgix.net/2018/12/vortala-logo-transparent.png?auto=compress%2Cformat&amp;fit=crop&amp;h=88&amp;ixlib=php-1.2.1&amp;q=70&amp;w=228&amp;wpsize=slidenav&amp;s=467ad88d20db32bef6ac025ee1c0637b" alt="Vortala Logo">
                                    </picture>
                                </div>
                            </div>
                            <div class="carousel-cell" aria-selected="false" style="position: absolute; left: 75%;">
                                <div class="naveimage">
                                    <picture>
                                        <img srcset="https://corp-brightlocal.imgix.net/2018/12/staylisted-logo-transparent.png?auto=compress%2Cformat&amp;fit=crop&amp;h=88&amp;ixlib=php-1.2.1&amp;q=70&amp;w=228&amp;wpsize=slidenav&amp;s=518eb4651f9da82f495c3296780bbb59" alt="Staylisted Logo">
                                    </picture>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="panel banner none top-none bottom-small" role="main" id="">
        <div class="panel-content">

            <div class="panel-banner blue">
                <blockquote>

                    <p><strong>
                            I recommend BrightLocal as a strong tool for anyone who is serious about local search. Having them on your side is like having full-time local SEO staff!</strong></p>
                </blockquote><cite><div class="flex"><div class="person"><span><strong>Casey Meraz</strong></span><span>CEO, Ethical SEO Consulting</span></div></div></cite>
                <div class="ph_cta"><a class="button green" href="{{route('signup')}}" title="">
                        14 Day Free Trial<sub>No Card Needed</sub>
                    </a>
                </div>

                <div class="grid right flex textcenter">
                    <div class="trustpilot">
                        <div class="rating-stars">
                        </div>
                        <h3>9.5/10</h3>
                        <p>from 300+ reviews on Trustpilot</p>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="panel cta_banner   " role="main" id="">
        <div class="panel-content">

            <div class="panel-row">
                <div class="cta-banner blue">
                    <div class="banner-wrapper">
                        <div class="cta-banner-image">
                            <img class="lazyload" data-src="https://corp-brightlocal.imgix.net/2019/06/icon-cta.svg?auto=compress%2Cformat&amp;ixlib=php-1.2.1&amp;q=70&amp;s=337d02a8feea1d79b538601e01eb75b0" alt=""> </div>
                        <div class="cta-banner-text">
                            <h4>Try BrightLocal free for 14 days</h4>
                            <p>All features · Unlimited access · No card required</p>
                        </div>
                        <div class="cta-banner-cta">
                            <div class="banner_cta"><a class="button green" href="https://tools.brightlocal.com/seo-tools/admin/sign-up/2003" title "14=" " day=" " free=" " trial"="">14 DAY FREE TRIAL<sub></sub></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel gallery none top-small bottom-regular" role="main" id="">
        <div class="panel-content">

            <div class="panel-row gallery">

                <div class="headings">
                    <h3 class="panel_heading">BrightLocal's tools and research have been featured in:</h3>
                </div>

                <div class="bl_gallery flex">

                    <div class="galleryimage">
                        <div class="picin">
                            <picture>
                                <img srcset="https://corp-brightlocal.imgix.net/2019/01/bloomberg.png?auto=compress%2Cformat&amp;fit=scale&amp;h=120&amp;ixlib=php-1.2.1&amp;q=70&amp;w=205&amp;wpsize=logo-gallery&amp;s=5dbd455db2b960f59543da2ada218a92" alt="">
                            </picture>
                            <p>Bloomberg covered our Local Consumer Review Survey as part of a story about fake Yelp Reviews.</p>
                        </div>
                    </div>

                    <div class="galleryimage">
                        <div class="picin">
                            <picture>
                                <img srcset="https://corp-brightlocal.imgix.net/2019/01/search-engine-land.png?auto=compress%2Cformat&amp;fit=scale&amp;h=120&amp;ixlib=php-1.2.1&amp;q=70&amp;w=205&amp;wpsize=logo-gallery&amp;s=dd9bcfe751a370665452eda8d903c525" alt="">
                            </picture>
                            <p>BrightLocal’s Head of Content, Jamie Pitman writes a monthly column for Search Engine Land, specializing in local marketing.</p>
                        </div>
                    </div>

                    <div class="galleryimage">
                        <div class="picin">
                            <picture>
                                <img srcset="https://corp-brightlocal.imgix.net/2019/01/cnbc.png?auto=compress%2Cformat&amp;fit=scale&amp;h=120&amp;ixlib=php-1.2.1&amp;q=70&amp;w=146&amp;wpsize=logo-gallery&amp;s=642330eda6b7b645405f4ea94f2b4b74" alt="">
                            </picture>
                            <p>An FTC ruling highlights issue with fake reviews, and CNBC reference our research in the process.</p>
                        </div>
                    </div>

                    <div class="galleryimage">
                        <div class="picin">
                            <picture>
                                <img srcset="https://corp-brightlocal.imgix.net/2019/01/sej.png?auto=compress%2Cformat&amp;fit=scale&amp;h=120&amp;ixlib=php-1.2.1&amp;q=70&amp;w=146&amp;wpsize=logo-gallery&amp;s=f375054b5ba2559296b812f80e7e4893" alt="">
                            </picture>
                            <p>SEJ mentioned our platform in a showcase of nine “essential” local SEO tools.</p>
                        </div>
                    </div>

                    <div class="galleryimage">
                        <div class="picin">
                            <picture>

                                <img srcset="https://corp-brightlocal.imgix.net/2019/04/fast-companySMALL-BANNER.png?auto=compress%2Cformat&amp;fit=scale&amp;h=120&amp;ixlib=php-1.2.1&amp;q=70&amp;w=414&amp;wpsize=logo-gallery&amp;s=14a34e1404ba65ae898ed7ee615a2502" alt="Fast Company">

                            </picture>
                            <p>Fast Company referenced data from BrightLocal’s Local Consumer Review Survey in a piece about responding to fake reviews.</p>
                        </div>
                    </div>

                    <div class="galleryimage">
                        <div class="picin">
                            <picture>

                                <img srcset="https://corp-brightlocal.imgix.net/2019/04/Econsultancy_SecondaryNEWER.png?auto=compress%2Cformat&amp;fit=scale&amp;h=120&amp;ixlib=php-1.2.1&amp;q=70&amp;w=260&amp;wpsize=logo-gallery&amp;s=d402138eff831f505de1cad459732345" alt="Econsultancy">

                            </picture>
                            <p>While recommending optimization for a range of voice assistants, Econsultancy referred to our landmark study, ‘Voice Search for Local Business’.</p>
                        </div>
                    </div>

                    <div class="galleryimage">
                        <div class="picin">
                            <picture>

                                <img srcset="https://corp-brightlocal.imgix.net/2019/04/Inc.png?auto=compress%2Cformat&amp;fit=scale&amp;h=120&amp;ixlib=php-1.2.1&amp;q=70&amp;w=193&amp;wpsize=logo-gallery&amp;s=1be512387ec28dcd240d79893f9ab267" alt="Inc logo">

                            </picture>
                            <p>In a high-level piece about leadership, Inc. pointed to data from BrightLocal’s Local Consumer Review Survey.</p>
                        </div>
                    </div>

                    <div class="galleryimage">
                        <div class="picin">
                            <picture>

                                <img srcset="https://corp-brightlocal.imgix.net/2019/04/download-1.png?auto=compress%2Cformat&amp;fit=scale&amp;h=120&amp;ixlib=php-1.2.1&amp;q=70&amp;w=193&amp;wpsize=logo-gallery&amp;s=501cfb3e95b1a7d6a30cabeb419bd7c7" alt="Forbes">

                            </picture>
                            <p>Forbes regularly cite BrightLocal’s research in their pieces on local business, online reviews, and business reputation.</p>
                        </div>
                    </div>

                    <div class="galleryimage">
                        <div class="picin">
                            <picture>

                                <img srcset="https://corp-brightlocal.imgix.net/2019/04/entrepreneur-logo2.png?auto=compress%2Cformat&amp;fit=scale&amp;h=120&amp;ixlib=php-1.2.1&amp;q=70&amp;w=305&amp;wpsize=logo-gallery&amp;s=b475ac827fc387b1777faa6ee7c92a69" alt="Entrepreneur">

                            </picture>
                            <p>Entrepreneur highlighted the importance of social proof on your website, and used data from the Local Consumer Survey to back it up.</p>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <div class="panel posts_panel insightsbg top-none bottom-none" role="main" id="">
        <div class="panel-content">

            <div class="panel-row">

                <div class="headings">
                    <h2 class="panel_heading">Get the Latest Local SEO Insights</h2>
                    <p class="panel_subheading">Got a question about local SEO? Browse a wealth of resources and reports for the knowledge you need.</p>
                </div>

                <div class="homepage-insights top-none bottom-none">
                    <div class="homepage-posts loop-index">

                        <article class="post">
                            <div class="insight-post">
                                <div class="thumb">
                                    <a href="https://www.brightlocal.com/blog/new-google-my-business-features-june-2019/"><img width="1588" height="768" src="https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;ixlib=php-1.2.1&amp;q=70&amp;s=e57787665effc7cf9397676794794c1d" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;ixlib=php-1.2.1&amp;q=70&amp;s=e57787665effc7cf9397676794794c1d 1588w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=544&amp;ixlib=php-1.2.1&amp;q=70&amp;w=1125&amp;wpsize=thumbnail1360&amp;s=71da5719c4469a052a17dd329488bc73 1125w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=145&amp;ixlib=php-1.2.1&amp;q=70&amp;w=300&amp;wpsize=medium&amp;s=61ba198663d898ef963f00e67e9a385e 300w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=371&amp;ixlib=php-1.2.1&amp;q=70&amp;w=768&amp;wpsize=thumbnail768&amp;s=0e7476f758a03265863fe60caf200eba 768w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=495&amp;ixlib=php-1.2.1&amp;q=70&amp;w=1024&amp;wpsize=thumbnail1024&amp;s=bdab1a555e0bfe08eb4b431ac4bf175b 1024w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=387&amp;ixlib=php-1.2.1&amp;q=70&amp;w=800&amp;wpsize=posts800&amp;s=0a379446bbf260bcba65eb02d93cfd47 800w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=656&amp;ixlib=php-1.2.1&amp;q=70&amp;w=1356&amp;wpsize=header1356&amp;s=8d7c22d9e678769ffc63ee2924a3ceb9 1356w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=200&amp;ixlib=php-1.2.1&amp;q=70&amp;w=414&amp;wpsize=thumbnail414&amp;s=2d5695d72ea7a6e325f5fb627c8b8479 414w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=364&amp;ixlib=php-1.2.1&amp;q=70&amp;w=753&amp;wpsize=header753&amp;s=c83ab15a05520e3427abf0ae82f987a8 753w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=266&amp;ixlib=php-1.2.1&amp;q=70&amp;w=551&amp;wpsize=header551&amp;s=9f6b0fc5c067fe45ddfc06bf6edffb04 551w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=728&amp;ixlib=php-1.2.1&amp;q=70&amp;w=1506&amp;wpsize=header1506&amp;s=e23af559ee46cd56186b81a96ba79b61 1506w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=232&amp;ixlib=php-1.2.1&amp;q=70&amp;w=480&amp;wpsize=cs_logo480&amp;s=7f74059220cadc478dc66856f5bba861 480w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=464&amp;ixlib=php-1.2.1&amp;q=70&amp;w=960&amp;wpsize=blog_thumb_l_2x&amp;s=5b77399aa825847149180844bf250c90 960w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=174&amp;ixlib=php-1.2.1&amp;q=70&amp;w=360&amp;wpsize=blog_thumb_m&amp;s=0412b52603efb9d4f28e102c2c9cf395 360w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=348&amp;ixlib=php-1.2.1&amp;q=70&amp;w=720&amp;wpsize=blog_thumb_m_2x&amp;s=d41d29b70e887a80034c07c9b6ee98eb 720w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=131&amp;ixlib=php-1.2.1&amp;q=70&amp;w=270&amp;wpsize=blog_thumb_s&amp;s=98ba4036532d75449e3e4ce0ab669202 270w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=261&amp;ixlib=php-1.2.1&amp;q=70&amp;w=540&amp;wpsize=blog_thumb_s_2x&amp;s=5d17e072696c705dba7be554956cdbce 540w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=120&amp;ixlib=php-1.2.1&amp;q=70&amp;w=248&amp;wpsize=logo-gallery&amp;s=24fbfa010ad3d96d0929448176b2c263 248w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=93&amp;ixlib=php-1.2.1&amp;q=70&amp;w=192&amp;wpsize=headshot&amp;s=2e8bcbd850c4f82b1cb69be3da6b888e 192w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=50&amp;ixlib=php-1.2.1&amp;q=70&amp;w=103&amp;wpsize=t_logo&amp;s=66453846e9041b58512abecf4064d159 103w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=530&amp;ixlib=php-1.2.1&amp;q=70&amp;w=1095&amp;wpsize=slide-hd&amp;s=e245bba4fcf7895fee263477da68116d 1095w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=435&amp;ixlib=php-1.2.1&amp;q=70&amp;w=900&amp;wpsize=slide-sd&amp;s=9003367a2ce7991031c757567d7e11f1 900w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=379&amp;ixlib=php-1.2.1&amp;q=70&amp;w=783&amp;wpsize=slide-tab&amp;s=6940718b3e5e930cbed3fdbab7aa4203 783w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=192&amp;ixlib=php-1.2.1&amp;q=70&amp;w=398&amp;wpsize=slide-mob&amp;s=6f40cb8040d937dddc7eb43efae8c626 398w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=757&amp;ixlib=php-1.2.1&amp;q=70&amp;w=1566&amp;wpsize=slide-tabx2&amp;s=23b3c4177c1332b00ce6973b234f4edf 1566w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=385&amp;ixlib=php-1.2.1&amp;q=70&amp;w=796&amp;wpsize=slide-mobx2&amp;s=e7120873f9d5dde3f5c95291f7669898 796w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=240&amp;ixlib=php-1.2.1&amp;q=70&amp;w=496&amp;wpsize=logo-galleryx2&amp;s=8357330799bf76b2cb9c9972b5ae8c7c 496w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=300&amp;ixlib=php-1.2.1&amp;q=70&amp;w=620&amp;wpsize=med-gallery&amp;s=739150fc1d0c7234c7353d6732d3c3fb 620w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=600&amp;ixlib=php-1.2.1&amp;q=70&amp;w=1241&amp;wpsize=med-galleryx2&amp;s=e8d6c7df1581f2580aad2f19b7a0b689 1241w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=560&amp;ixlib=php-1.2.1&amp;q=70&amp;w=1158&amp;wpsize=lrg-gallery&amp;s=d97bd50427848b862cb3a096d7fe088a 1158w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=12&amp;ixlib=php-1.2.1&amp;q=70&amp;w=24&amp;wpsize=menu-24x24&amp;s=3f53e3e4321483912a80d64bf88d76c8 24w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=17&amp;ixlib=php-1.2.1&amp;q=70&amp;w=36&amp;wpsize=menu-36x36&amp;s=47c12080ae2277dea41799e55d38c011 36w, https://corp-brightlocal.imgix.net/2019/06/The-New-Google-My-Business-Features-Giving-Power-Back-to-Local-BusinessesHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=23&amp;ixlib=php-1.2.1&amp;q=70&amp;w=48&amp;wpsize=menu-48x48&amp;s=eabd4881f7c033aa929f9d0cc319eb6e 48w" sizes="(max-width: 1588px) 100vw, 1588px"></a>
                                </div>
                                <div class="content">
                                    <h3><a href="https://www.brightlocal.com/blog/new-google-my-business-features-june-2019/">The New Google My Business Features Giving Power Back to Local Businesses</a></h3>
                                    <div class="excerpt">
                                        <p>In a blog on June 20th, Google My Business announced a number of exciting new features that we believe will help local businesses maximize the impact of their listings. The blog, ‘Helping businesses capture their identity with Google My Business‘, identifies a number of new features to Google My Business. While GMB is busy releasing […]</p>
                                        <span class="publish-date">Published 2019</span>
                                    </div>
                                </div>
                                <div class="post-meta">
                                    <div class="meta author"><img src="https://secure.gravatar.com/avatar/ca2c6154c050e809168af16c57d30d2b?s=48&amp;d=mm&amp;r=g" width="48" height="48" alt="Avatar" class="avatar avatar-48 wp-user-avatar wp-user-avatar-48 photo avatar-default"><span class="list"><strong>By Rosie Murphy on June 21st, 2019</strong></span><span class="grid"><strong>Rosie Murphy</strong><br>June 21st, 2019</span>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post">
                            <div class="insight-post">
                                <div class="thumb">
                                    <a href="https://www.brightlocal.com/blog/google-testing-gmb-posts-related-to-your-search/"><img width="1588" height="768" src="https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;ixlib=php-1.2.1&amp;q=70&amp;s=dac1a07053da10fa923ee67e4e94e4cc" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="Google Testing GMB Posts 'Related to Your Search'" srcset="https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;ixlib=php-1.2.1&amp;q=70&amp;s=dac1a07053da10fa923ee67e4e94e4cc 1588w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=544&amp;ixlib=php-1.2.1&amp;q=70&amp;w=1125&amp;wpsize=thumbnail1360&amp;s=17e706d355bc20b71cd5d2a557ef871b 1125w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=145&amp;ixlib=php-1.2.1&amp;q=70&amp;w=300&amp;wpsize=medium&amp;s=04ae34b0020aa1782a81817193a90f35 300w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=371&amp;ixlib=php-1.2.1&amp;q=70&amp;w=768&amp;wpsize=thumbnail768&amp;s=52d3f7ccdb6deea56e2f605adbaffc8a 768w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=495&amp;ixlib=php-1.2.1&amp;q=70&amp;w=1024&amp;wpsize=thumbnail1024&amp;s=e5c16fc1ef0f1cf4780953621418adf9 1024w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=387&amp;ixlib=php-1.2.1&amp;q=70&amp;w=800&amp;wpsize=posts800&amp;s=e96457feb2e38d2c5b2c1f654077b67d 800w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=656&amp;ixlib=php-1.2.1&amp;q=70&amp;w=1356&amp;wpsize=header1356&amp;s=fdba63c9e0984bd3e9fab1f980a57b71 1356w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=200&amp;ixlib=php-1.2.1&amp;q=70&amp;w=414&amp;wpsize=thumbnail414&amp;s=100148bf5777f690f994899c2fde1817 414w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=364&amp;ixlib=php-1.2.1&amp;q=70&amp;w=753&amp;wpsize=header753&amp;s=9cf5f083bb434b32c76df0607d52b7f2 753w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=266&amp;ixlib=php-1.2.1&amp;q=70&amp;w=551&amp;wpsize=header551&amp;s=060249199af1fd409f10a6fd7b1af2f4 551w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=728&amp;ixlib=php-1.2.1&amp;q=70&amp;w=1506&amp;wpsize=header1506&amp;s=bdbec62564325414f56406dd0d4d17ea 1506w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=232&amp;ixlib=php-1.2.1&amp;q=70&amp;w=480&amp;wpsize=cs_logo480&amp;s=aca779e4b0e07f5709d00736a3c7b798 480w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=464&amp;ixlib=php-1.2.1&amp;q=70&amp;w=960&amp;wpsize=blog_thumb_l_2x&amp;s=d458055a3f6240416671f239a6345c18 960w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=174&amp;ixlib=php-1.2.1&amp;q=70&amp;w=360&amp;wpsize=blog_thumb_m&amp;s=f193a688ba81a6d0b1af89729ba09438 360w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=348&amp;ixlib=php-1.2.1&amp;q=70&amp;w=720&amp;wpsize=blog_thumb_m_2x&amp;s=76fd8b6f6d4fa596125503fa1c3d7422 720w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=131&amp;ixlib=php-1.2.1&amp;q=70&amp;w=270&amp;wpsize=blog_thumb_s&amp;s=3bc3a4dcfce89f0e89851c21a256b79d 270w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=261&amp;ixlib=php-1.2.1&amp;q=70&amp;w=540&amp;wpsize=blog_thumb_s_2x&amp;s=48d4be25a263c52d9011ac85d422d7c1 540w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=120&amp;ixlib=php-1.2.1&amp;q=70&amp;w=248&amp;wpsize=logo-gallery&amp;s=4e599bfa178bd52ca547be28a6058aef 248w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=93&amp;ixlib=php-1.2.1&amp;q=70&amp;w=192&amp;wpsize=headshot&amp;s=d976245f9cac40947f5209933945fae1 192w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=50&amp;ixlib=php-1.2.1&amp;q=70&amp;w=103&amp;wpsize=t_logo&amp;s=4edc29e3c9a805b0cdc13d1bc0b4220d 103w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=530&amp;ixlib=php-1.2.1&amp;q=70&amp;w=1095&amp;wpsize=slide-hd&amp;s=c10364530d5b72d812a2eb3f2d2256bd 1095w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=435&amp;ixlib=php-1.2.1&amp;q=70&amp;w=900&amp;wpsize=slide-sd&amp;s=df1e93e094d78950946c2cd4a426058f 900w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=379&amp;ixlib=php-1.2.1&amp;q=70&amp;w=783&amp;wpsize=slide-tab&amp;s=813b84bcbbacafa742bbf7de78a9e66e 783w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=192&amp;ixlib=php-1.2.1&amp;q=70&amp;w=398&amp;wpsize=slide-mob&amp;s=1e01cf683e470fc3a971089e292893e7 398w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=757&amp;ixlib=php-1.2.1&amp;q=70&amp;w=1566&amp;wpsize=slide-tabx2&amp;s=6647e0591d5dc3693603b52d56ce8fed 1566w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=385&amp;ixlib=php-1.2.1&amp;q=70&amp;w=796&amp;wpsize=slide-mobx2&amp;s=61e2c04a6a1e57c2243e9f14b7edb3d1 796w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=240&amp;ixlib=php-1.2.1&amp;q=70&amp;w=496&amp;wpsize=logo-galleryx2&amp;s=7ae8c0bdc5fa2a388d80afec3577e1de 496w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=300&amp;ixlib=php-1.2.1&amp;q=70&amp;w=620&amp;wpsize=med-gallery&amp;s=dbc62846ce80929e1d73a77a3e1fb78f 620w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=600&amp;ixlib=php-1.2.1&amp;q=70&amp;w=1241&amp;wpsize=med-galleryx2&amp;s=3f4ee6cd2f85988988e7adfab767569f 1241w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=560&amp;ixlib=php-1.2.1&amp;q=70&amp;w=1158&amp;wpsize=lrg-gallery&amp;s=8e89d0561b07a497483ae35378139d02 1158w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=12&amp;ixlib=php-1.2.1&amp;q=70&amp;w=24&amp;wpsize=menu-24x24&amp;s=3c0ef9bcc8a5370915e17f8a3e4d66a1 24w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=17&amp;ixlib=php-1.2.1&amp;q=70&amp;w=36&amp;wpsize=menu-36x36&amp;s=9a152fa80c6a490078e70b9e9f1698b3 36w, https://corp-brightlocal.imgix.net/2019/06/Google-Testing-GMB-Posts-Related-to-Your-SearchHeader.png?auto=compress%2Cformat&amp;fit=scale&amp;h=23&amp;ixlib=php-1.2.1&amp;q=70&amp;w=48&amp;wpsize=menu-48x48&amp;s=b290d759a1a9c854de891b631c87c321 48w" sizes="(max-width: 1588px) 100vw, 1588px"></a>
                                </div>
                                <div class="content">
                                    <h3><a href="https://www.brightlocal.com/blog/google-testing-gmb-posts-related-to-your-search/">Google Is Testing GMB Posts ‘Related to Your Search’</a></h3>
                                    <div class="excerpt">
                                        <p>Google is always testing new things in Google My Business. Here, contributor Claire Carlile shows us an example which proves that GMB Posts are worth investing in for almost every type of local business. Back In the Day In my Brighton SEO deck from this April, I waxed lyrical about how we were all having […]</p>
                                        <span class="publish-date">Published 2019</span>
                                    </div>
                                </div>
                                <div class="post-meta">
                                    <div class="meta author"><img src="https://corp-brightlocal.imgix.net/2019/06/Claire-Carlile.png?fit=fit&amp;fm=jpg&amp;h=48&amp;ixlib=php-1.2.1&amp;w=48&amp;s=020c7b00eb1992943f997611206f57af" width="48" height="48" alt="Claire Carlile" class="avatar avatar-48 wp-user-avatar wp-user-avatar-48 alignnone photo"><span class="list"><strong>By Claire Carlile on June 20th, 2019</strong></span><span class="grid"><strong>Claire Carlile</strong><br>June 20th, 2019</span>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <article class="post">
                            <div class="insight-post">

                                <div class="content">
                                    <h3><a href="https://www.brightlocal.com/blog/acxiom-to-retire-directory-and-local-search-products-at-end-of-2019/">Acxiom to Retire Directory and Local Search Products at End of 2019</a></h3>
                                    <div class="excerpt">
                                        <p>In 2018, Acxiom was acquired by publicly-traded advertising company IPG. In order to comply with the California Consumer Privacy Act (CCPA) which comes into effect on January 1st, 2020, Acxiom will be shuttering its directory and local search operations, including its data aggregator functions. (N.b. If you’re asking ‘what’s a data aggregator?’ we’ve got you […]</p>
                                        <span class="publish-date">Published 2019</span>
                                    </div>
                                </div>
                                <div class="post-meta">
                                    <div class="meta author">
                                        <span class="list"><strong>By Vicky Chandler on June 12th, 2019</strong></span><span class="grid"><strong>Vicky Chandler</strong><br>June 12th, 2019</span>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>

                <div class="loadmore-button">
                    <a href="/bright-ideas" class="button blue" id="more_posts">Learn about latest trends in local marketing</a>
                </div>

            </div>

        </div>
    </div>

</section>
<!-- #page-content -->

<!-- <aside id="page-sidebar">
            </aside> -->
</div>

@endsection
